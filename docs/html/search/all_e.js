var searchData=
[
  ['raster',['Raster',['../structRaster.html',1,'']]],
  ['raster_2ec',['Raster.c',['../Raster_8c.html',1,'']]],
  ['raster_5fcopy',['Raster_copy',['../Raster_8c.html#a2c6feb8d14e31a76d4323da2c6692108',1,'Raster.c']]],
  ['raster_5fdownsample',['Raster_downsample',['../Raster_8c.html#aaaecebd69f030d0fb2b555dd5798327c',1,'Raster.c']]],
  ['raster_5ffree',['Raster_free',['../Raster_8c.html#a7bb9cd784b76c11040e69158668ccd6b',1,'Raster.c']]],
  ['raster_5finit',['Raster_init',['../Raster_8c.html#abdd3ed76f4a7e6a979b34032dad3db26',1,'Raster.c']]],
  ['raster_5fread',['Raster_read',['../Raster_8c.html#a71cc9dd0c30384a968c5f48e4d299891',1,'Raster.c']]],
  ['raster_5fstat',['Raster_stat',['../Raster_8c.html#afca5550e64a0aae4a6103db89b602650',1,'Raster.c']]],
  ['raster_5fwrite',['Raster_write',['../Raster_8c.html#ac107ca008d3aaa557b07db611c4ec62f',1,'Raster.c']]],
  ['record',['record',['../structVLR.html#a3dfe282e60d7bdecd224a5c576870a87',1,'VLR']]],
  ['record_5fid',['record_id',['../structVLR.html#a9ab534f362de2794689b9dad1f45ed4e',1,'VLR']]],
  ['record_5flen',['record_len',['../structVLR.html#a8db7948242ccd5fbf2e0e9fe072479e3',1,'VLR']]],
  ['relativeequalfloat',['relativeEqualFloat',['../Util_8c.html#a5e6a939b1592f52bfb271440aec1529e',1,'Util.c']]],
  ['restartiter',['restartIter',['../structCONVOptions.html#a9c9e60335d29341b147bc983c782bc66',1,'CONVOptions']]],
  ['ridge',['ridge',['../Grid_8c.html#ac6571eaa7872f21b139f976b6a22dcfb',1,'ridge(float xx, float xy, float yy):&#160;Grid.c'],['../Grid_8h.html#ac6571eaa7872f21b139f976b6a22dcfb',1,'ridge(float xx, float xy, float yy):&#160;Grid.c']]],
  ['right_5flcols',['right_LCols',['../structmem__win.html#ac123a4e8e17a41927a46324c54872644',1,'mem_win']]],
  ['rows',['rows',['../structGrid.html#a6f79f2e87557be68e606ea5046b2c5f7',1,'Grid']]]
];

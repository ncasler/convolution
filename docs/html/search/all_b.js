var searchData=
[
  ['nbands',['nBands',['../structCONVPatchDesc.html#aa35755854c193e26822dd6f8442cd824',1,'CONVPatchDesc']]],
  ['ndims',['ndims',['../structmGrid.html#acbc0c1441d1c2a8a252ceec2b6dabbcb',1,'mGrid']]],
  ['nibb_5fswap32',['nibb_swap32',['../Util_8c.html#aa51b6fdb4df430d7b06b15de5042267c',1,'Util.c']]],
  ['niter',['nIter',['../structCONVOptions.html#ae33b141a0538a96b8062f7d7cd0022c6',1,'CONVOptions']]],
  ['nodata',['nodata',['../structCONVPatchDesc.html#a5992f93d52c55ec5317e44e308d1ae50',1,'CONVPatchDesc::nodata()'],['../structGrid.html#a3f259846123ce695f57435704cd102fd',1,'Grid::nodata()']]],
  ['nvars',['nvars',['../structmGrid.html#ac9263bae762eea2bb8a487732a5e9b66',1,'mGrid']]]
];

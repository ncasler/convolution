var searchData=
[
  ['raster_5fcopy',['Raster_copy',['../Raster_8c.html#a2c6feb8d14e31a76d4323da2c6692108',1,'Raster.c']]],
  ['raster_5fdownsample',['Raster_downsample',['../Raster_8c.html#aaaecebd69f030d0fb2b555dd5798327c',1,'Raster.c']]],
  ['raster_5ffree',['Raster_free',['../Raster_8c.html#a7bb9cd784b76c11040e69158668ccd6b',1,'Raster.c']]],
  ['raster_5finit',['Raster_init',['../Raster_8c.html#abdd3ed76f4a7e6a979b34032dad3db26',1,'Raster.c']]],
  ['raster_5fread',['Raster_read',['../Raster_8c.html#a71cc9dd0c30384a968c5f48e4d299891',1,'Raster.c']]],
  ['raster_5fstat',['Raster_stat',['../Raster_8c.html#afca5550e64a0aae4a6103db89b602650',1,'Raster.c']]],
  ['raster_5fwrite',['Raster_write',['../Raster_8c.html#ac107ca008d3aaa557b07db611c4ec62f',1,'Raster.c']]],
  ['relativeequalfloat',['relativeEqualFloat',['../Util_8c.html#a5e6a939b1592f52bfb271440aec1529e',1,'Util.c']]],
  ['ridge',['ridge',['../Grid_8c.html#ac6571eaa7872f21b139f976b6a22dcfb',1,'ridge(float xx, float xy, float yy):&#160;Grid.c'],['../Grid_8h.html#ac6571eaa7872f21b139f976b6a22dcfb',1,'ridge(float xx, float xy, float yy):&#160;Grid.c']]]
];

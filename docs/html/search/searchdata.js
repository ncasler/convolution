var indexSectionsWithContent =
{
  0: "abcdeghiklmnoprsuvw",
  1: "bcgklmprsvw",
  2: "acgkpru",
  3: "abcgknprs",
  4: "adeghilmnopruvw",
  5: "cm",
  6: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Pages"
};


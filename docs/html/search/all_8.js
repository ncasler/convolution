var searchData=
[
  ['kernel',['Kernel',['../structKernel.html',1,'']]],
  ['kernel_2ec',['Kernel.c',['../Kernel_8c.html',1,'']]],
  ['kernel_5falloc',['Kernel_alloc',['../Kernel_8c.html#a60ab5d28f79df2b9f3a24bf07e1dfe1e',1,'Kernel.c']]],
  ['kernel_5ffree',['Kernel_free',['../Kernel_8c.html#a8b5e51900b7431515e9dac0cf81d1dd5',1,'Kernel.c']]],
  ['kernel_5fgauss',['Kernel_gauss',['../Kernel_8c.html#af30c3aafbab16fa426c865b59c6a4d31',1,'Kernel.c']]],
  ['kernel_5fget',['Kernel_get',['../Kernel_8c.html#a2985f7d08eac69f0af402ed89201430c',1,'Kernel.c']]],
  ['kernel_5finit',['Kernel_init',['../Kernel_8c.html#a562f32bfd14f417c70e79c0d66e03055',1,'Kernel.c']]],
  ['kernel_5fmean',['Kernel_mean',['../Kernel_8c.html#adc6d3260940c53f9a24ea364c75d2f24',1,'Kernel.c']]],
  ['kernel_5fset',['Kernel_set',['../Kernel_8c.html#ae02fa5f37de62ebdd4ed4a5bbe760b9e',1,'Kernel.c']]],
  ['kernel_5fsobel',['Kernel_sobel',['../Kernel_8c.html#afd7291e22e65c868fff72a238615f647',1,'Kernel.c']]]
];

/** Activation.c
 * Author: Nathan P. Casler <ncasler@illinois.edu>
 * Date: 12/01/2017
 */

/**
 * @file
 * Laundry list of available activation functions for neural networks
 * @see https://en.wikipedia.org/wiki/Activation_function
 */
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include "Util.h"

/** 
 * @brief Identify function, returns self
 * @param x input value
 * @return x
 */
double CONVA_ident(double x)
{
	return x;
}

/**
 * @brief Binary step function
 * @param x input value
 * @return 1 if positive, else 0
 */
int CONVA_step(double x)
{
	if (x < 0)
		return 0;
	else
		return 1;
}

/**
 * @brief Soft step (logarithmic) function
 * @param x input value
 * @return \f$\frac{1}{1+exp(-x)}\f$
 */
double CONVA_log(double x)
{
	return 1 / (1 + exp(-x));
}

/** 
 * @brief Hyperbolic Tangent activation function
 * @param x input value
 * @return \f$tanh(x)\f$
 */
double CONVA_tanh(double x)
{
	return tanh(x);
}

/**
 * @brief Arc tangent activation function
 * @param x input value
 * @return \f$arctan(x)\f$
 */
double CONVA_atan(double x)
{
	return atan(x);
}

// Soft sign
/**
 * @brief Soft-sign activation function
 * @param x input value
 * @return \f$\frac{x}{1+abs(x)}
 */
double CONVA_softsign(double x)
{
	return x / (1 + absolute(x));
}

/**
 * @brief Rectifier activation function
 * @see https:/en.wikipedia.org/wiki/Rectifier_(neural_networks)
 * @param x
 * @return x if x > 0, else 0
 */
double CONVA_relu(double x)
{
	if (x < 0)
		return 0;
	else
		return x;
}




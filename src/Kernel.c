/**
 * @file
 * @brief Functions needed for kernel convolution and window functions
 * @author Nathan Casler
 * @date 16 October 2017
 */
#include <stdio.h>
#include <stdlib.h>
#include "Kernel.h"
#include <string.h>
#include <math.h>
#include "Util.h"

/**
 * @brief Initialize a Kernel instance
 * @param kernel Targer Kernel instance
 * @return void
 */
void Kernel_init(Kernel *kernel) {
	kernel->cols = 0;
	kernel->rows = 0;
	kernel->coef = 0.0f;
	kernel->data = NULL;
}

/**
 * @brief Create a 3x3 Sobel Kernel
 * @param axis X if 0, else Y
 * @param kern Destination Kernel instance
 * @return 0
 */
int Kernel_sobel(int axis, Kernel *kern) {
	kern->cols = 3;
	kern->rows = 3;
	kern->coef = 4;
	Kernel_alloc(kern);
	if (axis == 0) // X Axis
	{
		float values[9] = {1, 0, -1, 2, 0, -2, 1, 0, -1};
		memcpy(kern->data, values, sizeof(values));
	} else // Y Axis
	{
		float values[9] = { 1, 2, 1, 0, 0, 0, -1, -2, -1};
		memcpy(kern->data, values, sizeof(values));
	}
	return 0;
}

// Function to calculate the number of pixels needed for Sigma 
// Scraped from http://blog.demofox.org/2015/08/19/gaussian-blur/
// TODO: Verify veracity of this function
/**
 * @brief Calculate the diameter necessary to represent a gaussian
 * @param sigma Designated sigma for gaussian distribution
 * @return Representative diameter for a gaussian
 * @see http://blog.demofox.org/2015/08/19/gaussian-blur
 */
int PixelsNeededForSigma(float sigma) 
{
	// Returns the number of pixels needed to represent a gaussian kernel that has values
	// down to the threshold amound. A gaussian function technically has values everywhere
	// on the image but the threshold lets us cut it off where the pixels contribute to
	// only small amounts that aren't noticeable
	const float c_threshold = 0.005f; // 0.5%
	int pxcount =  (int) floor(1.0f + 2.0f * sqrtf(-2.0f * sigma * sigma * log(c_threshold))) + 1;
	return pxcount;
}

/**
 * @brief Generate gaussian kernel
 * @param sigma Sigma defining the gaussian
 * @param kern Destination kernel instance
 * @return 1
 */
int Kernel_gauss(float sigma, Kernel *kern) 
{
	int radius = ceil(3 * sigma);
	int diam = 2 * radius + 1;
	kern->cols = diam;
	kern->rows = diam;
	printf("Kernel dims [%i, %i]\n", kern->cols, kern->rows);
	//float sum = 0.0f;
	//float r = 0.0f;
	//float s = 2.0 * sigma * sigma;
	float val = 0.0;
	Kernel_alloc(kern);
	int i = 0, j = 0;
	for (j = -radius; j <= radius; j++) 
	{
		for (i = -radius; i <= radius; i++)
		{
			val = gauss(i, j, sigma);
			Kernel_set(i+radius, j + radius, kern, val);
		}
	}
	kern->coef = 1;
	printf("Kernel Coef: %f\n", kern->coef);
	return kern->coef;
}

/**
 * @brief Generate a mean kernel estimator
 * @warning Deprecated. Use Grid_mean_win
 * @param radius Desired kernel radius
 * @kern  Destination Kernel instance
 * @return 1
 */
int Kernel_mean(int radius, Kernel *kern) {
	kern->cols = radius * 2 + 1;
	kern->rows = radius * 2 + 1;
	Kernel_alloc(kern);
	kern->coef = kern->cols * kern->rows;
	int i = 0;
	int j = 0;
	for (i = 0; i < kern->rows; i++) {
		for (j = 0; j < kern->cols; j++) {
			Kernel_set(j, i, kern, 1);
		}
	}
	return 1;
}

/**
 * @brief Allocate pixels for Kernel instance
 * @param kern  Destination Kernel instance
 * @return 1 if success, else -1
 */
int Kernel_alloc(Kernel *kern) {
	if (kern->data != NULL) {
		Kernel_free(kern);
	}
	if (kern->cols > 0 && kern->rows > 0) {

		kern->data = malloc(sizeof(float) * kern->cols * kern->rows);
		return 1;
	} else  // Invalid dims
	{
		return -1;
	}
}

/**
 * @brief Free memory allocated to Kernel
 * @param kern Kernel instance to free
 * @return void
 */
void Kernel_free(Kernel *kern) {
	if (kern->data != NULL) {
		free(kern->data);
		kern->data = NULL;
	}
}

/**
 * @brief Get pixel value of kernel
 * @param col   Column index
 * @param row   Row index
 * @param kern  Source Kernel instance
 * @return If success pixel value, else 0
 */
float Kernel_get(int col, int row, const Kernel *kern) {
	if ((col >= 0 && col < kern->cols) && (row >= 0 && row < kern->rows)) {
		int idx = row * kern->cols + col;
		return kern->data[idx];
	} else {
		return 0;
	}
}

/**
 * @brief Set pixel value of kernel
 * @parm col   Column index
 * @param row  Row index
 * @param kern Kernel Instance
 * @param val  Desired pixel value
 * @return if success 1, else -1
 */
int Kernel_set(int col, int row, Kernel * kern, float val) {
	if ((col >= 0 && col < kern->cols) && (row >= 0 && row < kern->rows)) {
		int idx = row * kern->cols + col;
		kern->data[idx] = val;
	} else {
		return -1;
	}
	return 1;
}


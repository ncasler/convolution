/** @file
 * @author Nathan Casler
 * @date 15 October 2017
 * @brief File containing function definitions for pixel arrays
 */
#include <stdlib.h>
#include <stdio.h>
#include "Grid.h"
#include "Kernel.h"
#include "Util.h"
#include <math.h>
#include <gdal.h>
#include <string.h>

/**
 * @brief Initialize a Grid instance
 *
 * @param grid Destination Grid instance
 * @return void
 */
void Grid_init(Grid *grid) {
	grid->cols = 0;
	grid->rows = 0;
	grid->data = NULL;
	grid->nodata = -9999;
};

/**
 * @brief Allocate pixels for Grid
 * 
 * If the grid instance is a non-null pointer, the grid instance will be freed
 * before it is allocated.
 * @param grid Grid defining pixel dimensions
 * @return 1 on success, else -1
 */
int Grid_alloc(Grid *grid) {
	if (grid->data != NULL) {
		Grid_free(grid);
	}
	if (grid->cols < 1 || grid->rows < 1) {
		printf("Invalid grid dimensions");
		return -1;
	} else {
		printf("Allocating %zu MB\n", (grid->cols * grid->rows * sizeof(float))/1000000);
		grid->data = malloc(sizeof(float) * grid->cols * grid->rows);
		return 1;
	}
}

/**
 * @brief Free memory allocated for Grid instance
 * @param Source Grid instance
 * @return void
 */
void Grid_free(Grid* grid) {
	grid->cols = 0;
	grid->rows = 0;
	if (grid->data != NULL) {
		free(grid->data);
	}
	grid->nodata = 0;
}

/**
 * @brief Copy constructor for grid instances, shallow or full
 * @param g1 Source Grid instance
 * @param g2 Destination Grid instance 
 * @param full If non-zero, do full copy of pixel data, else leave pixels uninitialized
 * @return 0
 */
int Grid_copy(Grid *g1, Grid *g2, int full) {
	g2->cols = g1->cols;
	g2->rows = g1->rows;
	Grid_alloc(g2);
	if (full != 0) {
		memcpy(g2->data, g1->data, sizeof(float) * g2->cols * g2->rows);
	}
	return 0;
}

/** 
 * @brief Retrieve a pixel value from Grid
 * @note This function uses mirroring on borders so out-of-bound indexes are
 * reflected to their internal index values.
 * @param col Column index for pixel
 * @param row Row index for pixel
 * @param grid Source Grid instance
 * @return Pixel data value (float32)
 */
float Grid_get(int col, int row, const Grid *grid) {
	float val = 0;
	int dif;
	// Implement edge mirroring
	if (col < 0)
	{
		col = abs(col);
	}
	if (row < 0) {
		row = abs(row);
	}
	if (col > grid->cols)
	{ 
		dif = grid->cols - col;
		col = grid->cols - dif;
	}
	if (row > grid->rows)
	{
		dif = grid->rows - row;
		row = grid->rows - dif;
	}
			int idx = row * grid->cols + col;
			// Conditional to make sure this fits in the buffer
			if (idx > 0 && idx < grid->cols * grid->rows)
				val =  grid->data[idx];
		return val;
}

/**
 * @brief Set the value of a Grid pixel
 * @param col Column index of pixel
 * @param row Row index of pixel
 * @param grid Destination Grid instance
 * @param val Specified pixel value
 * @note This function will return -1 if column or row indexes are out of bounds.
 * @return 1 on succes, else -1
 */
int Grid_set(int col, int row, Grid *grid, float val) {
	if ((col >= 0 && col < grid->cols) && (row >= 0 && row < grid->rows)) {	
		int idx = row * grid->cols + col;
		grid->data[idx] = val;
	} else {
		printf("Error: cell :[%i,%i] not available in [%i,%i] grid\n", col, row,grid->cols, grid->rows);
		return -1;
	}
	return 1;
}
// Currently based off of 3x3 neighborhood
/**
 * @brief Create binary grid of zero-crossings
 * 
 * A zero-crossing occurs in a neighborhood where both positive and negative
 * values are found. This can be used as an edge-detector on Laplacian derivatives.
 * @see https://en.wikipedia.org/wiki/Zero_crossing
 * @param in_grid Source Grid instance
 * @param out_grid Destination Grid instance
 * @note currently uses 3x3 neighborhood for calculations
 * @return 1
 */
int Grid_zero_cross(const Grid *in_grid, Grid *out_grid) {
	int i = 0;
	int j = 0;
	int ii = 0;
	int jj = 0;
	int pos = 0;
	int neg = 0;
	int radius = 1;
	float grid_val = 0.0;
	for (j = 0; j < in_grid->rows; j++) {
		for (i = 0; i < in_grid->cols; i++) {
			pos = 0;
			neg = 0;
			for (jj = -radius; jj <= radius; jj++) {
				for (ii = -radius; ii <= radius; ii++) {
					grid_val = Grid_get(i + ii, j+jj, in_grid);
					if (grid_val > 0.0) {
						pos++;
					} else if (grid_val < 0.0) {
						neg++;
					} else { // Value is nodata or 0,do nothing
						continue;
					}
				}
			}
			// Check parity
			if (pos > 0 && neg > 0) { // ZeroCrossing
				Grid_set(i, j, out_grid, 1);
			} else {
				Grid_set(i, j, out_grid, out_grid->nodata);
			}
		}
	}
	return 1;
}

/**
 * @brief Calculate Laplacian Derivative from Grid
 * @see https://en.wikipedia.org/wiki/Laplace_operator
 * @param in_grid Source Grid instance
 * @param out_grid Destination Grid instance
 * @warning May experience edge effects since Grid_get mirrors edges
 * @return 1
 */
int Grid_laplace(const Grid *in_grid, Grid *out_grid) 
{
	float accum = 0.0f;
	int i = 0;
	int j = 0;
	for (j = 0; j < in_grid->rows; j++) 
	{
		for (i = 0; i < in_grid->cols; i++)
		{
			accum = 0.0f;
			accum += Grid_get(i - 1, j, in_grid);
			accum += Grid_get(i + 1, j, in_grid);
			accum += Grid_get(i, j - 1, in_grid);
			accum += Grid_get(i, j + 1, in_grid);
			accum -= 4 * Grid_get(i, j, in_grid);
			//accum /= (dist * dist);
			Grid_set(i, j, out_grid, accum);
		}
	}
	return 1;
}

/**
 * @brief Calculate ridge strength factor
 * @warning Deprecated
 * Function is currently deprecated in favor of ridge() function
 * @param x First order derivative in x direction
 * @param y First order derivative in y direction
 * @param xx Second-order derivative in x direction
 * @param xy Second-order diagonal derivative
 * @param yy Second-order derivative in y direction
 * @see https://dsp.stackexchange.com/questions/1714/best-way-of-segmenting-veins-in-leaves
 * \f[  -\frac{y^{2}x'' + 2x'y'xy'' - x^{2}y''}{y^{3}}
 * \f]
 * @return Ridge strength (float32)
*/
float calc_ridge(float x, float y, float xx, float xy, float yy)
{
	float num = - ((y*y)*xx + 2 * x * y * xy - (x * x) * yy);
	float denom = y * y * y;
	return num / denom;
}

/** 
 * @brief Calculate Ridge Strength factor
 * @param xx Second-order x derivative
 * @param xy Second-order diagonal derivative
 * @param yy Second-order y derivative
 * @return Ridge strength factor
 */
float ridge(float xx, float xy, float yy)
{
	double sub = (xx*xx) + 4*(xy*xy) - 2 * (xx * yy) + (yy * yy);
	double out = (xx + yy + sqrt(sub)) / 2;
	return (float)out;
}

/**
 * @brief Convolve a kernel across a grid
 * @param in_grid Source Grid instance
 * @param kern    Kernel instance
 * @param out_grid Destination Grid instance
 * @return 0
 */
int Grid_convolve(const Grid *in_grid, const Kernel *kern, Grid *out_grid){
	int i = 0;
	int j = 0;
	int ii = 0;
	int jj = 0;
	float accumulation = 0;
	float grid_val = 0;
	float kernel_val = 0;
	int radius = floor(kern->cols / 2);
	float nodata = (float)in_grid->nodata;
	for (j = 0; j < in_grid->rows; j++) {
		for (i = 0; i < in_grid->cols; i++) {
			for (jj = -radius; jj <= radius; jj++) {
				for (ii = -radius; ii <= radius; ii++) {
					grid_val = Grid_get(i + ii, j+jj, in_grid);
					if (!almostEqualFloat(grid_val, nodata, 2)) {
						kernel_val = Kernel_get(radius + ii, radius + jj, kern);
					//printf("Grid[%i,%i] val: %f, Kern[%i,%i] val:%f\n", j, i, grid_val, jj,ii, kernel_val); 
						accumulation += grid_val * kernel_val;
					}
				}
			}
		float new_val = accumulation / kern->coef;
		//printf("New grid val: %f / %f = %f\n", accumulation, kern->coef,new_val);
		Grid_set(i, j, out_grid, new_val);
		
		
		
		accumulation = 0;
		}
	}
	return 0;
}

// NOTE: AS OF YET THE DISTANCE THRESHOLD DOES NOTHING. TODO: IMPLEMENT DISTANCE
// threshold for IDW
/**
 * @brief Perform inverse distance weighting interpolation on grid
 * @note Currently the distance threshold is not utilized
 * @warning Deprecated. Current implementation has errors and is replaced by null_fill
 * @param in_grid Source Grid instance
 * @param n_neighbors Desired number of neighbors
 * @param distance   Maximum distance threshold
 * @param power  Weighting coefficient
 * @param out_grid Destination Grid instance
 * @return 0
 */
int Grid_IDW(const Grid *in_grid, int n_neighbors, float distance, int power, Grid *out_grid) {
	int i = 0;
	int j = 0;
	int ii = 0;
	int jj = 0;
	int n_cnt = 0;
	int neigh_max = n_neighbors;
	int radius = 0;
	float neighbors[n_neighbors][2];
	float grid_val = 0.0f;
	float sum_weights = 0.0f;
	float sum_vals = 0.0f;
	float dist = 0.0f;
	float nodata = (float) in_grid->nodata;
	printf("Found %f nodata value\n", nodata);
	for (j = 0; j < in_grid->rows; j++) {
		for (i = 0; i < in_grid->cols; i++) {
			grid_val = Grid_get(i, j, in_grid);
			if (almostEqualFloat(grid_val, nodata, 2)) {
			//if (grid_val == in_grid->nodata) {
				radius = 0;
				n_cnt = 0;
				for (ii = 0; ii < neigh_max; ii++) {
					neighbors[ii][0] = 0.0f;
					neighbors[ii][1] = 0.0f;
				}
				radius = 3;
				for (jj = -radius; jj <= radius && n_cnt < neigh_max; jj++) {
					for (ii = -radius; ii <=radius && n_cnt < neigh_max; ii++) {
						grid_val = Grid_get(i +ii, j + jj, in_grid);
						if (!almostEqualFloat(grid_val, nodata,2) && !almostEqualFloat(grid_val, 0.0f, 2)) {
						//if (grid_val != in_grid->nodata && grid_val != 0.0f) {
							//printf("Checking %i, %i value %f\n", j+jj, i+ii, grid_val);
							dist = sqrt(abs(pow(ii,2)) + abs(pow(jj,2)));
							if (dist != 0.0) {
							dist = 1 /pow(sqrt(abs(pow(ii,2)) + abs(pow(jj,2))), power) ;
							neighbors[n_cnt][0] = grid_val * dist;
							neighbors[n_cnt][1] = dist;
							n_cnt++;
							//printf("n_cnt %i : %i\n", n_cnt, neigh_max);
							}
						}
					}
				
				}// Found necessary neighbors
				sum_weights = 0.0f;
				sum_vals = 0.0f;
				for (n_cnt = 0; n_cnt < neigh_max; n_cnt++) {
					sum_vals += neighbors[n_cnt][0];
					sum_weights += neighbors[n_cnt][1];
				}
				grid_val = sum_vals / sum_weights;
				//printf("%f / %f = %f new grid val\n", sum_vals, sum_weights, grid_val);
			}
			Grid_set(i, j, out_grid, grid_val);

		}
	}
	return 0;
}

/**
 * @brief Perform map arithmetic to subtract one Grid from another
 * @param in_grid_1 Source Grid to be subtracted from
 * @param in_grid_2 Source Grid to subtract
 * @param out_grid Destination Grid
 * @return 0
 */
int Grid_subtract(const Grid *in_grid_1, const Grid *in_grid_2, Grid *out_grid) 
{
	int i = 0;
	int j = 0;
	float val = 0;
	for (j = 0; j < out_grid->rows; j++) 
	{
		for (i = 0; i < out_grid->cols; i++)
		{
			val = Grid_get(i, j, in_grid_1) - Grid_get(i, j, in_grid_2);
			Grid_set(i, j, out_grid, val);
		}
	}
	return 0;
}

/** 
 * @brief Calculate the mean cell value for the Grid 
 * @param in_grid Source Grid instance
 * @return Mean value
 */
float Grid_mean(const Grid *in_grid) {
	int i = 0;
	int j = 0;
	float sum = 0;
	int count = 0;
	float tmp = 0.0f;
	for (j = 0; j < in_grid->rows; j++) {
		for (i = 0; i < in_grid->cols; i++) {
			tmp = Grid_get(i, j, in_grid);
			if (!almostEqualFloat(tmp, (float)in_grid->nodata, 1)) {
				sum += (float)tmp;
				count++;
			}
		}
	}
	return sum / count;
}

/**
 * @brief Calculate the cell value variance for a Grid
 * @param in_grid Source Grid instance
 * @param mean Mean pixel value
 * @return Variance
 */
float Grid_var(const Grid *in_grid, const float mean) {
	int i = 0;
	int j = 0;
	float accum = 0.0f;
	float tmp = 0.0f;
	int count = 0;
	for (j = 0; j < in_grid->rows; j++) {
		for (i = 0; i < in_grid->cols; i++) {
			tmp = Grid_get(j, i, in_grid);
			if (!almostEqualFloat(tmp, (float)in_grid->nodata, 1)) {
				accum += pow(tmp - mean, 2);
				count++;
			}
		}
	}
	return accum / count;
}

/** 
 * @brief Calculate the standard deviation for a Grid
 * @warning Deprecated, literally just applies square root to input
 * @param var Variance for Grid
 * @return Standard deviation
 */
float Grid_stdev(const float var) {
	return sqrt(var);
}

/**
 * @brief Calculate the square of a grid 
 * Raises all values to an exponent of 2
 * @param in_grid Source Grid instance
 * @param out_grid Destination Grid instance
 * @return 1
 */
int Grid_square(const Grid *in_grid, Grid *out_grid) {
	int i = 0;
	int j = 0;
	float tmp = 0;
	float nodata = (float)in_grid->nodata;
	for (j = 0; j < out_grid->rows; j++) {
		for (i = 0; i < out_grid->cols; i++) {
			tmp = Grid_get(j, i, in_grid);
			if (!almostEqualFloat(tmp, nodata, 2)){
			//if (tmp != in_grid->nodata) {
				Grid_set(i, j, out_grid, pow(tmp, 2));
			} else {
				Grid_set(i, j, out_grid, nodata);
			}
		}
	}
	return 1;
}

/**
 * @brief Read a Grid from a GDAL-compliant file
 * @param filePath File path to GDAL-compliant dataset
 * @param grid Destination Grid instance
 * @return 0 on success, -1 on IOError
 */
int Grid_read(const char *filePath, Grid* grid) {
	GDALDatasetH hDataset;
	GDALAllRegister();
	GDALRasterBandH hBand;
	int cols;
	int rows;

	hDataset = GDALOpen(filePath, GA_ReadOnly);
	if (hDataset == NULL ) {
		fprintf(stderr, "Failed to open raster at %s\n", filePath);
		exit(-1);
	}
	hBand = GDALGetRasterBand( hDataset, 1);
	cols = GDALGetRasterBandXSize(hBand);
	rows = GDALGetRasterBandYSize(hBand);
	grid->cols = cols;
	grid->rows = rows;
	printf("Band has type %s\n", GDALGetDataTypeName(GDALGetRasterDataType(hBand)));
	printf("Allocating memory\n");
	Grid_alloc(grid);
	printf("Allocation finished\n");
	GDALRasterIO(hBand, GF_Read, 0, 0, cols, rows, grid->data, cols, rows, GDT_Float32, 0, 0);
	printf("IO Finished\n");
	GDALClose(hDataset);
	return 0;
}

/**
 * @brief Downsample a grid to 2x pixel size
 * @param in_grid Source Grid instance
 * @param out_grid Destination Grid instance
 * @warning This methodology may suffer from edge effects
 * @return 0
 */
int Grid_downsample(const Grid *in_grid, Grid *out_grid) {
	int i = 0;
	int j = 0;
	int ii = 0;
	int jj = 0;
	float sum = 0.0f;
	float avg = 0.0f;
	float tmp = 0.0f;
	int cntr = 0;
	int block_len = 2;
	float nodata = (float) in_grid->nodata;
	out_grid->cols = ceil(in_grid->cols / block_len);
	out_grid->rows = ceil(in_grid->rows / block_len);
	out_grid->nodata = in_grid->nodata;
	Grid_alloc(out_grid);
	for (j = 0; j < out_grid->rows; j++) {
		for (i = 0; i < out_grid->cols; i++) {
			cntr = 0;
			sum = 0.0f;
			avg = 0.0f;
			for (jj = 0; jj < block_len; jj++) {
				for (ii = 0; ii < block_len; ii++) {
					tmp = Grid_get(block_len * i + ii, block_len * j +jj, in_grid);
					if (!almostEqualFloat(tmp, nodata, 2)) {
						cntr++;
						sum += tmp;
					}
				}
			}
			if (cntr > 0) {
				avg = sum / cntr;
			} else {
				avg = out_grid->nodata;
			}
			Grid_set(i, j, out_grid, avg);
			
		}
	}
	return 0;
}

/** 
 * @brief Calculate the neighborhood median value for a given pixel
 * @param in_grid Source Grid instance
 * @param i       Column index
 * @param j       Row index
 * @param radius  Search neighborhood radius
 * @return        Neighborhood median value
 */
float Grid_median(const Grid *in_grid, int i, int j, int radius) {
	int ii = 0;
	int jj = 0;
	int diam = (2 * radius) + 1;
	int radsq = pow(diam, 2);
	float in_list[radsq];
	//float out_list[radsq];
	int cntr = 0;
	float tmp = 0.0f;
	for (jj = -radius; jj <= radius; jj++) {
		for (ii = -radius; ii <= radius; ii++) {
			//printf("Getting value (%i,%i), radius = %i, of (%i,%i)\n", i+ii, j+jj, radius, in_grid->cols, in_grid->rows);
			
			tmp = Grid_get(i + ii, j+jj, in_grid);
			if (!almostEqualFloat(tmp, (float)in_grid->nodata, 1) && !almostEqualFloat(tmp, 0.0f, 1)) {
				in_list[cntr] = tmp;
				cntr++;
				//printf("[%i] added %f\n", cntr, tmp);
			}
		}
	}
	
	//printf("Sorting %i elements\n", cntr);
	//quickSortFloat(in_list, 0, cntr-1);
	qsort(in_list, cntr-1, sizeof(float), compareFloat);
		
	int med_cntr = (int)floor(cntr/2); // find middle element, is Floor proper here?
	//printf("Median is %i/%i\n", med_cntr, cntr);
	float med = in_list[med_cntr];
	//printf("Median: %f\n", med);
	return med;
	//printf("Input params: (%i,%i) r= %i\n", j, i, radius);
	//return 1.0f;
}

/**
 * @brief Greyscale Morphologic erosion
 * @param in_grid Source Grid instance
 * @param i   Column index
 * @param j   Row index
 * @param k   Structuring kernel
 * @return    Minimum value within structure kernel
 */
float Grid_erode(const Grid *in_grid, int i, int j, Kernel *k) {
	int ii = 0;
	int jj = 0;
	int cntr = 0;
	float in_list[k->cols * k->rows];
	float tmp = 0.0f;
	float k_tmp = 0.0f;
	float min = FLT_MAX;
	int halfcol = floor(k->cols / 2) + 1;
	int halfrow = floor(k->rows / 2) + 1;
	// TODO(Nathan): Could make this more efficient by only reading values 
	// corresponding to non-zero kernel entries
	for (jj = -halfrow; jj <= halfrow; jj++) 
	{
		for (ii = -halfcol; ii <= halfcol; ii++) 
		{
			// Check for out of bound errors on indexes(due to rounding errors)
			if (jj + halfrow > 0 && jj + halfrow < k->rows) 
			{
				if (ii + halfcol > 0 && ii + halfcol < k->cols) 
				{
					// Check that kernel value is greater than zero(would be 1)
					k_tmp = Kernel_get(ii + halfcol, jj + halfrow, k);
					if (k_tmp > 0) 
					{
						tmp = Grid_get(i + ii, j + jj, in_grid);
						// Check that value ! nodata
						if (!almostEqualFloat(tmp, (float)in_grid->nodata, 1)) 
						{
							in_list[cntr] = tmp;
							cntr++;
						}
					}
				}
			}
		}
	}
	for (ii = 0; ii < cntr; ii++) {
		if (in_list[ii] < min) {
			min = in_list[ii];
		}
	}
	return min;
}

/**
 * @brief Greyscale morphologic dilation
 * @param in_grid Source Grid instance
 * @param i       Column index
 * @param j       Row index
 * @param k       Structuring Kernel
 * @return  Maximum value masked by structuring kernel
 */
float Grid_dilate(const Grid *in_grid, int i, int j, Kernel *k) {
	int ii = 0;
	int jj = 0;
	int cntr = 0;
	float in_list[k->cols * k->rows];
	float tmp = 0.0f;
	float k_tmp = 0.0f;
	float max = -FLT_MAX;
	int halfcol = floor(k->cols / 2) + 1;
	int halfrow = floor(k->rows / 2) + 1;
	// TODO(Nathan): Could make this more efficient by only reading values 
	// corresponding to non-zero kernel entries
	for (jj = -halfrow; jj <= halfrow; jj++) 
	{
		for (ii = -halfcol; ii <= halfcol; ii++) 
		{
			// Check for out of bound errors on indexes(due to rounding errors)
			if (jj + halfrow > 0 && jj + halfrow < k->rows) 
			{
				if (ii + halfcol > 0 && ii + halfcol < k->cols) 
				{
					// Check that kernel value is greater than zero(would be 1)
					k_tmp = Kernel_get(ii + halfcol, jj + halfrow, k);
					if (k_tmp > 0) 
					{
						tmp = Grid_get(i + ii, j + jj, in_grid);
						// Check that value ! nodata
						if (!almostEqualFloat(tmp, (float)in_grid->nodata, 1)) 
						{
							in_list[cntr] = tmp;
							cntr++;
						}
					}
				}
			}
		}
	}
	for (ii = 0; ii < cntr; ii++) {
		if (in_list[ii] > max) {
			max = in_list[ii];
		}
	}
	return max;
}

/**
 * @brief Lee filter (Adaptive mean)
 * @param in_grid Source Grid instance
 * @param i       Column index
 * @param j       Row index
 * @param radius  Neighborhood search radius
 * @param g_var   Global variance measure
 * @return Weighted mean value
 */
float Grid_Lee(const Grid* in_grid, int i, int j, int radius, float g_var) 
{
	//printf("Calulating Lee val (%i,%i)\n", i, j);
	int k_win = pow(1 + (2* radius), 2);
	float data[k_win];
	int jj = 0;
	int ii = 0;
	int cntr = 0;
	float tmp = 0.0f;
	float sum = 0.0f;
	float l_mean = 0.0f;
	float l_var = 0.0f;
	float l_med = 0.0f;
	float weight = 0.0f;
	float out = 0.0f;
	for (jj = -radius; jj <= radius; jj++) 
	{
		for (ii = -radius; ii <=radius; ii++)
		{
			tmp = Grid_get(i+ii, j+jj, in_grid);
			if (!almostEqualFloat(tmp, (float)in_grid->nodata, 1) && !almostEqualFloat(tmp, 0.0, 1)) 
			{
				data[cntr] = tmp;
				sum += tmp;
				cntr++;
			}
		}
	}
	l_mean = sum / (cntr);
	// Get median
	qsort(&data, cntr, sizeof(float), compareFloat);
	int med_idx = (int)floor(cntr/2);
	l_med = data[med_idx];
	// Calculate variance
	sum = 0.0f;
	for (ii = 0; ii < cntr; ii++) 
	{
		sum += (data[ii] - l_mean)*(data[ii] - l_mean);
	}
	l_var = sum / cntr;
	// calculate weight
	weight = l_var / (l_var + g_var);
	// If only 1 point is found, no reason to calculate var
	if (cntr < 2)
		out = (float)in_grid->nodata;
	else
		out = l_mean + weight * (l_med - l_mean);
	return out;
}
	

/**
 * @brief Neighborhood mean value
 * @param in_grid Source Grid instance
 * @param i       Column index
 * @param j       Row index
 * @param radius  Search neighborhood radius
 * @return    Neighborhood mean value
 */
float Grid_mean_win(const Grid* in_grid, int i, int j, int radius) {
	int ii = 0;
	int jj = 0;
	float sum = 0.0f;
	int cntr = 0;
	float tmp = 0.0f;
	for (jj = -radius; jj <= radius; jj++) {
		for (ii = -radius; ii <= radius; ii++) {
				tmp = Grid_get(i+ii, j+jj, in_grid);
				if (!almostEqualFloat(tmp, (float)in_grid->nodata, 1) && !almostEqualFloat(tmp, 0.0f, 1)) {
					sum += tmp;
					cntr++;
				}
		}
	}
	if (cntr == 0) {
		return 0.0f;
	}
	return sum / cntr;
}

/**
 * @brief Window(neighborhood) variance
 * @param in_grid Source Grid instance
 * @param i       Column index
 * @param j       Row index
 * @param radius  Neighborhood search radius
 * @param mean    Neighborhood mean value
 * @return     Neighborhood variance value
 */
float Grid_var_win(const Grid* in_grid, int i, int j, int radius, float mean) {
	int ii = 0;
	int jj = 0;
	//int radsq = pow(radius, 2);
	float accum = 0.0f;
	float tmp = 0.0f;
	//float mean = Grid_mean_win(in_grid, i, j, radius);
	int count = 0;
	for (jj = -radius; jj < in_grid->rows; jj++) {
		for (ii = 0; ii < in_grid->cols; ii++) {
			tmp = Grid_get(i+ii, j+jj, in_grid);
			if (!almostEqualFloat(tmp, (float)in_grid->nodata, 1)) {
				accum += pow(tmp - mean, 2);
				count++;
			}
		}
	}
	if (count == 0) {
		return 0.0f;
	}
	return accum / count;
}

/**
 * @brief Second derivative in the gradient direction
 * @param x First-order x derivative
 * @param y First-order y derivative
 * @param xx Second-order x derivative
 * @param xy Second-order diagonal derivative
 * @param yy Second-order y derivative
 * @return Derivative in direction of gradient
 */
float sdgd(float x, float y, float xx, float xy, float yy)
{
	double tmp = 0.0f;
	double num = xx * (x * x) + 2 * xy * x * y + yy * (y * y);
	double denom = (x * x) + (y * y);
	tmp = num / denom;
	return (float)tmp;
}


/**
 * @file
 * Utility functions for library
 * @author Nathan Casler
 * @date 17 October 2017
 */
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <assert.h>
#include <math.h>
#include "Util.h"
#include <stdint.h>
/**
 * @brief Check for near-equality in floating-point numbers
 * This function uses integer comparison to check distance between floats
 * should allow us to determine if the values are close to the nodata value
 *
 * @see http://www.cygnus-software.com/papers/comparingfloats/Comparing%20floating%20point%20numbers.html
 * @param maxUlps: the maximum error in terms of Units in the Last Place
 * @return 1 if near-equal, else 0
 */
int almostEqualFloat(float a, float b, int maxUlps) {
	// make sure maxUlps is non-negative and small enough that the
	// default NAN won't compare as equal to anything.
	assert(maxUlps > 0 && maxUlps < 4 * 1024 * 1024);
	int a_int = *(int32_t*)&a;
	// Make aInt lexicographically ordered as a twos-complement int
	if (a_int < 0)
		a_int = 0x80000000 - a_int;
	// Make b_int lexicographically ordered as a twos-complement int
	int b_int = *(int32_t*)&b;
	if (b_int < 0)
		b_int = 0x80000000 - b_int;
	int int_diff = abs(a_int - b_int);
	if (int_diff <= maxUlps)
		return 1;
	return 0;
}

/**
 * @brief Naive floating-point comparison check for use in qsort
 * @param a Pointer to first value
 * @param b Pointer to second value
 * @return 1 if a > b, 0 if equal, -1 if a < b
 */
int compareFloat(const void * a, const void * b)
{
	float fa = *(const float*)a;
	float fb = *(const float*)b;
	return (fa > fb) - (fa < fb);
}

/**
 * @brief Integer comparison check for use in qsort
 * @param a pointer to first integer
 * @param b pointer to second integer
 * return 1 if a > b else 0
 */
int compareInt(const void * a, const void * b)
{
	return a > b;
}

/**
 * @brief Nibble-level endianness swap
 * @note Probably few actually applications unless someone changed order
 * of data at the nibble(4bit) level.
 * @param a Unsigned 32 bit integer to swap
 * @return Unsigned 32 bit integer where all nibbles have been swapped
 */
uint32_t nibb_swap32(uint32_t a) {
	uint32_t res = 0;
	uint32_t b0,b1,b2,b3,b4,b5,b6,b7;
	b0 = (a & 0x0000000f) << 28;
	b1 = (a & 0x000000f0) << 20;
	b2 = (a & 0x00000f00) << 12;
	b3 = (a & 0x0000f000) << 4;
	b4 = (a & 0x000f0000) >> 4;
	b5 = (a & 0x00f00000) >> 12;
	b6 = (a & 0x0f000000) >> 20;
	b7 = (a & 0xf0000000) >> 28;
	res = b0 | b1 | b2 | b3 | b4 | b5 | b6 | b7;
	return res;
}

/**
 * @brief Byte-level endianness swap
 * @note May be useful if data is saved on machine with different endianness.
 * @param a Source integer to swap
 * @return Unsigned 32-bit integer swapped at every 8bits
 */
uint32_t byte_swap32(uint32_t a) {
	uint32_t res = 0;
	uint32_t b0,b1,b2,b3;
	b0 = (a & 0x000000ff) << 24u;
	b1 = (a & 0x0000ff00) << 8u;
	b2 = (a & 0x00ff0000) >> 8u;
	b3 = (a & 0xff000000) >> 24u;
	res = b0 | b1 | b2 | b3;
	return res;
}

/**
 * @brief Relative equality comparison
 * @deprecated in favor fo almostEqualFloat due to limitations around 0
 * @param a first float
 * @param b second float
 * @param maxRelDiff Equality threshold
 * @return 1 if the difference is less than threshold, else 0
 */
int relativeEqualFloat(float a, float b, float maxRelDiff)
{
	// Calculate the difference
	float diff = fabs(a-b);
	a = fabs(a);
	b = fabs(b);
	float largest = (b > a) ? b : a;
	if (diff <= largest * maxRelDiff)
		return 1;
	return 0;
}
	

/**
 * @brief Calculate the gaussian value of a given pixel
 * @param x Column index
 * @param y Row index
 * @param sigma Sigma coefficient defining gaussian function
 * @return value of gaussian at \f$(x,y)\f$
 */
float gauss(int x, int y, float sigma)
{
	float tmp = 0.0f;
	double sigsq = sigma*sigma;
	double coef = 1 / (2 * M_PI * sigsq);
	double suff = exp(-((x*x) + (y*y))/ (2 * (sigsq)));
	tmp = (float)coef * suff;
	return tmp;
}

/**
 * @brief Absolute value of double-precision float
 * @param x Input double
 * @return \f$abs(x)\f$
 */
double absolute(double x) {
	if (x < 0) 
		return -x;
	else
		return x;
}

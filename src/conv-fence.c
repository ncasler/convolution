/** conv-fence.c
 * Author: Nathan P. Casler <ncasler@illinois.edu>
 * Date: 12/01/2017
 */

/* -*- Mode: C; c-basic-offset:4 ; -*- */
/** @file
 * List of workflow specific steps. These will often include communication
 * and iteration
 */
#include <mpi.h>

#include "conv.h"

static int above_LRows, left_LCols, right_LCols;

static MPI_Win matrix_win, temp_win;

/**
 * @brief memory window for Asynchronous Remote Memory Access
 */
typedef struct mem_win {
	void *mem; /** Pointer to memory window **/
	MPI_Win win; /** MPI Window object used to open and close access **/
	int above_LRows; /** Number of ghost rows above(North) patch **/
	int left_LCols;  /** Number of ghost columns left of patch **/
	int right_LCols; /** Number of ghost columns right of patch **/
} mem_win;

static mem_win mem_win_map[2];

/**
 * @brief Initialize Window and ghost arrays for one-sided communication
 * This function passes ghost arrays using point-to-point communication which
 * will suffer froma peformance penalty since each send is reliant on a matching
 * receive. As the number of processes grows, this will likely cause a deadlock.
 * @param patch Local Patch instance
 * @param m1 First 2-D array
 * @param m2 Second 2-D array 
 * @param privateData Pointer to memory
 * @return MPI_SUCCESS if sucess else MPI_ERR
 */
int CONV_ExchangeInitFence( CONVPatchDesc *patch,
			float **m1, float **m2, void *privateData )
{
	int    err = MPI_SUCCESS;
	//int    tmp_GFirstRow, tmp_GFirstCol;
	int    nprocs;

	int LRows = patch->lni;
	int LCols = patch->lnj;

	/* We save information in a global area above - we could instead
	 * allocate memory for it, and return a pointer to that memory.
	 * See the pt2ptUV code for an example of that use. */
	*(void **)privateData = (void *)0;

	/* create windows */
	/* Note that the meshes are passes as the pointers to the rows,
	 * but the rows are contiguous in memory */
	MPI_Win_create(m1[0], (LRows+2)*(LCols+2)*sizeof(float), 
				sizeof(float), MPI_INFO_NULL, patch->comm, &matrix_win);

	MPI_Win_create(m2[0], (LRows+2)*(LCols+2)*sizeof(float),
			sizeof(float), MPI_INFO_NULL, patch->comm, &temp_win);

	/* Store the mapping from memory address to associated window */

	mem_win_map[0].mem = m1[0];
	mem_win_map[0].win = matrix_win;
	mem_win_map[1].mem = m2[0];
	mem_win_map[1].win = temp_win;

	/* for one-sided communication, we need to know the number of
	 * local rows in rank above and the number of local columns
	 * in rank left and right in order to do
	 * the puts into the right locations in memory. */

	MPI_Comm_size(patch->comm, &nprocs);

	if (patch->up == MPI_PROC_NULL)
	{
		above_LRows = 0;
	} else {
		MPI_Recv(&above_LRows, 1, MPI_INT, patch->up, 0, patch->comm, 
				MPI_STATUS_IGNORE );
	}
	if (patch->down != MPI_PROC_NULL)
	{
		MPI_Send( &above_LRows, 1, MPI_INT, patch->down, 0, patch->comm );
	}

	if (patch->left == MPI_PROC_NULL)
		left_LCols = 0;
	else {
		MPI_Recv( &left_LCols, 1, MPI_INT, patch->left, 0, patch->comm,
				MPI_STATUS_IGNORE );
		MPI_Send( &patch->lnj, 1, MPI_INT, patch->left, 0, patch->comm );
	}

	if (patch->right == MPI_PROC_NULL)
		right_LCols = 0;
	else {
		/* this send matches the recv from the left, above */
		MPI_Send( &patch->lnj, 1, MPI_INT, patch->right, 0, patch->comm );
		/* This recv matches the send from the left, above */
		MPI_Recv( &right_LCols, 1, MPI_INT, patch->right, 0, patch->comm,
				MPI_STATUS_IGNORE );
	}

	return err;
}

/** 
 * @brief Free the windows allocated for this communication
 * @param privateData pointer to window memory
 * @return 0
 */
int CONV_ExchangeEndFence(void *privateData)
{
	MPI_Win_free(&matrix_win);
	MPI_Win_free(&temp_win);
	return 0;
}

/**
 * @brief Initialize Window and ghost arrays for one-sided communication
 * This function passes ghost arrays to neighbors using a 2-step trick to avoid
 * deadlock and diagonal communication. The left and right arrays are passed, a
 * fence is called to finish communication, then the top and bottom arrays are 
 * sent.
 * @param patch Local Patch instance
 * @param matrix Source 2-D array
 * @param timedata Timing tracker
 * @param privateData pointer to memory
 * @return MPI_SUCCESS or MPI_ERR
 */
 int CONV_ExchangeFence( CONVPatchDesc *patch, float **matrix,
			CONVTiming *timedata, void *privateData )
{
	int err = MPI_SUCCESS;
	MPI_Aint disp;
	int rank;
	MPI_Comm_rank(patch->comm, &rank);
	int LRows = patch->lni;
	int LCols = patch->lnj;
	
	static MPI_Datatype mytype     = MPI_DATATYPE_NULL;
	static MPI_Datatype left_type  = MPI_DATATYPE_NULL;
	static MPI_Datatype right_type = MPI_DATATYPE_NULL;
	
	MPI_Win win;

	/* Find the right window object */
	if (mem_win_map[0].mem == &matrix[0][0])
		win = mem_win_map[0].win;
	else
		win = mem_win_map[1].win;

	/* Create datatype if not already created */
	if (mytype == MPI_DATATYPE_NULL) {
		MPI_Type_vector(LRows, 1, LCols+2, MPI_FLOAT, &mytype);
		MPI_Type_commit(&mytype);
	}
	if (left_type == MPI_DATATYPE_NULL) {
		MPI_Type_vector(LRows, 1, left_LCols+2, MPI_FLOAT, &left_type);
		MPI_Type_commit(&left_type);
	}
	if (right_type == MPI_DATATYPE_NULL) {
		MPI_Type_vector(LRows, 1, right_LCols+2, MPI_FLOAT,
				&right_type);
		MPI_Type_commit(&right_type);
	}

	MPI_Win_fence(MPI_MODE_NOPRECEDE, win);

	/* first put the left, right edges */

	disp = (left_LCols + 2) + (left_LCols + 1);
	MPI_Put(&matrix[1][1], 1, mytype, patch->left, disp, 1,
			left_type, win);

	disp = right_LCols + 2;
	MPI_Put(&matrix[1][LCols], 1, mytype, patch->right, disp, 1,
			right_type, win);

	/* Complete the right/left/ transfers for the diagonal trick */
	MPI_Win_fence( 0, win );

	/* now put the top, bottom edges (including the diagonal
	 * points) */
	printf("[%i] Putting %i floats up(%i) and down %i\n", rank, LCols+2, patch->up, patch->down);
	MPI_Put(&matrix[1][0], LCols + 2, MPI_FLOAT, patch->up,
			(LRows+1)*(LCols+2), LCols+2, MPI_FLOAT, win);
	
	MPI_Put(&matrix[LRows][0], LCols + 2, MPI_FLOAT, patch->down, 0,
			LCols+2, MPI_FLOAT, win);
	

	//MPI_Win_fence(MPI_MODE_NOSTORE | MPI_MODE_NOPUT |
	//		MPI_MODE_NOSUCCEED, win);
	MPI_Win_fence(0, win);

	return err;
}

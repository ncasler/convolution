/** @file 
 * @author Nathan Casler
 * @date 15 October 2017
 * @brief File containing function definitions for Raster class
 */
#include <stdlib.h>
#include <math.h>
#include "Grid.h"
#include "Raster.h"
#include "gdal.h"
#include "cpl_string.h"
#include "cpl_conv.h" // for CPLMalloc
#include "ogr_srs_api.h"
#include <string.h>

/** void Raster_init
 * @brief Initialize a new Raster instance
 *
 *  @param raster The Raster instance to initialize
 *  @return void
 */
void Raster_init(Raster *raster) {
	memset(raster->proj4, 0, 1024);
	int i = 0;
	for (i = 0; i < 6; i++) {
		raster->affine[i] = 0;
	}
	Grid_init(&raster->grid);
}
/**
 *  @brief Read raster metadata into Raster instance, but not pixel data
 *
 *  @param in_file  File path to GDAL compliant dataset
 *  @param raster   Raster object to store metadata
 *  @return 1 if success else -1
 */
int Raster_stat(char *in_file, Raster *raster) {
	GDALDatasetH hDataset;
	//GDALDriverH hDriver;
	GDALRasterBandH hBand;
	GDALAllRegister();
	OGRErr err;
	OGRSpatialReferenceH hSRS;
	char *proj_str = NULL;
	hDataset = GDALOpen( in_file, GA_ReadOnly );
	if ( hDataset == NULL )
	{
		fprintf(stderr, "IO Error: Failed to open raster %s\n", in_file);
		return -1;
	}
	// Get GeoTransform
	GDALGetGeoTransform(hDataset, &raster->affine[0]);
	// Get SRS
	memset( &raster->proj4, 0, 1024 );
	if ( GDALGetProjectionRef ( hDataset ) != NULL )
	{
		proj_str = (char *) GDALGetProjectionRef(hDataset);
		hSRS = OSRNewSpatialReference(NULL);
		if (OSRImportFromWkt( hSRS, &proj_str ) == OGRERR_NONE )
		{
			err = OSRExportToProj4( hSRS, &proj_str );
			if ( err != OGRERR_NONE ) 
				fprintf(stderr,"Failed to copy projection %i error\n", err);
			strcpy( raster->proj4, proj_str);
		}
		OSRDestroySpatialReference(hSRS);
	}
	Grid_init( &raster->grid );
	raster->grid.data = NULL;
	raster->grid.cols = GDALGetRasterXSize(hDataset);
	raster->grid.rows = GDALGetRasterYSize(hDataset);
	hBand = GDALGetRasterBand( hDataset, 1);
	int noDataFlag = 0; // TODO: Deal with this flag. Flag to see if there is a noData value
	raster->grid.nodata = GDALGetRasterNoDataValue( hBand, &noDataFlag );
	GDALClose(hDataset);
	return 1;
}

/**
 *  @brief Read GDAL compliant raster data into a Raster instance
 *  @param in_file  File path to GDAL-compliant raster
 *  @param raster   Destination Raster instance
 *  @return 1 if success else -1
 */
int Raster_read(char *in_file, Raster *raster) {
	GDALDatasetH hDataset;
	//GDALDriverH hDriver;
	GDALRasterBandH hBand;
	GDALAllRegister();
	OGRErr err;
	OGRSpatialReferenceH hSRS;
	char *proj_str = NULL;
	printf("Opening File at %s\n", in_file);
	hDataset = GDALOpen( in_file, GA_ReadOnly) ;
	if (hDataset == NULL )
	{ 
		fprintf(stderr, "IO Error: Failed to open raster at %s\n", in_file);
		return -1;
	}
	// Geotransform stuff
	GDALGetGeoTransform(hDataset, &raster->affine[0]);
	// SRS Stuff, needlessly complex....
	memset(&raster->proj4, 0, 1024);
	if ( GDALGetProjectionRef( hDataset) != NULL )
	{
	
		proj_str  = (char *)GDALGetProjectionRef(hDataset);
		hSRS = OSRNewSpatialReference(NULL);
		printf("WKT is %s\n", proj_str);
		if (OSRImportFromWkt( hSRS, &proj_str) == OGRERR_NONE )
		{
			
			err = OSRExportToProj4(hSRS, &proj_str);
				
			if (err != OGRERR_NONE) {
				printf("Failed to copy projection %i err\n", err);
			}
			strcpy(raster->proj4, proj_str);
			printf("Projection is: \n%s\n", raster->proj4);
			//free(&proj_str);
			printf("Freed memory\n");
		}
		else
			printf("Coordinate System is `%s'\n", GDALGetProjectionRef(hDataset));
		
		OSRDestroySpatialReference(hSRS);
	}
	// Allocate the necessary grid
	Grid_init(&raster->grid);
	raster->grid.data = NULL;
	raster->grid.cols = GDALGetRasterXSize(hDataset);
	raster->grid.rows = GDALGetRasterYSize(hDataset);
	printf("Allocating grid\n");
	Grid_alloc(&raster->grid);
	// Read the data
	hBand = GDALGetRasterBand( hDataset, 1);
	int noDataFlag = 0; // TODO: Deal with this flag. Flag to see if there is a noData value
	raster->grid.nodata = GDALGetRasterNoDataValue( hBand, &noDataFlag );
	
	GDALRasterIO(hBand, GF_Read, 0, 0, raster->grid.cols, raster->grid.rows, 
			raster->grid.data, raster->grid.cols, raster->grid.rows, 
			GDT_Float32, 0, 0);

	GDALClose(hDataset);
	return 1;
};

/**
 *  @brief Copy constructor for Raster instances, may be full or shallow.
 *  @param r1  Source Raster instance
 *  @param r2  Destination Raster instance
 *  @param full Copy pixel data if true, else leave empty
 *  @return 0
 */
int Raster_copy(Raster *r1, Raster *r2, int full) {
	strcpy(r2->proj4, r1->proj4);
	memcpy(&r2->affine, &r1->affine, sizeof(double) * 6);
	Grid_copy(&r1->grid, &r2->grid, full);
	r2->grid.nodata = r1->grid.nodata;
	return 0;
}

/**
 *  @brief Write a raster instance out to GeoTiff file
 *  @param out_file Destination file path
 *  @param raster   Source raster instance
 *  @return 0
 */
int Raster_write(char *out_file, Raster* raster) {
	GDALDatasetH hDstDS;
	GDALRasterBandH hBand;
	OGRSpatialReferenceH hSRS;
	GDALDriverH hDriver;
	char *pszSRS_WKT = NULL;
	// Creation options here
	char **papszOptions = NULL;
	// Generate the Dataset, GTiff default as of now
	hDriver = GDALGetDriverByName("GTiff");
	hDstDS = GDALCreate(hDriver, out_file, raster->grid.cols, 
			raster->grid.rows, 1, GDT_Float32, papszOptions);
	//Geo Transform stuff
	GDALSetGeoTransform( hDstDS, raster->affine );
	// SRS Stuff
	hSRS = OSRNewSpatialReference( NULL );
	OSRImportFromProj4(hSRS, raster->proj4);
	OSRExportToWkt( hSRS, &pszSRS_WKT );
	OSRDestroySpatialReference( hSRS );
	GDALSetProjection( hDstDS, pszSRS_WKT );
	CPLFree( pszSRS_WKT );
	hBand = GDALGetRasterBand( hDstDS, 1 );
	int i = 0, j = 0;
	for (i = 0; i < 30; i++) {
		printf("[");
		for (j = 0; j < 30; j++) {
			printf("%.3f, ", Grid_get(j, i, &raster->grid));
		}
		printf("]\n");
	}
	GDALSetRasterNoDataValue(hBand, raster->grid.nodata);
	GDALRasterIO(hBand, GF_Write, 0, 0, raster->grid.cols, 
			raster->grid.rows, raster->grid.data, raster->grid.cols,
			raster->grid.rows, GDT_Float32, 0, 0);
	GDALClose( hDstDS );
	return 0;
}

/**
 *  @brief Downsample a raster instance so the resolution in x and y are 1/2
 *
 *  @detail Downsampling will use a basic average to downsample the pixels, which
 *  can cause border effects around edges of image. 
 *  @param r1 Source raster instance
 *  @param r2 Destination raster instance
 *  @return 1 if successed, else 0
 */
int Raster_downsample(const Raster *r1, Raster *r2) {
	strcpy(r2->proj4, r1->proj4);
	memcpy(&r2->affine, &r1->affine, sizeof(double) * 6);
	r2->grid.nodata = r1->grid.nodata;
	r2->affine[1] = r1->affine[1] * 2;
	r2->affine[5] = r1->affine[5] * 2;
	int flag = 0;
	flag = Grid_downsample(&r1->grid, &r2->grid);
	return flag;
}
/**
 *  @brief De-initialize and free memory allocated for a raster instance
 *  @param raster Raster instance to deallocate
 *  @return void
 */
void Raster_free(Raster  *raster) {
	Grid_free(&raster->grid);
	memset(raster->proj4, 0, 1024);
	//free(raster);
}

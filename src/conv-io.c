/* -*- Mode: C; c-basic-offset:4; -*- */
/* 
 * @file
 * @author Nathan Casler
 * @date 17 October 2017
 *
 * Borrows heavily from advmpi examples provided by Bill Gropp at ATPESC 2017
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <mpi.h>

#include "conv.h"
#include "conv-io.h"

/* STDOUT Implementation of checkpont (no restart) for MPI Convolution
 *
 * Data output in matrix order: spaces represent dead cells,
 * '*'s represent live ones.
 */

/**
 * @brief Delay process
 * @param msec Number of milliseconds to wait
 * @return void
 */
static void CONVIO_msleep(int msec);

static MPI_Comm convio_comm = MPI_COMM_NULL;

/**
 * @brief Initialize a duplicate communicator for processing
 * @param comm MPI communicator to duplicate
 * @return error flag
 */
int CONVIO_Init(MPI_Comm comm)
{
	int err;

	err = MPI_Comm_dup(comm, &convio_comm);

	return err;
}

/**
 * @brief Free the duplicated communicator
 * @param void
 * @return error flag
 */
int CONVIO_Finalize(void)
{
	int err;

	if (convio_comm != MPI_COMM_NULL) {
		err = MPI_Comm_free(&convio_comm);
	}

	return err;
}

#define MAX_SIZE 256
/**
 * @brief Gather and print data array to stdout
 * @note this is confined to 256x256 pixel array
 * @warning Deprecated. Printing to stdout not useful for large arrays.
 * @param patch Source Patch Instance
 * @param matrix Source 2-D array
 * @param iter Iteration number
 * @param info MPI_Info object for hints
 * @return 0 if success else 1
 */
int CONVIO_Checkpoint(CONVPatchDesc *patch, float **matrix,
		int iter, MPI_Info info)
{
	int err = 0;
	int rank, nprocs, maxcols;

	MPI_Comm_size(convio_comm, &nprocs);
	MPI_Comm_rank(convio_comm, &rank);

	/* 
	 * Check first that the data is not too big for this output
	 */
	MPI_Allreduce(&patch->lnj, &maxcols, 1, MPI_INT, MPI_MAX, convio_comm);
	if (maxcols + 2 > MAX_SIZE || patch->gNJ > MAX_SIZE) {
		if (rank == 0) {
			fprintf(stderr, "Maximum width(y) exceeded for stdout output, Max is %d, this run has a max of %d\n",
					MAX_SIZE, patch->gNJ);
		}
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	/* To ensuze that there are no errors in updating the display, 
	 * the data is sent to process 0 who writes it to the stdout.
	 * To avoid having the root process keep a copy of the entire
	 * (global) array, we step through the global rows, with
	 * each process sending information about its row (using the
	 * tag for the row).
	 */
	if (rank == 0)
	{
		char cbuf[MAX_SIZE];
		int buf[MAX_SIZE];
		int np, row, i;
		int npcols = patch->pNJ;

		printf("^[[H^[2J# Iteration %d\n", iter );
		for (row=1; row<= patch->gNI; row++) {
			/* Clear the cbuf */
			for (i = 0; i<patch->gNJ; i++)
			{
				cbuf[i] = ' ';
			}
			/* We know how many processes there are in each row */
			np = 0;
			/* Does this process contribute to this row? */
			if (row >= patch->gI && row < patch->gI + patch->lni) {
				for (i=0; i < patch->lnj; i++)
				{
					cbuf[i+patch->gJ-1] =
						matrix[row-patch->gI+1][i+1] ? '*' : ' ';
				}
				np ++;
			}
			while (np < npcols) {
				MPI_Recv( buf, MAX_SIZE, MPI_FLOAT, MPI_ANY_SOURCE, row,
						convio_comm, MPI_STATUS_IGNORE );
				/* for each entry in buf that is set, set the 
				 * corresponding element in cbuf. buf[0] is the 
				 * first col index, buf[1] is the number of elements */
				for (i=0; i <buf[1]; i++) {
					cbuf[buf[0]-1+i] = buf[i+2] ? '*' : ' ';
				}
				np ++;
			}
			cbuf[patch->gNJ] = 0;
			/* The odd characters are commands to an xterm/vt100 window */
			printf("^[[%03d;%03dH", patch->gNJ+2, 1);
		}
		fflush(stdout);
	}
	else 
	{
		int buf[MAX_SIZE], i, j, row;

		buf[0] = patch->gJ;
		buf[1] = patch->lnj;
		for (i=1; i <= patch->lni; i++) {
			row = i + patch->gI - 1;
			for (j = 0; j<patch->lnj; j++)
			{
				buf[2+j] = matrix[i][j+1];
			}
			MPI_Send( buf, patch->lnj + 2, MPI_INT, 0, row, convio_comm);
		}
	}
	CONVIO_msleep(250); /* give time to see the result */

	return err;
}

/**
 * @brief Restart iterations
 * @deprecated. Not useful for out current implementation
 * @param patch Source Patch instance
 * @param matrix Source 2-d Array
 * @param info MPI_Info object for hints
 * @return Error
 */
int CONVIO_Restart(CONVPatchDesc *patch, float **matrix, MPI_Info info)
{
	return MPI_ERR_IO;
}

/**
 * @brief Check whether process can be restarted
 * @deprecated Currently not useful for processing pipeline
 * @param void
 * @return 0
 */
int CONVIO_Can_restart(void)
{
	return 0;
}

#ifdef HAVE_NANOSLEEP
#include <time.h>
static void CONVIO_msleep(int msec)
{
	struct timespec t;

	t.tv_sec = msec / 1000;
	t.tv_nsec = 1000000 * (msec - t.tv_sec);

	nanosleep(&t, NULL);
}
#else
static void CONVIO_msleep(int msec)
{
	if (msec < 1000) {
		sleep(1);
	}
	else {
		sleep(msec / 1000);
	}
}
#endif

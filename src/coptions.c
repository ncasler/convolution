/** coptions.c
 * Author: Nathan P. Casler <ncasler@illinois.edu>
 * Date: 12/01/2017
 */

/* -*- Mode: C; c-basic-offset:4 ; -*- */
/** @file
 *  Command line argument parsing functions
 *  @note Command line arguments are not guaranteed in the MPI
 *  environment to be passed to all processes. To be portable, we must process 
 *  on rank 0 and distribute results
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <mpi.h>

#include "conv.h"
#include "conv-io.h"


/**
 * @brief Parse command line arguments
 * @param argc Argument count
 * @param argv Array of arguments
 * @param options Mesh creation option object
 * @return 0
 */
int CONV_ParseArgs(int argc, char **argv, CONVOptions *options)
{
	int          ret, rank, blklens[3];
	MPI_Aint     displs[3];
	MPI_Datatype types[3], argsType;

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	blklens[0] = 8;
	blklens[1] = sizeof(options->dataset)*2;
	blklens[2] = sizeof(options->prefix);
	displs[0] = 0;
	displs[1] = &options->dataset[0] - (char *)&options->gNI;
	displs[2] = &options->prefix[0] - (char *)&options->gNI;
	//displs[0] = 0; displs[1] = &options->prefix[0] - (char *)&options->gNI;
	types[0] = MPI_INT; types[1] = MPI_CHAR; types[2]= MPI_CHAR;
	MPI_Type_create_struct( 3, blklens, displs, types, &argsType );
	MPI_Type_commit( &argsType );

	if (rank == 0) {
		/* Initialize defaults */
		options->gNI = 20;
		options->gNJ = 20;
		options->pNI = 0;
		options->pNJ = 0;
		options->nIter = 10;
		options->verbose = 0;
		options->doIO = 0;
		options->restartIter = -1;
		strncpy(options->outdir, "/tmp", sizeof(options->outdir)-1);
		strncpy(options->dataset, "tmp.tif", sizeof(options->dataset)-1);
		//strncpy( options->dataset, "dset.tif", sizeof(options->dataset) - 1);
		strncpy( options->prefix, "conv", sizeof(options->prefix) - 1);

		while ((ret = getopt(argc, argv, "a:b:x:y:i:f:o:p:r:cv")) >= 0)
		{
			switch(ret) {
				case 'a':
					options->pNJ = atoi(optarg);
					break;
				case 'b':
					options->pNI = atoi(optarg);
					break;
				case 'c':
					options->doIO = 1;
					break;
				case 'x':
					options->gNJ = atoi(optarg);
					break;
				case 'y':
					options->gNI = atoi(optarg);
					break;
				case 'i':
					options->nIter = atoi(optarg);
					break;
				case 'f': 
					strncpy(options->dataset, optarg, sizeof(options->dataset)-1);
					break;
				case 'o':
					strncpy(options->outdir, optarg, sizeof(options->outdir)-1);
					break;
				case 'r':
					options->restartIter = atoi(optarg);
					break;
				case 'p':
					strncpy(options->prefix, optarg, sizeof(options->prefix)-1);
					break;
				case 'v':
					options->verbose = 1;
					break;
				default:
					fprintf(stderr, "\
							\t-a <pj>  - Number of processes in x (j) direction\n\
							\t-b <pi>  - Number of processes in y (i) direction\n\
							\t-c       - Enable I/O (checkpoint)\n\
							\t-x <nj>  - Size of mesh in x (j) direction\n\
							\t-y <ni>  - Size of mesh in y (i) direction\n\
							\t-i <n>   - Numer of iterations\n\
							\t-f <file>- Input file\n\
							\t-r <i>   - Iteration where restart begins (and read restart file)\n\
							\t-o <out> - Directory for output\n\
							\t-p <pre> - Filename prefix for I/O\n\
							\t-v       - Turn on verbose output");
					break;
			}
		}
	}

	MPI_Bcast( options, 1, argsType, 0, MPI_COMM_WORLD );
	MPI_Type_free( &argsType );
	return 0;
}

/**
 * @brief Abort MPI processes gracefully
 * @param str Error message
 * @return void
 */
void CONV_Abort( const char str[] )
{
	fprintf( stderr, "CONV Aborting: %s\n", str );
	MPI_Abort( MPI_COMM_WORLD, 1 );
}

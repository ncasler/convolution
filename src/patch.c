/* -*- Mode: C; c-basic-offset:4 ; -*- */
/**
 * @file
 * @brief basic patch configuration 
 * @author Nathan Casler
 * @date 17 October 2017
 * Heavily sampled from advanced MPI tutorials provided by Bill Gropp at ATPESC
 * @see will
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <stdint.h>
#include "gdal.h"
#include "cpl_conv.h"
#include "ogr_srs_api.h"
#include "Util.h"
#include "conv.h"

/**
 * @brief Create a patch mesh of processors
 * @param options Options for mesh
 * @param path    Target patch definition
 * @return 0
 */
int CONV_PatchCreateProcessMesh( CONVOptions *options, CONVPatchDesc *patch)
{
	int dims[2];
	int up, down, left, right, ul, ur, ll, lr;
	int prow, pcol;
	int nprocs, rank;

	dims[0] = options->pNI;
	dims[1] = options->pNJ;

	patch->comm = MPI_COMM_WORLD;

	MPI_Comm_size( MPI_COMM_WORLD, &nprocs );
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	printf("[%i] Found %i nprocs\n", rank, nprocs);
	// TODO: Decide which axis should be rounded up and which should be floored
	//dims[0] = (int)floor(sqrt((float)nprocs));
	//dims[1] = (int)nprocs / dims[0];
	//dims[1] = (int)floor(sqrt((float)nprocs));
	printf("[%i] Found dims: %i,%i\n", rank, dims[0], dims[1]);
	MPI_Dims_create(nprocs, 2, dims);

	patch->pNI = dims[0];
	patch->pNJ = dims[1];
	
	/*************************************************
	 * Extract metadata from datasets
	 *************************************************/
	printf("Reading dataset %s\n",options->dataset); 
	CONV_ParseDatasetHeader(patch, &options->dataset[0]);


	/* Compute the cartesion coordinates of this process;
	 * number across in row-major order 
	 */

	prow = rank / dims[1];
	pcol = rank % dims[1];
	patch->patchI = prow;
	patch->patchJ = pcol;
	printf("[%i] (%i,%i) of (%i,%i)\n", rank, prow,pcol, dims[0],dims[1]);
	/* Compute the neighbors */
	left = right = up = down = MPI_PROC_NULL;
	ul = ur = ll = lr = MPI_PROC_NULL;

	if (prow > 0)
	{
		up = rank - dims[1];
		if (pcol > 0) 
			ul = up - 1;
		if (pcol < dims[1] - 1) 
			ur = up + 1;
	}
	if (pcol > 0) {
		left = rank - 1;
	}
	if (prow < dims[0]-1) {
		down = rank + dims[1];
		if (pcol > 0) 
			ll = down - 1;
		if (pcol < dims[1] - 1)
			lr = down + 1;
	}
	if (pcol < dims[1]-1) {
		right = rank + 1;
	}
	patch->left = left;
	patch->right = right;
	patch->up    = up;
	patch->down  = down;
	patch->ul    = ul;
	patch->ur    = ur;
	patch->ll    = ll;
	patch->lr    = lr;

	return 0;
}

/**
 * @brief Create cartesian patch processor mesh
 * @param options Options for mesh creation
 * @param patch Destination patch definition
 * @return  0
 */
int CONV_PatchCreateProcessMeshWithCart( CONVOptions *options,
		CONVPatchDesc *patch )
{
	int dims[2], periods[2], coords[2];
	int up, down, left, right, ul, ur, ll, lr;
	//int prow, pcol;
	int nprocs, rank;


	MPI_Comm_size( MPI_COMM_WORLD, &nprocs );

	// TODO: Decide which axis should be rounded up and which should be floored
	dims[0] = options->pNI;
	dims[1] = options->pNJ;


	MPI_Dims_create(nprocs, 2, dims);

	patch->pNI = dims[0];
	patch->pNJ = dims[1];

	/* Extract Raster header information */
	CONV_ParseDatasetHeader(patch, options->dataset);
	//patch->gNI = options->gNI;
	//patch->gNJ = options->gNJ;

	/* Create a cartesian communicator using the recommended (by dims_create)
	 * sizes */
	periods[0] = periods[1] = 0;
	MPI_Cart_create( MPI_COMM_WORLD, 2, dims, periods, 1, &patch->comm );

	/* The ordering of processes, relaitive to the rank in the new
	 * communicator, is defined by the MPI standard. However, there are
	 * useful convenience functions  */

	MPI_Comm_rank( patch->comm, &rank );
	MPI_Cart_coords( patch->comm, rank, 2, coords );
	/* Compute the cartesian coords of this process; 
	 * row-major order 
	 */
	patch->patchI = coords[0]; /* prow */
	patch->patchJ = coords[1]; /* pcols */
	
	/* Get the neighbors. SHift can be used for the coordinate directions */
	MPI_Cart_shift( patch->comm, 0, 1, &up, &down );
	MPI_Cart_shift( patch->comm, 1, 1, &left, &right );
	printf("[%i] (%i,%i) NEIGHBORS: L: %i, R: %i, U: %i, D: %i\n", rank, coords[0],coords[1], left, right, up, down);
	/* For the diagonal processes, we can either:
	 * 1. Use the defined layout to comute them
	 * 2. Communicate with the neighbors in the coordinate directions, who
	 * know those ranks (e.g., as the up neighbor for the ranks of the up
	 * neighbors left and right neighbors).
	 */
	ul = ur = ll = lr = MPI_PROC_NULL;
	if (up != MPI_PROC_NULL) 
	{
		if (left != MPI_PROC_NULL)
			ul = up - 1;
		if (right != MPI_PROC_NULL)
			ur = up + 1;
	}
	if (down != MPI_PROC_NULL) 
	{
		if (left != MPI_PROC_NULL)
			ll = down - 1;
		if (right != MPI_PROC_NULL)
			lr = down + 1;
	}

	patch->left  = left;
	patch->right = right;
	patch->up    = up;
	patch->down  = down;
	patch->ul    = ul;
	patch->ur    = ur;
	patch->ll    = ll;
	patch->lr    = lr;

	return 0;
}

/**
 * @brief Create data decomposition for patch
 * @param options Mesh creation options
 * @param patch Destination Patch instance
 * @return 0
 */
int CONV_PatchCreateDataMeshDesc( CONVOptions *options,
		CONVPatchDesc *patch )
{
	int firstcol, firstrow, lastcol, lastrow;

	/* compute the decomposition of the global mesh.
	 * Thest are for the "active" part of the mesh, and range from
	 * 1 to GRows by 1 to GCols
	 */
	firstcol = 1 + patch->patchJ * (patch->gNJ / patch->pNJ);
	firstrow = 1 + patch->patchI * (patch->gNI / patch->pNI);
	if (patch->patchJ == patch->pNJ - 1)
	{
		lastcol = patch->gNJ;
	}
	else
	{
		lastcol = 1 + (patch->patchJ + 1) * (patch->gNJ / patch->pNJ) - 1;
	}
	if (patch->patchI == patch->pNI - 1) {
		lastrow = patch->gNI;
	}
	else 
	{
		lastrow = 1 + (patch->patchI + 1) * (patch->gNI / patch->pNI) -1;
	}

	patch->gI = firstrow;
	patch->gJ = firstcol;
	patch->lni = lastrow - firstrow + 1;
	patch->lnj = lastcol - firstcol + 1;

	return 0;
}

/**
 * @brief Read Raster metadata from GDAL-compliant raster
 * @param patch Destination Patch instance
 * @parm dset   File path to GDAL-compliant raster
 * @return 0 if success, else -1
 */
int CONV_ParseDatasetHeader(CONVPatchDesc *patch, const char *dset) {
	/*************************************************
	 * Extract metadata from datasets
	 *************************************************/
	OGRErr err;
	OGRSpatialReferenceH hSRS;
	char *wkt_srs = NULL;
	GDALAllRegister();
	patch->hDS = GDALOpen( dset, GA_ReadOnly );
	if (patch->hDS == NULL )
	{
		fprintf(stderr, "IO Err: Failed to open raster %s\n", dset);
		return -1;
	}
	// Get GeoTransform
	GDALGetGeoTransform(patch->hDS, &patch->gAffine[0]);
	// Zero-initialize String
	memset(&patch->proj_str, 0, 1024);
	if ( GDALGetProjectionRef ( patch->hDS ) != NULL )
	{
		wkt_srs = (char *) GDALGetProjectionRef(patch->hDS);
		hSRS = OSRNewSpatialReference(NULL);
		if (OSRImportFromWkt( hSRS, &wkt_srs ) == OGRERR_NONE )
		{
			err = OSRExportToProj4( hSRS, &wkt_srs );
			if (err != OGRERR_NONE ) {
				fprintf(stderr, "Failed to copy projection %i error\n", err);
				return -1;
			}

			strcpy(patch->proj_str, wkt_srs);
		}
		OSRDestroySpatialReference(hSRS);
	}
	patch->gNI = GDALGetRasterYSize(patch->hDS);
	patch->gNJ = GDALGetRasterXSize(patch->hDS);
	patch->nBands = GDALGetRasterCount(patch->hDS);
	return 0;
}

/**
 * @brief Allocate memory for Patch's pixel arrays
 * @param Destination Patch instance
 * @param m1 First array (used as source array in processing)
 * @param m2 Second array (used as destination in processing)
 * Allocate a C-style 2-D array (array of pointers). Allocate a local mesh
 * with ghost cells and as a contiguous block so that stridec access may be
 * used for left/right edges.
 *
 * For simplicity all patches have halo cells on all sides, even if the process
 * shares a physical boundary.
 * @return 0
 */
int CONV_AllocateLocalMesh(CONVPatchDesc *patch, float ***m1, float ***m2)
{
	float **mat, **mat2;
	int i;
	mat = NULL;
	mat  = (float **)malloc( (patch->lni + 2) * sizeof(float*) );
	if (!mat) 
		CONV_Abort( "Unable to allocate matrix ");
	mat[0] = (float*) malloc( (patch->lni+2)*(patch->lnj+2)*sizeof(float));
	if (!mat[0]) 
		CONV_Abort( "Unable to allocate mat[0]" );
	for (i=1; i < patch->lni+2; i++) {
		mat[i] = mat[i-1] + patch->lnj+2;
	}
	*m1 = mat;
	mat2 = NULL;
	mat2  = (float **) malloc( (patch->lni+2) * sizeof(float*) );
	if (!mat2) 
		CONV_Abort( "Unable to allocate matrix2");
	mat2[0] = (float*) malloc( (patch->lni+2)*(patch->lnj+2)*sizeof(float));
	if (!mat2[0])
		CONV_Abort( "Unable to allocate mat2[0]");
	for (i=1; i < patch->lni+2; i++) {
		mat2[i] = mat2[i-1] + patch->lnj+2;
	}
	*m2 = mat2;
	//printf("Allocated output matrix size: (%i,%i)\n",patch->lnj+2, patch->lni+2);
	return 0;
}

/**
 * @brief Allocate a kernel array
 * @param k Pointer for array
 * @param radius Kernel radius
 * @return 0 if success
 */
int CONV_AllocateKernel(float ***k, int radius)
{
	int cols = radius * 2 + 1;
	int rows = radius * 2 + 1;
	float **kernel;
	int i;
	kernel = NULL;
	kernel = (float **)malloc(rows * sizeof(float*));
	if (!kernel)
		CONV_Abort(" Unable to allocate kernel");
	kernel[0] = (float *)malloc(cols * rows * sizeof(float));
	if (!kernel[0])
		CONV_Abort( "Unable to allocate kernel[0]");
	for (i = 1; i< rows; i++)
	{
		kernel[i] = kernel[i-1] + cols;
	}
	*k = kernel;
	return 0;
}

/**
 * @brief Free memory allocated for local arrays
 * @param path Local patch instance
 * @param m1 First array
 * @param m2 Second array
 * @return 0
 */
int CONV_FreeLocalMesh( CONVPatchDesc *patch, float **m1, float **m2)
{
	free( m1[0] );
	free( m1 );
	free( m2[0] );
	free( m2 );
	CONV_FreePatch(patch);
	return 0;
}

/**
 * @brief Free memory allocated for kernel array
 * @param kernel Kernel to free
 * @return 0
 */
int CONV_FreeKernel( float **kernel)
{
	free(kernel[0]);
	free(kernel);
	return 0;
}

/**
 * @brief Initialize local mesh 
   Zero initialize arrays set nodata value, nodata initialize halo.
   Read raster data into m1 array.
 * @param patch Destination patch instance
 * @param m1 First array
 * @param m2 Second array
 * @return 0
 */
int CONV_InitLocalMesh(CONVPatchDesc *patch, float **m1, float **m2)
{
	int i, j;
	int lni = patch->lni, lnj = patch->lnj;
	int gi = patch->gI, gj = patch->gJ;
	GDALRasterBandH  *poBand;
	float nodata;
	int hasNodata;
	double nodataDbl;
	poBand = GDALGetRasterBand(patch->hDS, 1);
	nodataDbl = GDALGetRasterNoDataValue(poBand, &hasNodata);
	nodata = (float) nodataDbl;
	patch->nodata = nodata;
	int scanLen = 0, rowCnt = 0;
	scanLen = patch->gNJ - gj > lnj ? lnj : patch->gNJ - gj;
	rowCnt = patch->gNI - gi > lni ? patch->lni : patch->gNI - gi;
	/* initialize the boundaries of the matrix */
	for (j = 0; j < lnj+2; j++) {
		//printf("Idx: [%i,%i]\n", 0,j);
		m1[0][j] = nodata;
		//printf("Idx: [%i,%i]\n", lni+1,j);
		m1[lni+1][j] = nodata;
		//printf("Idx: [%i,%i]\n", 0, j);
		m2[0][j] = nodata; 
		//printf("Idx: [%i,%i]\n", lni+1, j);
		m2[lni+1][j] = nodata;
	}

	for (i = 0; i < lni+2; i++) {
		m1[i][0] = m1[i][lnj+1] = m2[i][0] = m2[i][lnj+1] = nodata;
	}
	/* Zero-Initialize the grid matrix */
	for (i = 0; i < lni+2; i++) {
		/* Set this to nodata for now */
		for (j = 0; j < lnj+2; j++) {
			m1[i][j] = nodata;
		}
	}

	// Initialize border length within buffer to account for
	// Patches that exceed the raster length
	// Read the data from the input raster
	printf("Reading patch from file\n");
	//printf("Patch Dims (%i,%i) Offset(%i,%i), Border: %i)\n", scanLen, rowCnt,
	//	gj,gi, borderwid);
	GDALRasterIO(poBand, GF_Read, gj, gi, scanLen, rowCnt, &m1[1][1], 
			scanLen, rowCnt, GDT_Float32, 0, sizeof(float)*(lnj+2));

	printf("Successfully read scan patch\n");
	return 0;
}
// Remove negative values, 0s and infinite numbers
/**
 * @brief Set outside valid elevation range to nodata
 * Useful inbetween iterations to remove erroneous values.
 * @param patch Target patch instance
 * @param matrix Array to clean
 * @return number of pixels changed
 */
int CONV_cleanPatch(CONVPatchDesc *patch, float **matrix) {
	int i, j;
	//float nodata = patch->nodata;
	float tmp = 0.0f;
	int cnt = 0;
	for (i = 0; i < patch->lni; i++) 
	{
		for (j = 0; j < patch->lnj; j++) 
		{
			tmp = matrix[i][j];
			if (!isnormal(tmp)) {
				matrix[i][j] = patch->nodata;
				cnt++;
			}
			// Check for elevations above highet known point
			if (tmp > 12000.0f) {
				matrix[i][j] = patch->nodata;
				cnt++;
			}
			//if (tmp < 1) {
			//	matrix[i][j] = patch->nodata;
			//	cnt++;
			//}
		}
	}
	return cnt;
}


/**
 * @brief Nearest neighbor null-filling
 * This function uses a 3x3 neighborhood to fill nulls with a focal mean.
 * Can be applied iteratively to fill voids.
 * @param patch target Patch instance
 * @param matrix Source 2-d array
 * @param row   Row index
 * @param col   Column index
 * @return Average of non-null neighbors if found, else nodata
 */
float CONV_NN(CONVPatchDesc *patch, float **matrix, int row, 
		int col)

{
	//Use 3x3 neighborhood to fill nulls with focal mean
	int i,j;
	float sum = 0;
	int cnt = 0;
	float avg;
	float tmp;
	for (i = -1; i <= 1; i++) 
	{
		if (row +i > 0 && row + i < patch->lni) {
			for (j = -1; j <= 1; j++) 
			{
				if (col + j > 0 && col + j < patch->lnj) {
					if (!(j==0 && i == 0)) {
						tmp = matrix[row+i][col+j];
						if (!relativeEqualFloat(tmp, patch->nodata,
									FLT_EPSILON *4))
						{
							//printf("Found [%i,%i]non-null value %f\n", patch->patchI + row + i, patch->patchJ + col + j, tmp);
							sum += tmp;
							cnt++;
						}
					}
				}
			}
		}
	}
	if (cnt < 1)
		avg = patch->nodata;
	else
		avg = sum / cnt;
	//if (
	return avg;
}

/**
 * @brief Calculate Laplacian of patch pixel
 * 
 * \f[ \Delta f = \frac{\partial^{2}f}{\partial x^{2}} + \frac{\partial^{2}f}{\partial y^{2}}
 * \f]
 * @param patch Source Patch instance
 * @param matrix Source 2-D array
 * @param row    Row index
 * @param col    Column index
 * @return Laplacian value
 */
float CONV_Laplace(CONVPatchDesc *patch, float **matrix, int row, int col)
{
	//int i, j;
	float iplus,iminus,jplus,jminus;
	float tmp;
	// Use dirichlet boundary for now
	if (row + 1 < 0 || row + 1 > patch->lni)
		iplus = 0.0f;
	else
		iplus = matrix[row+1][col];

	if (row - 1 < 0 || row - 1 > patch->lni)
		iminus = 0.0f;
	else 
		iminus = matrix[row-1][col];
	
	if (col + 1 < 0 || col + 1 > patch->lnj)
		jplus = 0.0f;
	else 
		jplus = matrix[row][col+1];

	if (col - 1 < 0 || col - 1 > patch->lnj)
		jminus = 0.0f;
	else 
		jminus = matrix[row][col-1];

	tmp = iplus + iminus + jplus + jminus - 4 * matrix[row][col];
	return tmp;
}

/**
 * @brief Calculate the mixed derivative \f$f_{xy}\f$ of a given patch pixel
 * @param patch  Source Patch instance
 * @param matrix Source 2-D array
 * @param row    Row index
 * @param col    Column index
 * @return if success Mixed derivative value, else 0
 */
float CONV_mixedDerivative(CONVPatchDesc *patch, float **matrix, int row, int col)
{

	//int i, j;
	float ul, ur, ll, lr;
	float nodata = patch->nodata;
	int lni = patch->lni;
	int lnj = patch->lnj; 
	
	if (row - 1 < 0 || row - 1 > lni + 2 || row + 1 < 0 || row + 1 > lni+2)
		return 0.0f;
	if (col - 1 < 0 || col - 1 > lnj + 2 || col + 1 < 0 || col + 1 > lnj+2)
		return 0.0f;

	ul = matrix[row-1][col-1];
	if (!relativeEqualFloat(ul, nodata, FLT_EPSILON*4)) 
		return 0.0f;
	ur = matrix[row-1][col+1];
	if (!relativeEqualFloat(ur, nodata, FLT_EPSILON * 4))
		return 0.0f;
	ll = matrix[row+1][col-1];
	if (!relativeEqualFloat(ll, nodata, FLT_EPSILON * 4))
		return 0.0f;
	lr = matrix[row+1][col+1];
	if (!relativeEqualFloat(lr, nodata, FLT_EPSILON * 4))
		return 0.0f;
	
	return (lr + ul - ll - ur)/4;
}
	

/**
 * @brief Calculate the curvature value for a given patch pixel
 * 
 * Based on the formula for planar implicit curves
 * \f[
 *  \kappa = -\frac{f_{xx}f^{2}_{y} - 2 f_{xy}f_{x}f_{y} + f^{2}_{x}f_{yy}}{(f^{2}_{x} + f^{2}_{y})^{3/2}}
 *  \f]
 * @param patch Source Patch instance
 * @param matrix Source 2-D array
 * @param row Row index
 * @param col Column index
 * @return Curvature value
 */
float CONV_Curvature(CONVPatchDesc *patch, float **matrix, int row, int col)
{
	//int i, j;
	float iplus, iminus, jplus, jminus;
	int lni = patch->lni;
	int lnj = patch->lnj;
	float nodata = patch->nodata;
	float x, y, xx, yy, xy;
	float tmp = 0.0f;
	float num = 0.0;
	float denom = 0.0;
	if (row + 1 < 0 || row + 1 > lni+2)
		iplus = nodata;
	else 
		iplus = matrix[row+1][col];

	if (row - 1 < 0 || row - 1 > lni+2)
		iminus = nodata;
	else 
		iminus = matrix[row-1][col];

	if (col + 1 < 0 || col + 1 > lnj+2)
		jplus = nodata;
	else 
		jplus = matrix[row][col+1];

	if (col - 1 < 0 || col - 1 > lnj+2)
		jminus = nodata;
	else
		jminus = matrix[row][col-1];
	

	if (relativeEqualFloat(jplus, nodata, FLT_EPSILON*4) ||
			relativeEqualFloat(jminus, nodata, FLT_EPSILON * 4) ||
			relativeEqualFloat(iplus, nodata, FLT_EPSILON * 4) ||
			relativeEqualFloat(iplus, nodata, FLT_EPSILON * 4) ||
			relativeEqualFloat(matrix[row][col], nodata, FLT_EPSILON * 4))
		return 0.0f;
	x = (jplus - jminus)/ 2;
	y = (iplus - iminus)/ 2;
	xx = jplus - 2 * matrix[row][col] + jminus;
	yy = iplus - 2 * matrix[row][col] + iminus;
	xy = CONV_mixedDerivative(patch, matrix, row, col);
	//xx2 = xx * xx;
	//yy2 = yy * yy;
	//xy2 = xy * xy;
	//xxyy = xx * yy;
	//rootex = xx2 + (xy2 + xy2 + xy2 +xy2) - ((xxyy + xxyy) + yy2);
	//root = sqrt(rootex);
	//ridgexp = xx + yy + root;
	//ridges = ridgexp / 2;
	num = yy * (x * x) + 2 * xy * x * y + xx * (y * y);
	denom = pow((x * x) + (y * y), 3/2);
	tmp = num / denom;
	return tmp;
	//float grad = (xx + yy);

}

/**
 * @brief Convolve a 2-d weight array with a patch pixel
 * @param patch Source patch instance
 * @param mat Source 2-d array
 * @param weights 2-d Weight array
 * @param radius  Convolution Kernel radius
 * @param row     Row index
 * @param col     Col index
 * @return Convolved value
 */
float CONV_Convolve(CONVPatchDesc *patch, float **mat, float **weights, 
		int radius, int row, int col)
{
	int i,j, ii, jj;
	i = row;
	j = col;
	int rank;
	MPI_Comm_rank(patch->comm, &rank);
	float accum = 0.0f;
	float weight_accum = 0.0f;
	//float grid_val = 0.0f;
	//float kern_val = 0.0f;
	int k_x = 0 ,k_y = 0;
	float nodata = patch->nodata;
	float tmp = 0.0f;
	int cnt = 0;
	cnt = 0;
	accum = 0.0f;
	for (ii = -radius; ii <=radius; ii++)
	{
		for (jj = -radius; jj <= radius; jj++) 
		{
			if (i+ii < 0 || i + ii > patch->lni+2)
				k_y = i - ii;
			else 
				k_y = i + ii;
			if (j +jj < 0 || j + jj > patch->lnj+2)
				k_x = j - jj;
			else 
				k_x = j + jj;
			//printf("[%i]Looking for value %i,%i, of %i,%i\n", rank, k_x,k_y, patch->lni,patch->lnj);
			if (k_y > 0 && k_y < patch->lni+2 && k_x > 0 && k_x < patch->lnj+2) 
			{
				tmp = mat[k_y][k_x];
				if (!relativeEqualFloat(tmp, patch->nodata, FLT_EPSILON*4))
				{
					//printf("[%i]Adding value %f at idx: %i,%i, of %i\n", rank,tmp, ii+radius,jj+radius,radius);
					accum += tmp * weights[ii+radius][jj+radius];
					weight_accum += weights[ii+radius][jj+radius];
					cnt++;
				}
			}
		}
	}
	if (cnt < 1)
		return nodata;

	//printf("Found value: %f/ %f = %f\n", accum, weight_accum, accum/weight_accum);
	return accum/weight_accum;
}


/* Fill nulls based on Shephards Inverse Distance Weighting formula */
/**
 * @brief Fill nulls in neighborhood using Inverse Distance Weighting
 * Formula uses Shephards inverse Distance Weighting formula.
 * @note Implementation currently has errors.
 * @param patch Source patch instance
 * @param matrix Source 2-D array
 * @param row   Row index
 * @param col   Column index
 * @param radius Neighborhood search radius
 * @param power  Weighting exponent coefficient
 * @return Weighted value
 */
float CONV_IDW(CONVPatchDesc *patch, float **matrix, int row, 
		int col, int radius, int power) {
	int i, j;
	float nbors[radius*radius];
	float dist[radius*radius];
	int nbor_cnt = 0;
	double accum = 0.0f;
	float nodata = patch->nodata;
	float weight = 0.0f;
	float weight_acc = 0.0f;
	int lni = patch->lni, lnj = patch->lnj;
	int rank;
	unsigned int ui;
	//int cols = patch->gNJ - patch->patchJ * lnj;
	//int rows = patch->gNI - patch->patchI * lni;
	MPI_Comm_rank(patch->comm, &rank);
	printf("COMPARING VALUE %f\n", matrix[row][col]);
	for (i = -radius; i <= radius; i++) 
	{
		// Check that this is within grid bounds
		if (row + i >= 0 && row +i < lni+1) 
		{
			for (j = -radius; j <= radius; j++)
			{
				// Check that this is within the grid bounds
				if (col + j >= 0 && col + j < lnj +1)
				{
					if (!relativeEqualFloat(matrix[row+i][col+j],nodata, 
								FLT_EPSILON)) {
						if (!(col == 0 && row == 0)) {
							//	if ((!almostEqualFloat(matrix[row+i][col+j], nodata, 3)) && 
							//			!almostEqualFloat(matrix[row+i][col+j], 0.00, 7)
							{

								nbors[nbor_cnt] = matrix[row+i][col+j];
								dist[nbor_cnt] = sqrt(abs(row - i)*abs(row-i) + 
										abs(col-j)*abs(col-j));
								memcpy(&ui, &matrix[row+i][col+j], sizeof(ui));

								dist[nbor_cnt] = 1.0f;
								//printf("[%i]Global coordinate: %i,%i\n", rank, row+i, col+j);
								printf("Found val %x, %f distance %f\n", ui, nbors[nbor_cnt], dist[nbor_cnt]);
								nbor_cnt++;
							}
						}
					}
				}
			}
		}
	}
	for (i = 0; i < nbor_cnt; i++)
	{
		weight = 1 / pow(dist[i], power);
		accum += nbors[i] / pow(dist[i], power);
		weight_acc += weight;
	}
	printf("[%i,%i] found %i neighbors\n", row, col,nbor_cnt);
	printf("Output function: %f / %f = %f\n", accum, weight_acc, accum/weight_acc);
	if (weight_acc > 0)
		return (float)accum / weight_acc;
	else 
		return (float) accum / nbor_cnt;
}

/**
 * @brief Write local patch data to a GeoTiff dataset
 * @param opts Mesh creation options
 * @param patch Source patch instance
 * @param mat   Source 2-D array to write
 * @param iter   Iteration count for use in output dataset name
 * @return 0
 */
int CONV_WriteLocalMesh(CONVOptions *opts, CONVPatchDesc *patch, float **mat, 
		int iter) {
	int id = patch->patchI * patch->pNJ + patch->patchJ;
	char outname[1024];
	sprintf(outname, "%s/%s_%i_%i.tif", opts->outdir, opts->prefix, id, iter);
	//printf("Output File is %s\n", outname);
	double lAffine[6];
	GDALDatasetH hDstDS;
	GDALRasterBandH hBand;
	OGRSpatialReferenceH hSRS;
	GDALDriverH hDriver;
	char *pszSRS_WKT = NULL;
	// Creation options here
	char **papszOptions = NULL;
	// Generate the Dataset, GTiff default as of now
	hDriver = GDALGetDriverByName("GTiff");
	hDstDS = GDALCreate(hDriver, outname, patch->lnj, patch->lni, 1, 
			GDT_Float32, papszOptions);
	//Geo transform stuff
	//Get the local top left corner
	lAffine[0] = patch->gAffine[0] + patch->gJ * patch->gAffine[1] + 
		patch->gI * patch->gAffine[2];
	lAffine[3] = patch->gAffine[3] + patch->gI * patch->gAffine[4] + 
		patch->gI * patch->gAffine[5];
	// Pixel width/height
	lAffine[1] = patch->gAffine[1];
	lAffine[2] = patch->gAffine[2];
	// Pixel rotation coefficients
	lAffine[4] = patch->gAffine[4];
	lAffine[5] = patch->gAffine[5];
	GDALSetGeoTransform(hDstDS, lAffine );
	// SRS stuff
	hSRS = OSRNewSpatialReference( NULL );
	OSRImportFromProj4(hSRS, patch->proj_str);
	printf("Importing projection %s\n", patch->proj_str);
	OSRExportToWkt( hSRS, &pszSRS_WKT );
	OSRDestroySpatialReference( hSRS );
	GDALSetProjection( hDstDS, pszSRS_WKT );
	CPLFree( pszSRS_WKT );
	hBand = GDALGetRasterBand(hDstDS, 1);
	GDALSetRasterNoDataValue(hBand, patch->nodata );
	GDALRasterIO(hBand, GF_Write, 0, 0, patch->lnj, patch->lni, &mat[1][1], 
			patch->lnj, patch->lni, GDT_Float32, 0, sizeof(float)*(patch->lnj+2));
	GDALClose( hDstDS );
	return 0;
}

/**
 * @brief Calculate the mean for a patch's array data
 * @param patch Target Patch instance
 * @param mat   Source 2-D array
 * @return Mean of array data
 */
float CONV_PatchMean(CONVPatchDesc *patch, float **mat) {
	int i = 0, j = 0;
	float sum = 0;
	int count = 0;
	float tmp = 0.0f;
	for (i = 1; i < patch->lni+1; i++) {
		for (j = 1; j < patch->lnj+1; j++) {
			tmp = mat[i][j];
			if (!relativeEqualFloat(tmp, patch->nodata, FLT_EPSILON)) {
				sum += tmp;
				count++;
			}
		}
	}
	return sum / count;
}

/** 
 * @brief Calculate variance of patch data
 * @param patch Target patch instance
 * @param mat  Source 2-D array to read from
 * @param mean Mean for source array
 * @return Patch variance
 */
float CONV_PatchVariance(CONVPatchDesc *patch, float **mat, const float mean)
{
	int i = 0, j = 0;
	float accum = 0.0f;
	float tmp = 0.0f;
	int count = 0;
	for (i = 1; i < patch->lni+1; i++) {
		for (j = 1; j < patch->lnj+1; j++) {
			tmp = mat[i][j];
			if (!relativeEqualFloat(tmp, patch->nodata, FLT_EPSILON)) {
				accum += pow(tmp - mean, 2);
				count++;
			}
		}
	}
	return accum / count;
}

/**
 * @brief Apply Lee filter to patch
 * The Lee filter is designed to eliminate speckle noise while preserving edges
 * and point features in radar imagery. Based on a linear speckle noise model 
 * and the minimum mean square error (MMSE) design approach, the filter produces
 * the enhanced data according to
 * \f[ \widehat{I}_{s} = \overline{I}_{s} + k_{s}(I_{s} - \overline{I}_{s}) \f]
 *
 * Where: \f$\overline{I}_{s}\f$ is the local mean value within window \f$\eta_{s}\f$
 * and \f$k_{s}\f$ is the adaptive filter coefficient.
 *
 * \f[ k_{s} = 1 - \frac{C^{2}_{u}}{C^{2}_{s}} \f]
 *
 * And 
 * \f[ C^{2}_{s} = \frac{1}{\vert\eta_{s}\vert}\sum_{p\in \eta} \frac{(I_{p} - \overline{I}_{s})^{2}}{(I_{p} - \overline{I}_{s})^{2}} \f]
 *
 * and \f$C^{2}_{u}\f$ is a constant for a given imange and can be determined by
 *
 * \f[ C_{u}^{2} = \frac{var(z')}{(\overline{z}')^{2}} \f]
 * 
 * Where \f$var(z')\f$ is the intensity variance and \f$\overline{z}'\f$is the mean over a homogenous area of the image.
 *
 * @see http://www.cs.virginia.edu/~lgs9a/rodinia/heartwall/srad/paper_2.pdf
 * @param patch Target patch instance
 * @param mat   Source 2-d Array to read from
 * @param i     Column index
 * @param j     Row index
 * @param radius Neighborhood search radius
 * @param g_var  Global variance measure for patch
 * @return Weighted neighborhood mean
 */
float CONV_Lee(CONVPatchDesc *patch, float **mat, int i, int j, int radius, float g_var)
{
	int k_win = pow(1 + (2*radius), 2);
	float data[k_win];
	int jj = 0;
	int ii = 0;
	int k_x = 0, k_y = 0;
	int rank = 0;
	MPI_Comm_rank(patch->comm, &rank);
	int cntr = 0;
	float tmp = 0.0f;
	float sum = 0.0f;
	float l_mean = 0.0f;
	float l_var = 0.0f;
	float l_med = 0.0f;
	float weight = 0.0f;
	float out = 0.0f;
	for (ii = -radius; ii <=radius; ii++)
	{
		for (jj = -radius; jj <= radius; jj++) 
		{
			if (i+ii < 0 || i + ii > patch->lni+2)
				k_y = i - ii;
			else 
				k_y = i + ii;
			if (j +jj < 0 || j + jj > patch->lnj+2)
				k_x = j - jj;
			else 
				k_x = j + jj;
			//printf("[%i]Looking for value %i,%i, of %i,%i\n", rank, k_x,k_y, patch->lni,patch->lnj);
			if (k_y > 0 && k_y < patch->lni+2 && k_x > 0 && k_x < patch->lnj+2) {
			tmp = mat[k_y][k_x];
			if (!relativeEqualFloat(tmp, patch->nodata, FLT_EPSILON))
			{
			//	printf("[%i]Adding value %f at idx: %i, of %i\n", rank,tmp, cntr,k_win);
				data[cntr] = tmp;
				sum += tmp;
				cntr++;
			}
			}
		}
	}
	//printf("[%i]Found %i elements, sum %f\n", rank, cntr, sum );
	if (cntr < 2)
		return patch->nodata;
	l_mean = sum / cntr;
	// Get median
	//printf("Sorting %i floats\n", cntr);
	qsort(&data, cntr, sizeof(float), compareFloat);
	int med_idx = (int)ceil(cntr/2);
	//printf("Found median idx: %i, val: %f\n", med_idx, data[med_idx]);
	l_med = data[med_idx];
	// Calculate variance
	sum = 0.0f;
	for (ii = 0; ii < cntr; ii++)
	{
		sum += (data[ii] - l_mean) * (data[ii] - l_mean);
	}
	//calculate weight
	weight = l_var / (l_var + g_var);
	// If only 1 point is fount, no reason to calculate var
		out = l_mean + weight * (l_med - l_mean);

	return out;
}


/** 
 * @brief Free memory allocated to a Patch instance
 * @param patch Patch instance
 * @return void
 */
void CONV_FreePatch(CONVPatchDesc *patch) 
{
	memset(patch->proj_str, 0, sizeof(patch->proj_str) - 1);
	patch->nBands = 0;
	printf("Closing dataset \n");
	GDALClose(patch->hDS);
}


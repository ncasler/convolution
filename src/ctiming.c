/** ctiming.c
 * Author: Nathan P. Casler <ncasler@illinois.edu>
 * Date: 12/01/2017
 */

/* -*- Mode: C; c-basic-offset:4 ; -*- */
/**
 * @file
 * Communication dependent functions for mesh
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <mpi.h>
#include "conv.h"
#include "conv-io.h"
#include "Util.h"


static int CONV_nextstate(float **matrix, int y, int x);

/**
 * @brief Exchange values between patches
 * @param patch Local Patch Instance
 * @param nIter Number of iterations
 * @param doCheckpoint Whether to perform a checkpoint after exchange
 * @param m1 First 2-D array
 * @param m2 Second 2-D array
 * @param exchangeInit Exchange initialization function
 * @param exchange     HaloExchange function
 * @param exchangeEnd  Exchange finalization function
 * @param timedata     Timing tracker
 * @return 0
 */
int CONV_TimeIterations( CONVPatchDesc *patch, int nIter, 
		int doCheckpoint,
		float **m1, float **m2,
		int (*exchangeInit) ( CONVPatchDesc *, float **, float **, void *),
		int (*exchange) ( CONVPatchDesc *, float **, CONVTiming *, void *),
		int (*exchangeEnd) ( void * ),
		CONVTiming *timedata)
{
	double t1, t2, t3;
	int i, j, k;
	float **temp;
	int LCols = patch->lnj;
	int LRows = patch->lni;
	void *privateData;
	printf("Patch dims: %i, %i\n", LRows, LCols);
	/* Initialize the timdata */
	timedata->packtime = timedata->unpacktime = 0.0f;
	timedata->exchtime = timedata->itertime = 0.0f;

	/* Initialize mesh */
	CONV_InitLocalMesh( patch, m1, m2 );
	if (doCheckpoint)
		CONVIO_Checkpoint( patch, m1, 0, MPI_INFO_NULL );

	/* Initialize exchange */
	(*exchangeInit) ( patch, m1, m2, &privateData );

	/* Time the iterations */
	t1 = 0;

	MPI_Barrier( MPI_COMM_WORLD );
	t2 = MPI_Wtime();
	for (k=0; k<nIter; k++) {
		t3 = MPI_Wtime();
		(*exchange) ( patch, m1, timedata, privateData );
		t1 += MPI_Wtime() - t3;

		/* Calculate new state for all nonboundary elements */
		/* Change this loop to compute a different solution 
		 * (e.g. an explicit method for the wave and heat 
		 * equation or the matrix-vector product for an implicit 
		 * method. Also change the second loop below */
		for (i = 1; i <=LRows; i++) {
			for (j = 1; j <= LCols; j++) {
				m2[i][j] = CONV_nextstate(m1, i, j);
			}
		}

		/* swap the matrices */
		temp = m1;
		m1   = m2;
		m2   = temp;

		if (doCheckpoint)
			CONVIO_Checkpoint( patch, m1, k+1, MPI_INFO_NULL );
	}
	t2 = MPI_Wtime() - t2;

	(*exchangeEnd) ( privateData );

	/* Get the maximum time over all involved processes */
	timedata->packtime   = timedata->packtime / nIter;
	timedata->unpacktime = timedata->unpacktime / nIter;
	timedata->exchtime   = t1 / nIter;
	timedata->itertime   = t2 / nIter;
	/* Pass the address of the first double in timedata - otherwise,
	 * the MPI implementation may warn about a mismatch in datatypes */
	MPI_Allreduce( MPI_IN_PLACE, &timedata->packtime, 4,
			MPI_DOUBLE, MPI_MAX, patch->comm );

	return 0;
}

/**
 * @brief Time the exchagne between patches
 * @param patch Local Patch instance
 * @param m1  First 2-D array
 * @param m2  Second 2-D array
 * @param exchangeInit Communication initialization function
 * @param exchange     Communication function
 * @param exchangeEnd  Communication finalization function
 * @param timedata     Time tracker
 * @return 0
 */
int CONV_TimeExchange( CONVPatchDesc *patch, 
		float **m1, float **m2,
		int (*exchangeInit) ( CONVPatchDesc *, float **, float **, void *),
		int (*exchange) ( CONVPatchDesc *, float **, CONVTiming *, void *),
		int (*exchangeEnd) ( void * ),
		CONVTiming *timedata)
{
	double t1, t2;
	void *privateData;
	/* Initialize the timdata */
	timedata->packtime = timedata->unpacktime = 0.0f;
	timedata->exchtime = timedata->itertime = 0.0f;

	/* Initialize exchange */
	(*exchangeInit) ( patch, m1, m2, &privateData );

	/* Time the iterations */
	t1 = 0;

	t2 = MPI_Wtime();
	/*for (k=0; k<nIter; k++) {
	  t3 = MPI_Wtime();
	  t1 += MPI_Wtime() - t3;
	  */	
	(*exchange) ( patch, m1, timedata, privateData );
	MPI_Barrier( MPI_COMM_WORLD );
	/* Get the maximum time over all involved processes */
	t2 = MPI_Wtime() - t2;
	//MPI_Barrier(MPI_COMM_WORLD);
	(*exchangeEnd) ( privateData );
	/*
	   timedata->packtime   = timedata->packtime / nIter;
	   timedata->unpacktime = timedata->unpacktime / nIter;
	   timedata->exchtime   = t1 / nIter;
	   timedata->itertime   = t2 / nIter;
	   */
	/* Pass the address of the first double in timedata - otherwise,
	 * the MPI implementation may warn about a mismatch in datatypes */
	/*
	   MPI_Allreduce( MPI_IN_PLACE, &timedata->packtime, 4,
	   MPI_DOUBLE, MPI_MAX, patch->comm );
	   */
	return 0;
}

/** 
 * @brief Apply Lee Filter and exchange
 * @param patch Local Patch instance
 * @param nIter number of iterations
 * @param doCheckpoint Write output?
 * @param m1 First 2-D array
 * @param m2 Second 2-D array
 * @param exchangeInit Communication initialization function
 * @param exchange     Communication function
 * @param exchangeEnd  Communication finalization function
 * @param timedata     Time tracker
 * @warning doCheckpoint and nIter are useless currently
 * @return 0
 */
int CONV_TimePreProcess( CONVPatchDesc *patch, int nIter, 
		int doCheckpoint,
		float **m1, float **m2,
		int (*exchangeInit) ( CONVPatchDesc *, float **, float **, void *),
		int (*exchange) ( CONVPatchDesc *, float **, CONVTiming *, void *),
		int (*exchangeEnd) ( void * ),
		CONVTiming *timedata)
{
	double t1, t2;
	int i, j;
	float **temp;
	//float **kernel;
	//int k_sigma = 1;
	//int k_radius = ceil(k_sigma * 3);
	int LCols = patch->lnj;
	int LRows = patch->lni;
	void *privateData;
	//float nodata = patch->nodata;
	//printf("Patch dims: %i, %i\n", LRows, LCols);
	/* Initialize the timdata */
	timedata->packtime = timedata->unpacktime = 0.0f;
	timedata->exchtime = timedata->itertime = 0.0f;

	/* Initialize exchange */
	(*exchangeInit) ( patch, m1, m2, &privateData );

	/* Time the iterations */
	t1 = 0;

	//MPI_Barrier( MPI_COMM_WORLD );
	t2 = MPI_Wtime();
	/*for (k=0; k<nIter; k++) {
	  t3 = MPI_Wtime();
	  t1 += MPI_Wtime() - t3;
	  */	
	// Remove Infinite,Subnormal,Zero and other bad values if present
	(*exchange) ( patch, m1, timedata, privateData );
	MPI_Barrier( MPI_COMM_WORLD );
	printf("Calculating Mean and variance\n");
	// Get stats and begin filtration
	float g_mean = CONV_PatchMean(patch, m1);
	float g_var = CONV_PatchVariance(patch, m1, g_mean);
	// Apply a lee filter
	printf("Applying Lee filter\n");
	for (i = 1; i <= LRows; i++) {
		for (j = 1; j <= LCols; j++) 
		{
			m2[i][j] = CONV_Lee(patch, m1, i,j, 1, g_var);
		}
	}
	temp = m1;
	m1 = m2;
	m2 = temp;
	//strncpy(opts->prefix, "lee", sizeof(opts->prefix));
	//CONV_WriteLocalMesh(opts, patch, m1, 1);
	  
	/* Get the maximum time over all involved processes */
	t2 = MPI_Wtime() - t2;
	MPI_Barrier(MPI_COMM_WORLD);
	(*exchangeEnd) ( privateData );
	/*
	   timedata->packtime   = timedata->packtime / nIter;
	   timedata->unpacktime = timedata->unpacktime / nIter;
	   timedata->exchtime   = t1 / nIter;
	   timedata->itertime   = t2 / nIter;
	   */
	/* Pass the address of the first double in timedata - otherwise,
	 * the MPI implementation may warn about a mismatch in datatypes */
	/*
	   MPI_Allreduce( MPI_IN_PLACE, &timedata->packtime, 4,
	   MPI_DOUBLE, MPI_MAX, patch->comm );
	   */
	return 0;
}

/**
 * @brief Fill null values using focal mean with a 3x3 window
 * @param patch Local Patch instance
 * @param doCheckpoint Write output?
 * @param m1 First 2-D Array
 * @param m2 Second 2-D Array
 * @param exchangeInit Communication initialization function
 * @param exchange     Communication function
 * @param exchangeEnd  Communication finalization function
 * @param timedata     Time tracker
 * @return 0
 */
int CONV_TimeFillNull( CONVPatchDesc *patch, int nIter, 
		int doCheckpoint,
		float **m1, float **m2,
		int (*exchangeInit) ( CONVPatchDesc *, float **, float **, void *),
		int (*exchange) ( CONVPatchDesc *, float **, CONVTiming *, void *),
		int (*exchangeEnd) ( void * ),
		CONVTiming *timedata)
{
	double t1, t2;
	int i, j, k;
	int LCols = patch->lnj;
	int LRows = patch->lni;
	void *privateData;
	float nodata = patch->nodata;
	float **temp;
	//printf("Patch dims: %i, %i\n", LRows, LCols);
	/* Initialize the timdata */
	timedata->packtime = timedata->unpacktime = 0.0f;
	timedata->exchtime = timedata->itertime = 0.0f;

	/* Initialize mesh */
	//CONV_InitLocalMesh( patch, m1, m2 );
	/* Initialize Kernel */
	if (doCheckpoint)
		CONVIO_Checkpoint( patch, m1, 0, MPI_INFO_NULL );
	printf("Intialized mesh\n");
	/* Initialize exchange */
	(*exchangeInit) ( patch, m1, m2, &privateData );

	/* Time the iterations */
	t1 = 0;

	//MPI_Barrier( MPI_COMM_WORLD );
	t2 = MPI_Wtime();
	/*for (k=0; k<nIter; k++) {
	  t3 = MPI_Wtime();
	  t1 += MPI_Wtime() - t3;
	  */	
	// Remove Infinite,Subnormal,Zero and other bad values if present
	(*exchange) ( patch, m1, timedata, privateData );
	//MPI_Barrier( MPI_COMM_WORLD );
	CONV_cleanPatch(patch, m1);
	for (k = 0; k < nIter; k++) {
		printf("Iteration %i\n", k);
		(*exchange) ( patch, m1, timedata, privateData );
		MPI_Barrier( MPI_COMM_WORLD );
		/* Calculate new state for all nonboundary elements */
		/* Change this loop to compute a different solution 
		 * (e.g. an explicit method for the wave and heat 
		 * equation or the matrix-vector product for an implicit 
		 * method. Also change the second loop below */
		for (i = 1; i <=LRows; i++) {
			for (j = 1; j <= LCols; j++) {
				if (!relativeEqualFloat(m1[i][j], nodata, FLT_EPSILON*4)) {
					m2[i][j] = m1[i][j];
				} else {
					m2[i][j] = CONV_NN(patch, m1, i, j);
					//m2[i][j] = CONV_IDW(patch, m1, i, j, 1, 2);
				}

			}
		}
		/* swap the matrices */
		temp = m1;
		m1   = m2;
		m2   = temp;
	}
	t2 = MPI_Wtime() - t2;
	MPI_Barrier(MPI_COMM_WORLD);
	(*exchangeEnd) ( privateData );
	/*
	   timedata->packtime   = timedata->packtime / nIter;
	   timedata->unpacktime = timedata->unpacktime / nIter;
	   timedata->exchtime   = t1 / nIter;
	   timedata->itertime   = t2 / nIter;
	   */
	/* Pass the address of the first double in timedata - otherwise,
	 * the MPI implementation may warn about a mismatch in datatypes */
	/*
	   MPI_Allreduce( MPI_IN_PLACE, &timedata->packtime, 4,
	   MPI_DOUBLE, MPI_MAX, patch->comm );
	   */
	return 0;
}

/**
 * @brief Apply Gaussian smoothing to raster
 * @param patch Local Patch instance
 * @param nIter Number of iterations
 * @param m1    First 2-D array
 * @param m2    Second 2-D array
 * @param exchangeInit Communication initialization function
 * @param exchange     Communication function
 * @param exchangeEnd  Communication finalization function
 * @param timedata     Time tracker
 * @return  0
 */
int CONV_TimeProcess( CONVPatchDesc *patch, int nIter, 
		float **m1, float **m2,
		int (*exchangeInit) ( CONVPatchDesc *, float **, float **, void *),
		int (*exchange) ( CONVPatchDesc *, float **, CONVTiming *, void *),
		int (*exchangeEnd) ( void * ),
		CONVTiming *timedata)
{
	double t1, t2;
	int i, j, k;
	float **temp;
	float **kernel;
	int k_sigma = 1;
	int k_radius = ceil(k_sigma * 3);
	int LCols = patch->lnj;
	int LRows = patch->lni;
	void *privateData;
	//float nodata = patch->nodata;
	//printf("Patch dims: %i, %i\n", LRows, LCols);
	/* Initialize the timdata */
	timedata->packtime = timedata->unpacktime = 0.0f;
	timedata->exchtime = timedata->itertime = 0.0f;

	/* Initialize mesh */
	//CONV_InitLocalMesh( patch, m1, m2 );
	/* Initialize Kernel */
	CONV_AllocateKernel(&kernel, k_radius);
	for (i = -k_radius; i <= k_radius; i++)
	{
		printf("[");
		for (j = -k_radius; j <= k_radius; j++)
		{
			kernel[i+k_radius][j+k_radius] = gauss(i,j, k_sigma);
			printf(" %f, ", kernel[i+k_radius][j+k_radius]);
		}
		printf("]\n");
	}
	printf("Finished initializing gaussian kernel\n");
	/* Initialize exchange */
	(*exchangeInit) ( patch, m1, m2, &privateData );

	/* Time the iterations */
	t1 = 0;

	//MPI_Barrier( MPI_COMM_WORLD );
	t2 = MPI_Wtime();
	/*for (k=0; k<nIter; k++) {
	  t3 = MPI_Wtime();
	  t1 += MPI_Wtime() - t3;
	  */	
	// Remove Infinite,Subnormal,Zero and other bad values if present
	//strncpy(opts->prefix, "lee", sizeof(opts->prefix));
	//CONV_WriteLocalMesh(opts, patch, m1, 1);
	
	 printf("Beginning Gaussian Blur\n");
	for (k = 0; k < nIter; k++) 
	{
		(*exchange) (patch, m1, timedata, privateData );
		printf("Gaussian iteration %i\n", k);
		for (i = 1; i <= LRows; i++) 
		{
			for (j = 1; j <= LCols; j++)
			{
				m2[i][j] = CONV_Convolve(patch, m1, kernel, k_radius, i, j);
			}
		}
	
		temp = m1;
		m1 = m2;
		m2 = temp;
	}
	
	//Calculate the curvature
	/*(*exchange) (patch, m1, timedata, privateData );
	for (i = 1; i <= LRows; i++)
	{
		for (j = 1; j <= LCols; j++)
		{
			m2[i][j] = CONV_Curvature(patch, m1, i, j);
		}
	}*/
	/* Get the maximum time over all involved processes */
	t2 = MPI_Wtime() - t2;
	MPI_Barrier(MPI_COMM_WORLD);
	(*exchangeEnd) ( privateData );
	printf("Freeing kernel\n");
	CONV_FreeKernel(kernel);
	return 0;
}

/**
 * @brief Calculate implicit curvature of patch data
 * @param patch Local Patch instance
 * @param m1    First 2-D array
 * @param m2    Second 2-D array
 * @param exchangeInit Communication initialization function
 * @param exchange     Communication function
 * @param exchangeEnd  Communication finalization function
 * @return 0
 */
int CONV_TimeClassify( CONVPatchDesc *patch,
		float **m1, float **m2,
		int (*exchangeInit) ( CONVPatchDesc *, float **, float **, void *),
		int (*exchange) ( CONVPatchDesc *, float **, CONVTiming *, void *),
		int (*exchangeEnd) ( void * ),
		CONVTiming *timedata)
{
	double t1, t2;
	int i, j;
	//float **temp;
	int LCols = patch->lnj;
	int LRows = patch->lni;
	void *privateData;
	//float nodata = patch->nodata;
	//printf("Patch dims: %i, %i\n", LRows, LCols);
	/* Initialize the timdata */
	timedata->packtime = timedata->unpacktime = 0.0f;
	timedata->exchtime = timedata->itertime = 0.0f;

	/* Initialize mesh */
	//CONV_InitLocalMesh( patch, m1, m2 );
	/* Initialize Kernel */
	/* Initialize exchange */
	(*exchangeInit) ( patch, m1, m2, &privateData );

	/* Time the iterations */
	t1 = 0;

	//MPI_Barrier( MPI_COMM_WORLD );
	t2 = MPI_Wtime();
	/*for (k=0; k<nIter; k++) {
	  t3 = MPI_Wtime();
	  t1 += MPI_Wtime() - t3;
	  */	
	// Remove Infinite,Subnormal,Zero and other bad values if present
	//strncpy(opts->prefix, "lee", sizeof(opts->prefix));
	//CONV_WriteLocalMesh(opts, patch, m1, 1);
	
	printf("Calculating curvature\n");
	//Calculate the curvature
	(*exchange) (patch, m1, timedata, privateData );
	for (i = 1; i <= LRows; i++)
	{
		for (j = 1; j <= LCols; j++)
		{
			m2[i][j] = CONV_Curvature(patch, m1, i, j);
		}
	}
	/* Get the maximum time over all involved processes */
	t2 = MPI_Wtime() - t2;
	MPI_Barrier(MPI_COMM_WORLD);
	(*exchangeEnd) ( privateData );
	return 0;
}

/**
 * @brief Calculate sum of neighbors for game of life
 * @deprecated Has no use in this library, will be removed in release
 * @param matrix Input 2-D array
 * @param row    Row index
 * @param col    Column index
 * @return 1 if it has 2 neighbors
 */
static int inline CONV_nextstate(float **matrix, int row, int col)
{
	int sum;

	/* add values of all eight neighbors */
	sum = matrix[row-1][col-1] + matrix[row-1][col] +
		matrix[row-1][col+1] + matrix[row][col-1] +
		matrix[row][col+1] + matrix[row+1][col-1] +
		matrix[row+1][col] + matrix[row+1][col+1];

	if ( sum < 2 || sum > 3 ) return NODATA;
	else if (sum == 3)        return 0;
	else                      return matrix[row][col];
}



/*static float inline CONV_diffuse(float **matrix, float **weights, int axis, int row, int col)
  {
  float sigma = sqrt(2 * time);
  int radius = ceil(2 * sigma); // Should we use 2sigma or 3sigma?
  int width = 1 + 2*radius;
  float weights[width];
  int i;
  for (i = -radius; i <= radius; i++) {
  float pref = 1 / (2 * M_PI * (sigma * sigma));
  float suff = exp(- (abs(i) *abs(i))/(2 * pow(sigma,2)));

  weights[i+radius] = (1 / (2 * M_PI * (sigma*sigma)) * exp(- (abs(i)*abs(i)/(2 * sigma*sigma)));
  */	

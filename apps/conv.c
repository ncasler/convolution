/** conv.c
 * Author: Nathan P. Casler <ncasler@illinois.edu>
 * Date: 12/01/2017
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include "conv.h"
#include "conv-io.h"

static int verbose = 0;

int main(int argc, char *argv[])
{
	int           rank, provided;
	CONVPatchDesc patch;
	CONVOptions   options;
	CONVTiming    timedataFence;
	float         **m1, **m2;
	int           commtype;
	const char    *commstr = 0;
	int doCheckpoint = 0;

	MPI_Init_thread(&argc, &argv, MPI_THREAD_SINGLE, &provided);

	CONV_ParseArgs(argc, argv, &options);
	verbose = options.verbose;
	doCheckpoint = options.doIO;

	CONVIO_Init(MPI_COMM_WORLD);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	for (commtype =0; commtype<2; commtype++) {
		/* Create the communicator, including the determining coordinates
		 * of the "patch" that this process owns, and the decomposition
		 * of the domain */
		switch(commtype) {
			case 0: 
				CONV_PatchCreateProcessMesh( &options, &patch );
				commstr = "COMM_WORLD";
				break;
			case 1:
				CONV_PatchCreateProcessMeshWithCart( &options, &patch );
				commstr = "CART_CREATE";
				break;
			default:
				fprintf( stderr, "Internal error - commtype = %d\n", commtype);
				MPI_Abort( MPI_COMM_WORLD, 1 );
		}
		CONV_PatchCreateDataMeshDesc( &options, &patch );
	
	if (verbose) {
		printf(" [%d] Proc with %s is [%d,%d] in [%d,%d], mesh [%d,%d]x[%d,%d] within [%d,%d\n",
			rank, commstr,
			patch.patchI, patch.patchJ, patch.pNI, patch.pNJ,
			patch.gI, patch.gI+patch.lni-1,
			patch.gJ, patch.gJ+patch.lnj-1,
			patch.gNI, patch.gNJ );
		MPI_Barrier( MPI_COMM_WORLD );
	}
	printf("Requested %i iterations\n", options.nIter);
	CONV_AllocateLocalMesh( &patch, &m1, &m2 );
	CONV_InitLocalMesh(&patch, m1, m2);
	CONV_WriteLocalMesh(&options, &patch, m1, options.nIter);
	printf("Allocated mesh\n");
	strncpy(options.prefix, "exchange", sizeof(options.prefix));
	CONV_TimeExchange(&patch, m1, m2, CONV_ExchangeInitFence,
			CONV_ExchangeFence, 
			CONV_ExchangeEndFence,
			&timedataFence );
	CONV_WriteLocalMesh(&options, &patch, m1, options.nIter);
	strncpy(options.prefix, "null", sizeof(options.prefix));
	CONV_TimeFillNull(&patch, 20, doCheckpoint, m1, m2, 
			CONV_ExchangeInitFence,
			CONV_ExchangeFence,
			CONV_ExchangeEndFence,
			&timedataFence );
	CONV_WriteLocalMesh(&options, &patch, m1, options.nIter);
	/* Run these tests multiple times. In many cases, the first
	 * run will take significantly longer, as library setup actions
	 * take place. To illustrate this, write out the timing data for
	 * each run, so that the first and second times can be compared */
	//for (kk=0; kk<2; kk++) {
		/* For each communication approach, perform these steps
		 * 1: Initialize the mesh (initial data)
		 * 2: Initialize the exchange 
		 * 3: Time the iterations, including any communication steps.
		 */
	/*CONV_TimeIterations( &patch, options.nIter, doCheckpoint, m1, m2, 
				CONV_ExchangeInitFence,
				CONV_ExchangeFence,
				CONV_ExchangeEndFence,
				&timedataFence );
	*/
		/* Print the total time taken */
	/*	if (rank == 0) {
			printf("Mesh size [%d,%d] on process array [%d,%d] for %s\n",
					patch.gNI, patch.gNJ, patch.pNI, patch.pNJ, commstr );
			printf( "Exchange type\tPer iter\tExchange\tPacktime\tUnpacktime\n");
			printf( "[%d] Fence: \t%e\t%e\n", rank,
					timedataFence.itertime,
					timedataFence.exchtime );
		}
	//}
	*/
	strncpy(options.prefix, "filter", sizeof(options.prefix));
	CONV_TimePreProcess(&patch, options.nIter, doCheckpoint, m1, m2,
			CONV_ExchangeInitFence,
			CONV_ExchangeFence,
			CONV_ExchangeEndFence,
			&timedataFence );
	CONV_WriteLocalMesh(&options, &patch, m2, options.nIter);

	strncpy(options.prefix, "gauss", sizeof(options.prefix));
	CONV_TimeProcess(&patch, options.nIter, m1, m2,
			CONV_ExchangeInitFence,
			CONV_ExchangeFence,
			CONV_ExchangeEndFence,
			&timedataFence );
	CONV_WriteLocalMesh(&options, &patch, m2, options.nIter);
	
	strncpy(options.prefix, "curve", sizeof(options.prefix));
	CONV_TimeClassify(&patch, m1, m2, 
			CONV_ExchangeInitFence,
			CONV_ExchangeFence,
			CONV_ExchangeEndFence,
			&timedataFence );
	CONV_WriteLocalMesh(&options, &patch, m2, options.nIter);


	CONV_FreeLocalMesh( &patch, m1, m2 );

	// Run Diffusion filter  then curvature extraction
	}
	CONVIO_Finalize();
	MPI_Finalize();

	return 0;
}

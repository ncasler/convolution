#CC=gcc
#CFLAGS = -g -ggdb -O3 -ipo -Wall -Wextra -fPIC 
#OUTOUT_OPTION = -MMD -MP -o -@
#SHARED_LIBRARY_EXTENSION = so
#SHARED_LIBRARY_FLAG = -shared

include makefile.config

GCOV_PREFIX=./coverage
DEBUG=1
GDAL_DIR = /sw/geosoft/gdal-2.0.2-fgdb

ZLIB_DIR = /sw/zlib


H5_DIR = /sw/hdf5-1.8.15-para

MPI_DIR = /sw/mpich-3.1.4

FFTW_DIR = /home/ncasler/apps/fftw

GTIFF_DIR= /sw/lidar/libgeotiff

NC_DIR = /sw/netcdf4/4.4.1.1-parallel

SRC_DIR = ./src
INCLUDE_DIR = ./include
DEP_DIR = ./deps

EXTLIB = -L$(GDAL_DIR)/lib -L$(ZLIB_DIR)/lib -L$(MPI_DIR)/lib -L$(H5_DIR)/lib -L$(FFTW_DIR)/lib -L$(GTIFF_DIR)/lib -L$(NC_DIR)/lib
EXTINCLUDE = -I$(INCLUDE_DIR) -I$(GDAL_DIR)/include -I$(ZLIB_DIR)/include -I$(MPI_DIR)/include -I$(H5_DIR)/include -I$(FFTW_DIR)/include -I$(GTIFF_DIR)/include -I$(NC_DIR)/include
UNIT_TEST_DIR = ./test/unit
INT_TEST_DIR = ./test/integration
BIN_TEST_DIR = ./bin/test
APP_DIR = ./apps
BIN_DIR = ./bin
LIB_DIR= ./lib
OBJ_DIR=./obj
LIBS = -lm -lgdal -lz -lmpi -lhdf5 -lfftw3 -lgeotiff -lnetcdf -lnetcdff

SOURCE = $(wildcard $(SRC_DIR)/*.c)
OBJS = $(SOURCE:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)
DEPS = $(SOURCE:$(SRC_DIR)/%.c=$(DEP_DIR)/%.d)
-include ${DEPS}


#lib: libconv(${OBJS})

#libconv.${SHARED_LIBRARY_EXTENSION}: ${OBJS}
#	${CC} ${SHARED_LIBRARY_FLAG} -o $@ $^

all: bin/gridRead \
	bin/Conv \
	$(BIN_TEST_DIR)/Kernel \
	$(BIN_TEST_DIR)/Grid \
	$(BIN_TEST_DIR)/Compare \
	$(BIN_TEST_DIR)/Convolve \
	$(BIN_TEST_DIR)/GridFromRaster \
	$(BIN_TEST_DIR)/Raster \
	$(BIN_TEST_DIR)/GridDeterminant \
	$(BIN_TEST_DIR)/GridGauss \
	$(BIN_TEST_DIR)/GridSubtract \
	$(BIN_TEST_DIR)/GridZeroCross \
	$(BIN_TEST_DIR)/GridSmooth \
	$(BIN_TEST_DIR)/GridHDF \
	$(BIN_TEST_DIR)/GridDownSample \
	$(BIN_TEST_DIR)/GridVar \
	$(BIN_TEST_DIR)/Stats \
	$(BIN_TEST_DIR)/Sort \
	$(BIN_TEST_DIR)/Median \
	$(BIN_TEST_DIR)/FFTW \
	$(BIN_TEST_DIR)/Lee \
	$(BIN_TEST_DIR)/Gauss \
	$(BIN_TEST_DIR)/Laplace \
	$(BIN_TEST_DIR)/Isophote \
	$(BIN_TEST_DIR)/Gradient \
	$(BIN_TEST_DIR)/SDGD \
	$(BIN_TEST_DIR)/Float \
	$(BIN_TEST_DIR)/Activate \
	$(BIN_TEST_DIR)/Erode

bin/gridRead: $(INT_TEST_DIR)/GridRead.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Kernel: $(UNIT_TEST_DIR)/Kernel.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Grid: $(UNIT_TEST_DIR)/Grid.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Compare: $(UNIT_TEST_DIR)/Compare.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Stats: $(INT_TEST_DIR)/GridStats.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/FFTW: $(INT_TEST_DIR)/Fftw.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Sort: $(UNIT_TEST_DIR)/Sort.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Median: $(INT_TEST_DIR)/GridMedian.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Float: $(UNIT_TEST_DIR)/Float.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Lee: $(INT_TEST_DIR)/GridLee.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)
$(BIN_TEST_DIR)/Laplace: $(INT_TEST_DIR)/GridLaplace.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Isophote: $(INT_TEST_DIR)/Isophote.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/SDGD: $(INT_TEST_DIR)/GridSDGD.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Gradient: $(INT_TEST_DIR)/Gradient.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Convolve: $(INT_TEST_DIR)/Convolution.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/GridFromRaster: $(INT_TEST_DIR)/GridFromRaster.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Raster: $(UNIT_TEST_DIR)/Raster.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/GridDeterminant: $(INT_TEST_DIR)/GridDeterminant.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/GridGauss: $(INT_TEST_DIR)/GridGauss.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/GridSubtract: $(INT_TEST_DIR)/GridSubtract.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/GridZeroCross: $(INT_TEST_DIR)/GridZeroCross.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/GridSmooth: $(INT_TEST_DIR)/GridSmooth.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/GridHDF: $(INT_TEST_DIR)/GridHDF.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/GridDownSample: $(INT_TEST_DIR)/GridDownSample.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/GridVar: $(INT_TEST_DIR)/GridVar.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Gauss: $(UNIT_TEST_DIR)/Gauss.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Activate: $(INT_TEST_DIR)/Activate.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_TEST_DIR)/Erode: $(INT_TEST_DIR)/GridErode.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(BIN_DIR)/Conv: $(APP_DIR)/conv.c $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(EXTINCLUDE) $(EXTLIB) $(LIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) -c -o $@ $< $(CFLAGS) $(EXTINCLUDE)

.PHONY: clean

clean:
	-rm -f $(OBJ_DIR)/*.o
	-rm -f $(BIN_DIR)/*
	-rm -f $(BIN_TEST_DIR)/test/*

tags:
	ctags -R $(SRC_DIR)/*.c $(INCLUDE_DIR)/*.h

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Grid.h"
#include "Kernel.h"
#include "Raster.h"
#include <string.h>

int main(int argc, char **argv) {
	char in_file[1024];
	char out_file[1024];
	float sigma = 0.0f;
	Raster in_raster;
	Raster out_raster;
	Raster_init(&in_raster);
	Raster_init(&out_raster);
	strcpy(in_file, argv[1]);
	strcpy(out_file, argv[2]);
	sigma = atof(argv[3]);
	Kernel kern;
	Kernel_init(&kern);
	printf("Generating kernel\n");
	Kernel_gauss(sigma, &kern);
	printf("Filename is %s\n", in_file);
	Raster_read(in_file, &in_raster);
	int i = 0;
	int j = 0;
	for (i = 0; i < kern.rows; i++) {
		printf("[");
		for (j = 0; j < kern.cols; j++) {
			printf("%.3f, ", Kernel_get(j, i, &kern));
		}
		printf("]\n");
	}
	//float tmp_float = 0.0;
	Raster_copy(&in_raster, &out_raster, 0);
	printf("Convolving\n");
	Grid_convolve(&in_raster.grid, &kern, &out_raster.grid);
	printf("Writing to %s\n", out_file);
	Raster_write(out_file, &out_raster);
	printf("Cleaning up\n");
	Raster_free(&in_raster);
	Raster_free(&out_raster);
	Kernel_free(&kern);
	return 0;
}

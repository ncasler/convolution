#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "Grid.h"
#include "Kernel.h"
#include "Raster.h"
#include "Util.h"

int main(int argc, char **argv) {
	char file_xx[1024];
	char file_yy[1024];
	char file_xy[1024];
	char file_out[1024];
	Raster raster_xx;
	Raster raster_yy;
	Raster raster_xy;
	Raster raster_out;

	Raster_init(&raster_xx);
	Raster_init(&raster_yy);
	Raster_init(&raster_xy);
	Raster_init(&raster_out);

	strcpy(file_xx, argv[1]);
	strcpy(file_yy, argv[2]);
	strcpy(file_xy, argv[3]);
	strcpy(file_out, argv[4]);

	Raster_read(file_xx, &raster_xx);
	Raster_read(file_yy, &raster_yy);
	Raster_read(file_xy, &raster_xy);
	int i = 0;
	int j = 0;
	float pxx = 0.0f;
	float pxy = 0.0f;
	float pyy = 0.0f;
	float tmp_float = 0.0f;
	float val = 0.0f;
	float nodata = raster_xx.grid.nodata;
	Raster_copy(&raster_xx, &raster_out, 0);
	int cols = raster_xx.grid.cols;
	int rows = raster_xx.grid.rows;
	for (j = 0; j < rows; j++) {
		for (i = 0; i < cols; i++) {
			pxx = Grid_get(i, j, &raster_xx.grid);
			pyy = Grid_get(i, j, &raster_yy.grid);
			pxy = Grid_get(i, j, &raster_xy.grid);
			if (almostEqualFloat(pxx,nodata,1) || almostEqualFloat(pyy, nodata, 1) 
					|| almostEqualFloat(pxy, nodata,1)) {
				Grid_set(i, j, &raster_out.grid, nodata);
			} else {
				tmp_float = (pxx * pyy) - (pxy * pxy);
				if (tmp_float < 0) 
				{
					val = 0;
				} else if (tmp_float > 0) {
					if (pxx > 0) {
						val = -1;
					} else if(pxx < 0) {
						val = 1;
					} else {
						val = 0;
					}
				} else {
					val = -9999;
				}
				// Used for visualization, classify -1,0, 1
				//printf("Px [%i,%i] value: %f\n", j, i, tmp_float);
				Grid_set(i, j, &raster_out.grid, val);
			}
		}
	}

	Raster_write(file_out, &raster_out);
	Raster_free(&raster_out);
	Raster_free(&raster_xx);
	Raster_free(&raster_yy);
	Raster_free(&raster_xy);
	return 0;
}



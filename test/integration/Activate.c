#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "Grid.h"
#include "Util.h"
#include "Raster.h"
#include "Activation.h"

int main (int argc, char **argv)
{
	char file_in[1024];
	char file_out[1024];
	Raster raster_in;
	Raster raster_out;
	int i, j = 0;
	float tmp = 0.0f;
	Raster_init(&raster_in);
	Raster_init(&raster_out);
	strcpy(file_in, argv[1]);
	strcpy(file_out, argv[2]);
	printf("Reading raster from %s\n", file_in);
	Raster_read(file_in, &raster_in);
	Raster_copy(&raster_in, &raster_out, 0);
	printf("Allocating Derivative Grids\n");
	int cols = raster_in.grid.cols;
	int rows = raster_in.grid.rows;
	printf("Applying activation\n");
	for (j = 0; j < rows; j++)
	{
		for (i = 0; i < cols; i++) 
		{
			tmp = Grid_get(i, j, &raster_in.grid);
			Grid_set(i, j, &raster_out.grid, (float)CONVA_relu((double)tmp));
		}
	}
	printf("Writing to file %s\n", file_out);
	Raster_write(file_out, &raster_out);
	printf("Cleaning up\n");
	Raster_free(&raster_in);
	Raster_free(&raster_out);
	return 0;
}


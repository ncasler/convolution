#include <stdio.h>
#include <stdlib.h>
#include "Grid.h"
#include "Kernel.h"
#include <math.h>

int main() {
	Grid in_grid;
	Grid out_grid;
	Kernel kern;
	in_grid.data = NULL;
	out_grid.data = NULL;
	kern.data = NULL;
	in_grid.cols = 15;
	in_grid.rows = 15;
	out_grid.cols = 15;
	out_grid.rows = 15;
	kern.cols = 3;
	kern.rows = 3;
	Grid_alloc(&in_grid);
	Grid_alloc(&out_grid);
	Kernel_sobel(0, &kern);
	int i = 0;
	int j = 0;

	for (i = 0; i < in_grid.rows; i++) {
		for (j = 0; j < in_grid.cols; j++) {
			Grid_set(j, i, &in_grid, i +j);
		}
	}
	printf("Kernel coefficient: %3.f\n", kern.coef);
	Grid_convolve(&in_grid, &kern, &out_grid);
	Grid_free(&in_grid);
	Grid_free(&out_grid);
	Kernel_free(&kern);
	return 0;
}

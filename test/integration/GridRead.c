#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <float.h>
#include <math.h>
#include <time.h>
#include "gdal.h"
#include "cpl_conv.h"
#include <argp.h>
#include <string.h>
/* Program documentation */
static char doc[] = 
	"Grid read example -- A program which will read a Tiff and write it to another file";


/* A description of available arguments */
static char args_doc[] = "Verbose Input_File";

/* The options we understand */
static struct argp_option options[] = {
	{"verbose", 'v', 0, 0, "Produce verbose output"},
	{"quiet", 'q', 0, 0, "Don't produce any output"},
	{"silent", 's', 0, OPTION_ALIAS},
	{"input", 'i', "FILE", 0, "Input GeoTIFF"},
	{"output", 'o', "OUT", 0, "Output GeoTIFF"},
	{ 0 }
};

/* Used by main to communicate with parse_opt */
struct arguments
{
	int silent,verbose;
	char *input_file;
	char *output_file;
};

/* Parse a single option */
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we 
	 * know is a pointer to our arguments structure */
	struct arguments *arguments = state->input;

	switch (key)
	{
		case 'q': case 's':
			arguments->silent = 1;
			break;
		case 'v':
			arguments->verbose = 1;
			break;
		case 'i':
			arguments->input_file = arg;
			break;
		case 'o':
			arguments->output_file = arg;

		case ARGP_KEY_ARG:
			if (state->arg_num >= 2) {
				/* Too many arguments */
				argp_failure(state, 1, 0, "Unknown argument");
				//argp_usage(state);
				
			//arguments->args[state->arg_num] = arg;
			}
			break;
		
			

		default:
			return ARGP_ERR_UNKNOWN;
			//return 0;
	}
	return 0;
}

/* Our argp parser */
static struct argp argp = { options, parse_opt, args_doc, doc };


int main(int argc, char **argv) {
	struct arguments arguments;
	arguments.silent = 0;
	arguments.verbose = 0;
	arguments.input_file = "-";
	arguments.output_file = "-";
	GDALDatasetH hDataset;
	GDALAllRegister();
	GDALRasterBandH hBand;
	int nBlockXSize, nBlockYSize;
	int bGotMin, bGotMax;
	//int nXSize, nYSize;
	double adfMinMax[2];
	char inFile[1024];
	char outFile[1024];
	//int i = 0;
	//int j = 0;
	//int k = 0;
	// Kernel specific parameters
	//int k_cols = 3;
	//int k_rows = 3;
	//int kernel[9] = {1, 2, 1, 2, 4, 2, 1, 2, 1};
	//float kernel_factor = 1 / 16;
	// Parse arguments
	argp_parse(&argp, argc, argv, 0, 0, &arguments);
	
	printf( "INPUT_FILE=%s\n"
			"OUTPUT_FILE=%s\nVERBOSE =%s\nSILENT=%s\n",
			arguments.input_file,
			arguments.output_file,
			arguments.verbose ? "yes" : "no",
			arguments.silent ? "yes" : "no");
	strcpy(inFile, arguments.input_file);
	strcpy(outFile, arguments.output_file);
	// Try to open the raster
	hDataset = GDALOpen(inFile, GA_ReadOnly);
	if (hDataset == NULL ){
		fprintf(stderr, "Failed to open raster at %s\n", inFile);
		exit(-1);
	}
	
	
	hBand = GDALGetRasterBand( hDataset, 1 );
	GDALGetBlockSize( hBand, &nBlockXSize, &nBlockYSize );
	adfMinMax[0] = GDALGetRasterMinimum( hBand, &bGotMin );
	adfMinMax[1] = GDALGetRasterMaximum( hBand, &bGotMax );
	if ( ! (bGotMin && bGotMax) ) 
		GDALComputeRasterMinMax(hBand, TRUE, adfMinMax);
	//nXSize = GDALGetRasterBandXSize(hBand);
	//nYSize = GDALGetRasterBandYSize(hBand);

	printf( "Min=%.3fd, Max=%.3f\n", adfMinMax[0], adfMinMax[1] );

	/* TODO: Find window to convolve and pull using rasterIO
		Need to decide whether to mirror or 0-pad the datset
		*/
	
	
	GDALClose(hDataset);
	return 0;
}

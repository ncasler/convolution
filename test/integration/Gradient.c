#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "Grid.h"
#include "Util.h"
#include "Raster.h"

int main(int argc, char **argv) {

	char file_in[1024];
	char out_mag[1024];
	char out_dir[1024];
	Raster raster_in;
	Raster raster_mag;
	Raster raster_dir;
	Grid   grid_x;
	Grid   grid_y;
	Kernel kern_x;
	Kernel kern_y;
	int i, j = 0;
	float px = 0.0f;
	float py = 0.0f;

	Kernel_init(&kern_x);
	Kernel_init(&kern_y);
	Grid_init(&grid_x);
	Grid_init(&grid_y);
	Raster_init(&raster_in);
	Raster_init(&raster_mag);
	Raster_init(&raster_dir);
	strcpy(file_in, argv[1]);
	strcpy(out_mag, argv[2]);
	strcpy(out_dir, argv[3]);
	printf("Reading raster from %s\n", file_in);
	Raster_read(file_in, &raster_in);
	Raster_copy(&raster_in, &raster_mag, 0);
	Raster_copy(&raster_in, &raster_dir, 0);
	printf("Allocating memory\n");
	Grid_copy(&raster_in.grid, &grid_x, 0);
	Grid_copy(&raster_in.grid, &grid_y, 0);
	Kernel_sobel(0, &kern_x);
	Kernel_sobel(1, &kern_y);

	int cols = raster_in.grid.cols;
	int rows = raster_in.grid.rows;
	double mag = 0.0f;
	double dir = 0.0f;
	printf("Calculating X'\n");
	Grid_convolve(&raster_in.grid, &kern_x, &grid_x);
	printf("Calculating Y'\n");
	Grid_convolve(&raster_in.grid, &kern_y, &grid_y);
	printf("Calculating Magnitude\n");
	for (j = 0; j < rows; j++)
	{
		for (i = 0; i < cols; i++) 
		{
			px = Grid_get(i, j, &grid_x);
			py = Grid_get(i, j, &grid_y);
			//printf("(%i,%i) = [%.4f,%4f]\n", i,j, px, py);
			mag = sqrt((px * px) + (py * py));
			dir = atan(py / px);
			Grid_set(i, j, &raster_mag.grid, (float)mag);
			Grid_set(i, j, &raster_dir.grid, (float)dir);
		}
	}
	printf("Writing to file %s\n", out_mag);
	Raster_write(out_mag, &raster_mag);
	printf("Writing to file %s\n", out_dir);
	Raster_write(out_dir, &raster_dir);
	Grid_free(&grid_x);
	Grid_free(&grid_y);
	Raster_free(&raster_dir);
	Raster_free(&raster_mag);
	Raster_free(&raster_in);
	Kernel_free(&kern_x);
	Kernel_free(&kern_y);
	return 0;
}


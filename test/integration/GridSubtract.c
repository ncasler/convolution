#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Grid.h"
#include "Raster.h"
#include <string.h>

int main(int argc, char **argv) {
	char file1[1024];
	char file2[1024];
	char file_out[1024];
	Raster raster1;
	Raster raster2;
	Raster raster_out;
	printf("Initializing variables\n");
	Raster_init(&raster1);
	Raster_init(&raster2);
	Raster_init(&raster_out);
	strcpy(file1, argv[1]);
	strcpy(file2, argv[2]);
	strcpy(file_out, argv[3]);
	printf("Reading rasters\n");
	Raster_read(file1, &raster1);
	Raster_read(file2, &raster2);
	Raster_copy(&raster1, &raster_out, 0);
	printf("Subtracting rasters\n");
	Grid_subtract(&raster1.grid, &raster2.grid, &raster_out.grid);
	Raster_write(file_out, &raster_out);
	printf("Cleaning up\n");
	Raster_free(&raster1);
	Raster_free(&raster2);
	Raster_free(&raster_out);
	return 0;
};

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Grid.h"
#include "Kernel.h"
#include "Raster.h"
#include <string.h>

int main(int argc, char **argv) {
	char in_file[1024];
	char out_file[1024];
	int axis;
	Raster in_raster;
	Raster out_raster;
	Raster_init(&in_raster);
	Raster_init(&out_raster);
	strcpy(in_file, argv[1]);
	strcpy(out_file, argv[2]);
	axis = atoi(argv[3]);
	Kernel kern;
	kern.data = NULL;
	Kernel_sobel(axis, &kern);
	printf("Filename is %s\n", in_file);
	Raster_read(in_file, &in_raster);
	printf("Convolving\n");
	Raster_copy(&in_raster, &out_raster, 0);
	Grid_convolve(&in_raster.grid, &kern, &out_raster.grid);
	Raster_write(out_file, &out_raster);
	printf("Cleaning up");
	Raster_free(&in_raster);
	Raster_free(&out_raster);
	Kernel_free(&kern);
	return 0;
}

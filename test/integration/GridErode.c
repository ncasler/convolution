#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "Grid.h"
#include "Raster.h"
#include "Util.h"

int main (int argc, char **argv)
{
	char in_file[1024];
	char out_file[1024];
	int radius = 2;
	Raster in_raster;
	Raster out_raster;
	Kernel s_kern;
	int flag = 0;
	if (flag < 0){
		fprintf(stderr, "Error: Failed to allocate the kernel structure\n");
		exit(-1);
	}
	int i = 0;
	int j = 0;
	float tmp = 0.0f;
	if (argc < 2) {
		fprintf(stderr, "Error: Too Few Arguments");
		exit(-1);
	}
	strcpy(in_file, argv[1]);
	strcpy(out_file, argv[2]);
	Raster_init(&in_raster);
	Raster_init(&out_raster);
	Kernel_init(&s_kern);
	s_kern.cols = 2 * radius + 1;
	s_kern.rows = 2 * radius + 1;
	flag = Kernel_alloc(&s_kern);
	// Set kernel all positive
	for (i = 0; i < s_kern.cols * s_kern.rows; i++)
	{
		s_kern.data[i] = 1.0f;
	}
	Raster_read(in_file, &in_raster);
	printf("Reading %s", in_file);
	Raster_copy(&in_raster, &out_raster, 0);
	for (j = 0; j < in_raster.grid.rows; j++) 
	{
		for (i = 0; i < in_raster.grid.cols; i++)
		{
			tmp = Grid_erode(&in_raster.grid, i, j, &s_kern);
			Grid_set(i, j, &out_raster.grid, tmp);
		}
	}
	printf("Writing raster %s\n", out_file);
	Raster_write(out_file, &out_raster);
	printf("Cleaning up\n");
	Raster_free(&out_raster);
	Raster_free(&in_raster);
	Kernel_free(&s_kern);
	return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Grid.h"
#include <string.h>
#include "Raster.h"

int main(int argc, char **argv) {
	char in_file[1024];
	char out_file[1024];
	int radius = 2;
	Raster in_raster;
	Raster out_raster;
	Raster_init(&in_raster);
	Raster_init(&out_raster);
	strcpy(in_file, argv[1]);
	strcpy(out_file, argv[2]);
	Raster_read(in_file, &in_raster);
	int i = 0;
	int j = 0;
	float tmp = 0.0f;
	printf("Reading %s\n",in_file);
	Raster_copy(&in_raster, &out_raster, 0);
	// Calculate global Mean for raster
	printf("Calculating mean\n");
	float g_mean = Grid_mean(&in_raster.grid);
	// Calculate global Variance for raster
	printf("Calculating variance\n");
	float g_var = Grid_var(&in_raster.grid, g_mean);
	printf("Applying Lee filter\n");
	for (j = 0; j < in_raster.grid.rows; j++) {
		for (i = 0; i < in_raster.grid.cols; i++) {
			//printf("Filtering (%i,%i)\n", i, j);
			tmp = Grid_Lee(&in_raster.grid, i, j, radius, g_var);
			Grid_set(i, j, &out_raster.grid, tmp);
		}
	}
	printf("Writing raster\n");
	Raster_write(out_file, &out_raster);
	printf("Cleaning up\n");
	Raster_free(&in_raster);
	Raster_free(&out_raster);
	return 0;
}

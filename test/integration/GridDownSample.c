#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Raster.h"
#include <string.h>

int main(int argc, char **argv) {
	char file_in[1024];
	char file_out[1024];
	Raster raster_in;
	Raster raster_out;
	Raster_init(&raster_in);
	Raster_init(&raster_out);
	strcpy(file_in, argv[1]);
	strcpy(file_out, argv[2]);
	printf("Reading raster\n");
	Raster_read(file_in, &raster_in);
	printf("Downsampling\n");
	Raster_downsample(&raster_in, &raster_out);
	printf("Writing\n");
	Raster_write(file_out, &raster_out);
	printf("Cleaning up\n");
	Raster_free(&raster_out);
	Raster_free(&raster_in);
	return 0;
}


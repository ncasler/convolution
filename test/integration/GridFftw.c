#include <stdio.h>
#include <stdlib.h>
#include <fftw3.h>
#include <mpi.h>
#include "Grid.h"
#include "Raster.h"

int main(int argc, char **argv) {
		int N = 10000;
		int N_OUT = N<<1 + 1;
		fftw_complex  *cmp;
		fftw_plan p, b;
		double *in, *out;
		MPI_Comm comm;
		MPI_Init(&argc, &argv);
		int i = 0;
		double start, end;
		start = MPI_Wtime();
		printf("Allocating\n");
		in =  (double*) fftw_malloc(sizeof(double) * N);
		cmp = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N_OUT);
		out = (double*) fftw_malloc(sizeof(double) * N);
		p = fftw_plan_dft_r2c_1d(N, in, cmp, FFTW_ESTIMATE);
		b = fftw_plan_dft_c2r_1d(N, cmp, out, FFTW_ESTIMATE);
		for (i = 0; i < N; ++i)
		{
			in[i] = (double) i;
			//printf("%f\n", in[i]);
		}
		printf("IN:\n");
		for (i = 0; i < N; ++i) 
		{
			printf("%4.4f, ", in[i]);
		}
		printf("To Complex\n");
		fftw_execute(p);
		printf("Returning to Reality\n");
		fftw_execute(b);
		end = MPI_Wtime() - start;
		printf("OUT:\n");
		for (i = 0; i < N; ++i) 
		{
			out[i] /= N;
			printf("%5.2f, ", out[i]);
		}
		printf("Computed transform of %i elem finished in %lf seconds\n", N, end);
		fftw_destroy_plan(p);
		fftw_destroy_plan(b);
		fftw_free(in);
		fftw_free(out);
		fftw_free(cmp);
		MPI_Finalize();
		return 0;
	}

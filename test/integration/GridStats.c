#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Grid.h"
#include "Raster.h"
#include "Util.h"
#include <string.h>

int main(int argc, char **argv) {
	char file_in[1024];
	Raster raster_in;
	float mean = 0.0f;
	float var = 0.0f;
	float stdev = 0.0f;
	
	//Grid grid_sq;
	//Grid grid_mean;
	//Grid grid_stdev;

	// Initialize Rasters
	Raster_init(&raster_in);
	// Parse Arguments
	strcpy(file_in, argv[1]);
	printf("Reading Raster\n");
	Raster_read(file_in, &raster_in);
	mean = Grid_mean(&raster_in.grid);
	var = Grid_var(&raster_in.grid, mean);
	stdev = sqrt(var);
	printf("Grid stats: Mean: %.4f, Var: %.4f, StDev: %.4f\n", mean, var, stdev);
	Raster_free(&raster_in);
	return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Grid.h"
#include "Raster.h"
#include <string.h>
int main(int argc, char **argv) {
	char file_in[1024];
	char file_out[1024];
	Raster raster_in;
	Raster raster_out;
	Grid grid_sq;
	Grid grid_mean;
	Grid grid_mean_sq;
	Grid grid_sq_mean;
	Kernel kern;
	
	// Initialize Rasters
	Raster_init(&raster_in);
	Raster_init(&raster_out);
	// Initialize Kernel
	Kernel_init(&kern);
	Kernel_mean(1, &kern); // Create 3x3 mean kernel
	
	// Initialize intermediate grids
	Grid_init(&grid_sq);
	Grid_init(&grid_mean);
	Grid_init(&grid_mean_sq);
	Grid_init(&grid_sq_mean);
	// Read cmd line arguments
	strcpy(file_in, argv[1]);
	strcpy(file_out, argv[2]);
	printf("Reading rasters\n");
	Raster_read(file_in, &raster_in);
	printf("Copying raster dimensions\n");
	Raster_copy(&raster_in, &raster_out, 0);
	Grid_copy(&raster_in.grid, &grid_mean, 0);
	Grid_copy(&raster_in.grid, &grid_sq, 0);
	printf("Convolving first iteration\n");
	Grid_convolve(&raster_in.grid, &kern, &grid_mean);
	Grid_square(&raster_in.grid, &grid_sq);
	// Free some memory
	// Now square the mean and mean the square!
	printf("Copying raster dimensions\n");
	Grid_copy(&grid_mean, &grid_mean_sq, 0);
	Grid_copy(&grid_sq, &grid_sq_mean, 0);
	printf("Convolving second iteration\n");
	Grid_square(&grid_mean, &grid_mean_sq);
	Grid_convolve(&grid_sq, &kern, &grid_sq_mean);
	// Free up some memory
	Grid_free(&grid_sq);
	Grid_free(&grid_mean);
	// Subtract: sq_mean - mean_sq
	printf("Subtracting outputs\n");
	Grid_subtract(&grid_sq_mean, &grid_mean_sq, &raster_out.grid);
	// Free some memory
	Grid_free(&grid_sq_mean);
	Grid_free(&grid_mean_sq);
	Raster_write(file_out, &raster_out);
	printf("Cleaning up\n");
	Raster_free(&raster_in);
	Raster_free(&raster_out);
	Kernel_free(&kern);
	return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Grid.h"
#include "Kernel.h"
#include "Raster.h"
#include <string.h>

int main(int argc, char **argv) {
	char in_file[1024];
	char out_file[1024];
	int power = 2;
	float distance = 30.0f;
	int neighbor_count = 0;
	Raster in_raster;
	Raster out_raster;
	Raster_init(&in_raster);
	Raster_init(&out_raster);
	strcpy(in_file, argv[1]);
	strcpy(out_file, argv[2]);
	neighbor_count = atoi(argv[3]);
	power = atoi(argv[4]);
	printf("Filename is %s\n", in_file);
	Raster_read(in_file, &in_raster);
	Raster_copy(&in_raster, &out_raster, 0);
	printf("Smoothing\n");
	Grid_IDW(&in_raster.grid, neighbor_count, distance, power, &out_raster.grid);
	printf("Writing\n");
	Raster_write(out_file, &out_raster);
	printf("Cleaning up\n");
	Raster_free(&in_raster);
	Raster_free(&out_raster);
	return 0;
}

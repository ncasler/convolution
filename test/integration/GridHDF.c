#include <stdio.h>
#include <stdlib.h>
#include "Grid.h"
#include "Raster.h"
#include "hdf5.h"
#include <string.h>

int main(int argc, char **argv) {
	
	hid_t file_id, dataset_id, dataspace_id; /* Identifiers */
	hid_t attr1, attr2, attr3; /* Attribute identifiers */
	hid_t attr;
	hid_t aid1, aid2, aid3; /* Attribute Dataspace identifiers */
	size_t sdim;
	hid_t dtype, dtype_mem;
	hsize_t adims = 256;
	hsize_t afdims = 6;
	hsize_t nodatadims = 1;
	hid_t space_id, memtype;
	hid_t atype, atype_mem; /* Attribute type */
	hsize_t dims[2];
	herr_t ret; /* Return value */
	unsigned i, j; /* Counters */
	char projstr[1024]; /* Buffer to hold string */
	char grid_file[1024];
	char hdf_out[1024];
	strcpy(grid_file, argv[1]);
	strcpy(hdf_out, argv[2]);
	Raster raster;
	Raster_init(&raster);
	Raster_read(grid_file, &raster);
	/* Create a new file using default properties */
	file_id = H5Fcreate(hdf_out, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
	
		
	/* Create the data space for the dataset */
	dims[0] = raster.grid.cols;
	dims[1] = raster.grid.rows;
	dataspace_id = H5Screate_simple(2, dims, NULL);
	
	/* Create the dataset */
	dataset_id = H5Dcreate2(file_id, "/dset", H5T_IEEE_F32LE, dataspace_id,
			H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	ret = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, raster.grid.data);
	
	// Create Dataspace for the first attribute
	aid1 =  H5Screate(H5S_SCALAR);
	atype = H5Tcopy(H5T_C_S1);
			H5Tset_size(atype, 256);
			H5Tset_strpad(atype,H5T_STR_NULLTERM);
	attr1 = H5Acreate2(dataset_id, "Projection", atype, aid1, H5P_DEFAULT, H5P_DEFAULT);
	/* Write String attribute */
	ret = H5Awrite(attr1, atype, raster.proj4);
	/* End access to the dataset and release resources used by it */
	/* Clean up */
	ret = H5Sclose(aid1);
	ret = H5Aclose(attr1);
	ret = H5Tclose(atype);
	aid2= H5Screate_simple(1, &afdims, NULL);
	atype= H5Tcopy(H5T_IEEE_F64LE);
	attr2 = H5Acreate2(dataset_id, "Affine", atype, aid2, H5P_DEFAULT, H5P_DEFAULT);
	ret = H5Awrite(attr2, atype, raster.affine);

	ret = H5Sclose(aid2);
	ret = H5Aclose(attr2);
	ret = H5Tclose(atype);
	/* Terminate access to the data space. */

	aid3 = H5Screate(H5S_SCALAR);
	ret = H5Sset_extent_simple(aid3, 1, &nodatadims, NULL);
	attr3 = H5Acreate2(dataset_id, "NoData", H5T_IEEE_F64LE, aid3, H5P_DEFAULT, H5P_DEFAULT);
	ret = H5Awrite(attr3, H5T_NATIVE_DOUBLE, &raster.grid.nodata);
	printf("Writing no data %f\n", raster.grid.nodata);
	H5Sclose(aid3);
	H5Aclose(attr3);
	/* Close the file */
	ret = H5Sclose(dataspace_id);
	ret = H5Dclose(dataset_id);
	ret = H5Fclose(file_id);

	/* Open File dataset and attribute */
	file_id = H5Fopen(hdf_out, H5F_ACC_RDONLY, H5P_DEFAULT);
	dataset_id = H5Dopen(file_id, "/dset", H5P_DEFAULT);
	attr1 = H5Aopen(dataset_id, "Projection", H5P_DEFAULT);
	atype = H5Aget_type(attr1);
	sdim = H5Tget_size(atype);
	sdim++; // Make room for null terminator
	
	// Get space for attribute
	aid1 = H5Aget_space(attr1);
	adims = H5Sget_simple_extent_dims(aid1, dims, NULL);

	// Get a memory type that is and read
	memtype = H5Tcopy(H5T_C_S1);
	ret = H5Tset_size(memtype, sdim);
	ret = H5Aread(attr1, memtype, projstr);
	
	printf("Projection is :%s\n", projstr);
	ret = H5Aclose(attr1);
	ret = H5Dclose(dataset_id);
	ret = H5Sclose(aid1);
	ret = H5Tclose(atype);
	ret = H5Tclose(memtype);
	ret = H5Fclose(file_id);
	return 0;
}

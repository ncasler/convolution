#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Grid.h"
#include <string.h>
#include "Raster.h"

int main(int argc, char **argv) {
	char in_file[1024];
	char out_file[1024];
	Raster in_raster;
	Raster out_raster;
	Raster_init(&in_raster);
	Raster_init(&out_raster);
	strcpy(in_file, argv[1]);
	strcpy(out_file, argv[2]);
	Raster_read(in_file, &in_raster);
	Raster_copy(&in_raster, &out_raster, 0);
	Grid_laplace(&in_raster.grid, &out_raster.grid);
	printf("Writing raster\n");
	Raster_write(out_file, &out_raster);
	printf("Cleaning up\n");
	Raster_free(&in_raster);
	Raster_free(&out_raster);
	return 0;
}

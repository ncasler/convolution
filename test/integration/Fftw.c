#include <stdio.h>
#include <stdlib.h>
#include <fftw3.h>
#include <float.h>
#include <math.h>
#include <string.h>
#include "Raster.h"


int main(int argc, char** argv) {
	char file_in[1024];
	char file_out1[1024];
	char file_out2[1024];
	Raster raster_in;
	Raster raster_mag;
	Raster raster_phase;
	Raster_init(&raster_in);
	Raster_init(&raster_mag);
	Raster_init(&raster_phase);

	strcpy(file_in, argv[1]);
	strcpy(file_out1, argv[2]);
	strcpy(file_out2, argv[3]);

	Raster_read(file_in, &raster_in);
	Raster_copy(&raster_in, &raster_mag, 0);
	Raster_copy(&raster_in, &raster_phase, 0);

	fftw_plan plan;

	fftw_complex* in;
	fftw_complex* out;

	in = fftw_malloc(sizeof(fftw_complex) * raster_in.grid.cols * raster_in.grid.rows);
	out = fftw_malloc(sizeof(fftw_complex) * raster_in.grid.cols * raster_in.grid.rows);

	plan = fftw_plan_dft_2d(raster_in.grid.cols, raster_in.grid.rows, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

	int i,j;
	float tmp = 0.0f;
	for (j = 0; j < raster_in.grid.rows; j++) 
	{
		for (i = 0; i < raster_in.grid.cols; i++) 
		{
			tmp = Grid_get(i, j, &raster_in.grid);
			in[raster_in.grid.rows * j + i][0] = (double)tmp;
		}
	}

	fftw_execute(plan);

	// Normalize the data
	double real, img, mag, phase;
	double min = DBL_MAX;
	double max = -DBL_MAX;
	for (j = 0; j < raster_in.grid.rows; j++) 
	{
		for (i = 0; i < raster_in.grid.cols; i++) 
		{
			real = out[raster_in.grid.rows * j + i][0] / (double)(raster_in.grid.cols * raster_in.grid.rows);
			img  = out[raster_in.grid.rows * j + i][1] / (double)(raster_in.grid.cols * raster_in.grid.rows);
			mag = log(1 + sqrt((real * real) + (img * img)));
			//mag = sqrt(real * real) + (img * img);
			min = min > mag ? mag : min;
			max = max < mag ? mag : max;
			phase = atan(img/real);
			Grid_set(i, j, &raster_phase.grid, (float)phase);
			Grid_set(i, j, &raster_mag.grid, (float)mag);
		}
	}
	// Phase?
	//
	//
	Raster_write(file_out2, &raster_phase);
	Raster_write(file_out1, &raster_mag);
	fftw_destroy_plan(plan);
	fftw_free(in);
	fftw_free(out);
	Raster_free(&raster_in);
	Raster_free(&raster_mag);
	return 0;
}

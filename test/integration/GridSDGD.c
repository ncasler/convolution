#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "Grid.h"
#include "Util.h"
#include "Raster.h"

int main (int argc, char **argv)
{
	char file_in[1024];
	char file_out[1024];
	Raster raster_in;
	Raster raster_out;
	Grid grid_x;
	Grid grid_y;
	Grid grid_xx;
	Grid grid_xy;
	Grid grid_yy;
	Kernel kern_x;
	Kernel kern_y;
	int i, j = 0;
	float tmp = 0.0f;
	float px = 0.0f;
	float py = 0.0f;
	float pxx = 0.0f;
	float pxy = 0.0f;
	float pyy = 0.0f;

	Kernel_init(&kern_x);
	Kernel_init(&kern_y);
	Raster_init(&raster_in);
	Raster_init(&raster_out);
	Grid_init(&grid_x);
	Grid_init(&grid_y);
	Grid_init(&grid_xx);
	Grid_init(&grid_xy);
	Grid_init(&grid_yy);
	strcpy(file_in, argv[1]);
	strcpy(file_out, argv[2]);
	
	Kernel_sobel(0, &kern_x);
	Kernel_sobel(1, &kern_y);
	printf("Reading raster from %s\n", file_in);
	Raster_read(file_in, &raster_in);
	Raster_copy(&raster_in, &raster_out, 0);
	printf("Allocating Derivative Grids\n");
	Grid_copy(&raster_in.grid, &grid_x, 0);
	Grid_copy(&raster_in.grid, &grid_y, 0);
	Grid_copy(&raster_in.grid, &grid_xx, 0);
	Grid_copy(&raster_in.grid, &grid_xy, 0);
	Grid_copy(&raster_in.grid, &grid_yy, 0);

	int cols = raster_in.grid.cols;
	int rows = raster_in.grid.rows;

	printf("Calculating x\n");
	Grid_convolve(&raster_in.grid, &kern_x, &grid_x);
	printf("Calculating y\n");
	Grid_convolve(&raster_in.grid, &kern_y, &grid_y);
	printf("Calculating xx\n");
	Grid_convolve(&grid_x, &kern_x, &grid_xx);
	printf("Calculating xy\n");
	Grid_convolve(&grid_x, &kern_y, &grid_xy);
	printf("Calculating yy\n");
	Grid_convolve(&grid_y, &kern_y, &grid_yy);

	printf("Calculating SDGD values\n");
	for (j = 0; j < rows; j++)
	{
		for (i = 0; i < cols; i++)
		{
			px = Grid_get(i, j, &grid_x);
			py = Grid_get(i, j, &grid_y);
			pxx = Grid_get(i, j, &grid_xx);
			pxy = Grid_get(i, j, &grid_xy);
			pyy = Grid_get(i, j, &grid_yy);

			tmp = sdgd(px, py, pxx, pxy, pyy);
			Grid_set(i, j, &raster_out.grid, tmp);
		}
	}
	printf("Writing to file %s\n", file_out);
	Raster_write(file_out, &raster_out);
	printf("Cleaning up\n");
	Grid_free(&grid_x);
	Grid_free(&grid_y);
	Grid_free(&grid_xx);
	Grid_free(&grid_xy);
	Grid_free(&grid_yy);
	Raster_free(&raster_in);
	Raster_free(&raster_out);
	Kernel_free(&kern_x);
	Kernel_free(&kern_y);
	return 0;
}

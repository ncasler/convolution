#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Grid.h"
#include "Raster.h"
#include <string.h>

int main(int argc, char **argv) {
	char file_in[1024];
	char file_out[1024];
	Raster raster_in;
	Raster raster_out;
	Raster_init(&raster_in);
	Raster_init(&raster_out);
	strcpy(file_in, argv[1]);
	strcpy(file_out, argv[2]);
	Raster_read(file_in, &raster_in);
	Raster_copy(&raster_in, &raster_out, 0);
	printf("Calculating zero crossings\n");
	Grid_zero_cross(&raster_in.grid, &raster_out.grid);
	Raster_write(file_out, &raster_out);
	printf("Cleaning up\n");
	Raster_free(&raster_in);
	Raster_free(&raster_out);
	return 0;
}

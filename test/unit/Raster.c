#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Raster.h"
#include <string.h>
int main() {
	char in_file[1024] = "/projects/isgs/output/pp2g/champ/10.tif";
	char out_file[1024] = "/gpfs_scratch/ncasler/data/tmp/raster_test.tif";
	Raster raster;
	Raster_init(&raster);
	Raster_read(&in_file[0], &raster);
	printf("Writing Raster");
	Raster_write(&out_file[0], &raster);
	printf("Cleaning up");
	Raster_free(&raster);
	return 1;
}


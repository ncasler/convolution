#include <stdlib.h>
#include <stdio.h>
#include "Grid.h"

int main() {
	Grid grid;
	grid.cols = 10;
	grid.rows = 10;
	grid.data = NULL;
	Grid_alloc(&grid);
	int i = 0;
	int j = 0;
	for (i = 0; i < grid.rows; i++) {
		for (j = 0; j < grid.cols; j++) {
			printf("Setting Grid %i,%i to %i\n", j, i, i * j);
			Grid_set(j, i, &grid, (float)i * j);
		}
	}
	printf("Freeing Grid\n");
	Grid_free(&grid);
	return 0;
}


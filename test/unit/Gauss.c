#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include "Util.h"

int main() {

	int radius = 1;
	int i = 0;
	int j = 0;
	float tmp;
	float sigma = 1;
	radius = ceil(3 *sigma);
	int diam = 1 + (2 * radius);
	printf("Kernel for %i,%i gauss \n", diam, diam);
	for (j = -radius; j <= radius; j++)
	{
		printf("[ ");
		for (i = -radius; i <= radius; i++) 
		{
			tmp = gauss(i, j, sigma); 
			printf("%.3f, ", tmp);
		}
		printf(" ]\n");
	}
	sigma = 2;
	radius = ceil(3 * sigma);
	for (j = -radius; j <= radius; j++)
	{
		printf("[ ");
		for (i = -radius; i <= radius; i++) 
		{
			tmp = gauss(i, j, sigma); 
			printf("%.3f, ", tmp);
		}
		printf(" ]\n");
	}
	sigma = 3;
	radius = ceil(3 * sigma);
	for (j = -radius; j <= radius; j++)
	{
		printf("[ ");
		for (i = -radius; i <= radius; i++) 
		{
			tmp = gauss(i, j, sigma); 
			printf("%.3f, ", tmp);
		}
		printf(" ]\n");
	}



	return 0;
}

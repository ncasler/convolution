#include <stdio.h>
#include <stdlib.h>
#include "Kernel.h"

int main() {
	Kernel kern;
	kern.cols = 0;
	kern.rows = 0;
	kern.data = NULL;
	Kernel_sobel(0, &kern);
	printf("Kernel dimensions %ix%i\n", kern.cols, kern.rows);
	int i = 0;
	int j = 0;
	for (i = 0; i < kern.rows; i++) {
		printf("[");
		for (j = 0; j < kern.cols; j++) {
			printf("%.3f, ", Kernel_get(i, j, &kern));
		}
		printf("]\n");
	}
	Kernel_free(&kern);
	return 0;
}

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "Util.h"
#include <stdint.h>
#include <float.h>
int main()
{
	float tmp = -9999.0f;
	float comp = -9999;
	
	int iters = 10000;
	int i = 0;
	int relsum = 0;
	int ulpsum = 0;
	for (i = 0; i < iters; i++) 
	{
		if (almostEqualFloat(tmp, comp, 5))
			ulpsum++;
		if (relativeEqualFloat(tmp,comp, FLT_EPSILON))
			relsum++;
	}
	printf("Absolute: %i, rel: %i, total %i\n", ulpsum, relsum, iters);
	return 1;
}

#include <stdlib.h>
#include <stdio.h>
#include "Util.h"

int main() {
	int len = 10;
	int i = 0;
	int arr[10] = {2, 4, 1, 3, 7, 5, 9, 6, 8, 0};
	for (i = 0; i < len; i++) {
		printf("%i,",arr[i]);
	}
	printf("\nSorting\n");
	//quickSort(arr, 0, 9);
	qsort(arr, 9, sizeof(int), compareInt);
	for (i = 0; i < len; i++) {
		printf("%i,",arr[i]);
	}

	float f_arr[10] = {2.0f, 4.0f, 1.0f, 3.0f, 7.0f, 5.0f, 9.0f, 6.0f, 8.0f, 0.0f};
	printf("Float test:\n");
	for (i = 0; i < len; i++) {
		printf("%.1f,", f_arr[i]);
	}
	printf("\nSorting\n");
	qsort(f_arr, len-1, sizeof(float), compareFloat);
	//quickSortFloat(f_arr, 0, len-1);
	printf("sorted\n");
	for (i = 0; i < len; i++) {
		printf("%.1f,", f_arr[i]);
	}

	printf("\nComplete\n");
	return 0;
}


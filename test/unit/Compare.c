#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <assert.h>
#include "Util.h"

int main() {
	float a = 10.0f;
	float b = 10.0f;
	// Check that these are evaluate as equal
	assert(almostEqualFloat(a, b, 2));
	float c = 10.00005f;
	// Check that these evaluate as false
	assert(!almostEqualFloat(a, c, 2));
	return 0;
}

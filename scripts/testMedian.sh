#!/bin/bash

module load gdal2-stack zlib hdf5-parallel MPICH

OUT_BASE=/gpfs_scratch/ncasler/data/tmp/median

INPUT=/projects/isgs/output/pp2g/macon/10.tif

MEDIAN=./bin/test/Median

OUTPUT="${OUT_BASE}/macon_med.tif"

${MEDIAN} ${INPUT} ${OUTPUT}

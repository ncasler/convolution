#!/bin/bash

module load gdal2-stack zlib hdf5-parallel MPICH libgeotiff

OUT_BASE=/gpfs_scratch/ncasler/data/tmp/macon/ds/3

APP_BASE=/home/ncasler/app-dev/convolution
#INPUT=/projects/isgs/output/pp2g/macon/10.tif
#INPUT=/gpfs_scratch/ncasler/data/tmp/macon_idw.tif
#INPUT=/gpfs_scratch/ncasler/data/tmp/scale/macon_ds4.tif
INPUT=/gpfs_scratch/ncasler/data/tmp/macon/ds/macon_ds3.tif
GAUSS=${APP_BASE}/bin/test/GridGauss
SUBTRACT=${APP_BASE}/bin/test/GridSubtract
ZERO_CROSS=${APP_BASE}/bin/test/GridZeroCross

GAUSS1="${OUT_BASE}/macon_gauss1.tif"
GAUSS2="${OUT_BASE}/macon_gauss2.tif"
GAUSS3="${OUT_BASE}/macon_gauss3.tif"
GAUSS4="${OUT_BASE}/macon_gauss4.tif"
GAUSS5="${OUT_BASE}/macon_gauss5.tif"
GAUSS6="${OUT_BASE}/macon_gauss6.tif"
GAUSS7="${OUT_BASE}/macon_gauss7.tif"
GAUSS8="${OUT_BASE}/macon_gauss8.tif"
GAUSS9="${OUT_BASE}/macon_gauss9.tif"
GAUSS10="${OUT_BASE}/macon_gauss10.tif"

DOG1="${OUT_BASE}/macon_dog1.tif"
DOG2="${OUT_BASE}/macon_dog2.tif"
DOG3="${OUT_BASE}/macon_dog3.tif"
DOG4="${OUT_BASE}/macon_dog4.tif"
DOG5="${OUT_BASE}/macon_dog5.tif"
DOG6="${OUT_BASE}/macon_dog6.tif"
DOG7="${OUT_BASE}/macon_dog7.tif"
DOG8="${OUT_BASE}/macon_dog8.tif"
DOG9="${OUT_BASE}/macon_dog9.tif"
DOG10="${OUT_BASE}/macon_dog10.tif"

ZC1="${OUT_BASE}/macon_zc1.tif"
ZC2="${OUT_BASE}/macon_zc2.tif"
ZC3="${OUT_BASE}/macon_zc3.tif"
ZC4="${OUT_BASE}/macon_zc4.tif"
ZC5="${OUT_BASE}/macon_zc5.tif"
ZC6="${OUT_BASE}/macon_zc6.tif"
ZC7="${OUT_BASE}/macon_zc7.tif"
ZC8="${OUT_BASE}/macon_zc8.tif"
ZC9="${OUT_BASE}/macon_zc9.tif"
ZC10="${OUT_BASE}/macon_zc10.tif"

echo "CREATING STACK"

$GAUSS $INPUT $GAUSS1 1.6

$GAUSS $GAUSS1 $GAUSS2 1.6

$GAUSS $GAUSS2 $GAUSS3 1.6

$GAUSS $GAUSS3 $GAUSS4 1.6

$GAUSS $GAUSS4 $GAUSS5 1.6

$GAUSS $GAUSS5 $GAUSS6 1.6

$GAUSS $GAUSS6 $GAUSS7 1.6

$GAUSS $GAUSS7 $GAUSS8 1.6

$GAUSS $GAUSS8 $GAUSS9 1.6

$GAUSS $GAUSS9 $GAUSS10 1.6

echo "DIFFERENCING GAUSSIANS"

$SUBTRACT $INPUT $GAUSS1 $DOG1

$SUBTRACT $GAUSS1 $GAUSS2 $DOG2

$SUBTRACT $GAUSS2 $GAUSS3 $DOG3

$SUBTRACT $GAUSS3 $GAUSS4 $DOG4

$SUBTRACT $GAUSS4 $GAUSS5 $DOG5

$SUBTRACT $GAUSS5 $GAUSS6 $DOG6

$SUBTRACT $GAUSS6 $GAUSS7 $DOG7

$SUBTRACT $GAUSS7 $GAUSS8 $DOG8

$SUBTRACT $GAUSS8 $GAUSS9 $DOG9

$SUBTRACT $GAUSS9 $GAUSS10 $DOG10

echo "CALCULATING ZERO CROSSINGS"

$ZERO_CROSS $DOG1 $ZC1

$ZERO_CROSS $DOG2 $ZC2

$ZERO_CROSS $DOG3 $ZC3

$ZERO_CROSS $DOG4 $ZC4

$ZERO_CROSS $DOG5 $ZC5

$ZERO_CROSS $DOG6 $ZC6

$ZERO_CROSS $DOG7 $ZC7

$ZERO_CROSS $DOG8 $ZC8

$ZERO_CROSS $DOG9 $ZC9

$ZERO_CROSS $DOG10 $ZC10

#!/bin/bash
#PBS -N leeTest
#PBS -e /gpfs_scratch/ncasler/logs/conv/Macon_Lee.err
#PBS -o /gpfs_scratch/ncasler/logs/conv/Macon_Lee.out
#PBS -S /bin/bash
#PBS -l walltime=5:00:00
#PBS -l nodes=1:ppn=20

module load gdal2-stack zlib hdf5-parallel libgeotiff MPICH parallel

OUT_BASE=/gpfs_scratch/ncasler/data/tmp/Lee

#INPUT_DIR=/projects/isgs/output/pp2g/macon
INPUT=/projects/isgs/output/pp2g/champ/10.tif
#
#FILES=$(find ${INPUT_DIR} -name '*.tif')

LEE=/home/ncasler/app-dev/convolution/bin/test/Lee

OUTPUT="${OUT_BASE}/champ_lee.tif"

#parallel -j 20 $LEE {} ${OUT_BASE}{/} ::: ${FILES}

${LEE} ${INPUT} ${OUTPUT}

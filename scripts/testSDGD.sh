#!/bin/bash

module load gdal2-stack zlib hdf5-parallel libgeotiff MPICH parallel

BASE=/gpfs_scratch/ncasler/data/tmp
INPUT=${BASE}/gauss/macon_gauss4.tif
OUTPUT=${BASE}/median/macon_gauss_sdgd.tif

SDGD=/home/ncasler/app-dev/convolution/bin/test/SDGD

${SDGD} ${INPUT} ${OUTPUT}

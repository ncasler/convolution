#!/bin/bash

module load gdal2-stack

OUT_BASE=/gpfs_scratch/ncasler/data/tmp/sobel
#INPUT=/projects/isgs/output/pp2g/macon/10.tif
INDIR=/gpfs_scratch/ncasler/data/tmp/median
INPUT=${INDIR}/macon_med.tif

FILTER=./bin/test/GridFromRaster
DET=./bin/test/GridDeterminant
OUTX="${OUT_BASE}"/macon_10x.tif
OUTY="${OUT_BASE}"/macon_10y.tif
OUTXX="${OUT_BASE}"/macon_10xx.tif
OUTXY="${OUT_BASE}"/macon_10xy.tif
OUTYY="${OUT_BASE}"/macon_10yy.tif
OUTYX="${OUT_BASE}"/macon_10yx.tif
OUTDET="${OUT_BASE}"/macon_det4.tif
#OUTPUT=/gpfs_scratch/ncasler/data/tmp/raster_conv1.tif

${FILTER} $INPUT $OUTX 0

${FILTER} $INPUT $OUTY 1

${FILTER} $OUTX $OUTXX 0

${FILTER} $OUTX $OUTXY 1

${FILTER} $OUTY $OUTYY 1

${FILTER} $OUTY $OUTYX 0

${DET} ${OUTXX} ${OUTYY} ${OUTXY} ${OUTDET}

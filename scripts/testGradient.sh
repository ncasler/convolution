#!/bin/bash

module load gdal2-stack zlib hdf5-parallel libgeotiff MPICH parallel

BASE=/gpfs_scratch/ncasler/data/tmp

OUTDIR=${BASE}/gradient

INPUT=${BASE}/median/champ_med.tif

OUT_MAG=${OUTDIR}/champ_mag.tif
OUT_DIR=${OUTDIR}/champ_dir.tif

GRADIENT=/home/ncasler/app-dev/convolution/bin/test/Gradient

${GRADIENT} ${INPUT} ${OUT_MAG} ${OUT_DIR}

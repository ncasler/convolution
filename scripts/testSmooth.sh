#!/bin/bash

module load gdal2-stack

OUT_BASE=/gpfs_scratch/ncasler/data/tmp
INPUT=/projects/isgs/output/pp2g/champ/10.tif

OUTPUT=${OUT_BASE}/champ_idw.tif
SMOOTH=./bin/test/GridSmooth

POWER=3
NEIGHBORS=10
${SMOOTH} $INPUT $OUTPUT $NEIGHBORS $POWER


#!/bin/bash

module load gdal2-stack zlib hdf5-parallel MPICH libgeotiff

OUT_BASE=/gpfs_scratch/ncasler/data/tmp/gauss
#INPUT=/projects/isgs/output/pp2g/champ/10.tif
#INPUT=/gpfs_scratch/ncasler/data/tmp/champ_idw.tif
INPUT=/gpfs_scratch/ncasler/data/tmp/median/champ_med.tif
GAUSS=./bin/test/GridGauss
SUBTRACT=./bin/test/GridSubtract
ZERO_CROSS=./bin/test/GridZeroCross

GAUSS1="${OUT_BASE}/champ_gauss1.tif"
GAUSS2="${OUT_BASE}/champ_gauss2.tif"
GAUSS3="${OUT_BASE}/champ_gauss3.tif"
GAUSS4="${OUT_BASE}/champ_gauss4.tif"

DOG1="${OUT_BASE}/champ_dog1.tif"
DOG2="${OUT_BASE}/champ_dog2.tif"
DOG3="${OUT_BASE}/champ_dog3.tif"
DOG4="${OUT_BASE}/champ_dog4.tif"

ZC1="${OUT_BASE}/champ_zc1.tif"
ZC2="${OUT_BASE}/champ_zc2.tif"
ZC3="${OUT_BASE}/champ_zc3.tif"
ZC4="${OUT_BASE}/champ_zc4.tif"

$GAUSS $INPUT $GAUSS1 1.4

$GAUSS $INPUT $GAUSS2 2

$GAUSS $INPUT $GAUSS3 2.45

$GAUSS $INPUT $GAUSS4 2.8

$SUBTRACT $INPUT $GAUSS1 $DOG1

$SUBTRACT $GAUSS1 $GAUSS2 $DOG2

$SUBTRACT $GAUSS2 $GAUSS3 $DOG3

$SUBTRACT $GAUSS3 $GAUSS4 $DOG4

$ZERO_CROSS $DOG1 $ZC1

$ZERO_CROSS $DOG2 $ZC2

$ZERO_CROSS $DOG3 $ZC3

$ZERO_CROSS $DOG4 $ZC4

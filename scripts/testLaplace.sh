#!/bin/bash

module load gdal2-stack zlib hdf5-parallel libgeotiff MPICH parallel

OUTBASE=/gpfs_scratch/ncasler/data/tmp/laplace

LAPLACE=/home/ncasler/app-dev/convolution/bin/test/Laplace

INPUT=/gpfs_scratch/ncasler/data/tmp/median/macon_med.tif

OUTPUT=${OUTBASE}/macon_lap.tif

${LAPLACE} ${INPUT} ${OUTPUT}



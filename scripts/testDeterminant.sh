#!/bin/bash

module load gdal2-stack

OUT_BASE=/gpfs_scratch/ncasler/data/tmp
APP=./bin/test/GridDeterminant

XX="${OUT_BASE}/champ_10xx.tif"
YY="${OUT_BASE}/champ_10yy.tif"
XY="${OUT_BASE}/champ_10xy.tif"
OUT="${OUT_BASE}/champ_10det.tif"

${APP} ${XX} ${YY} ${XY} ${OUT}

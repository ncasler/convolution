#!/bin/bash

LIDARDIR=/projects/isgs/lidar
LOGDIR=/gpfs_scratch/ncasler/logs/pp2g

COUNTIES=('champaign' 'edgar' 'douglas' 'jasper' 'richland' 'vermilion' 'clark' 'coles' 'crawford' 'cumberland' 'lawrence')
ITER=(5 15 20)
#echo ${COUNTIES[*]}
echo "name, iterations, filecount, size, time"
for j in ${ITER[@]}; do
	for i in ${COUNTIES[@]}; do
		COUNT="$(find $LIDARDIR/$i/ -name *.las | wc -l)"
		SIZE=$(du -h $LIDARDIR/$i/*/ | grep "\/[lL][aA][sS]" | awk '{print $1}')
		TIME=$(tail -n 10 ${LOGDIR}/${i}_250m_80p_${j}it.out | grep "Exec time:" | awk '{print $3}')
		echo "$i, $j, $COUNT, $SIZE, $TIME"
	done
done
exit 0

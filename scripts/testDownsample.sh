#!/bin/bash

module load gdal2-stack zlib hdf5-parallel MPICH libgeotiff

OUT_BASE=/gpfs_scratch/ncasler/data/tmp/macon/ds
#INPUT=/projects/isgs/output/pp2g/macon/10.tif
INPUT=/gpfs_scratch/ncasler/data/tmp/median/macon_med.tif


DS1=${OUT_BASE}/macon_ds1.tif
DS2=${OUT_BASE}/macon_ds2.tif
DS3=${OUT_BASE}/macon_ds3.tif
DS4=${OUT_BASE}/macon_ds4.tif
DS5=${OUT_BASE}/macon_ds5.tif
DS6=${OUT_BASE}/macon_ds6.tif
DS7=${OUT_BASE}/macon_ds7.tif
DS8=${OUT_BASE}/macon_ds8.tif
DS9=${OUT_BASE}/macon_ds9.tif
DS10=${OUT_BASE}/macon_ds10.tif

GAUSS1=${OUT_BASE}/macon_dsgaus1.tif
GAUSS2=${OUT_BASE}/macon_dsgaus2.tif
GAUSS3=${OUT_BASE}/macon_dsgaus3.tif
GAUSS4=${OUT_BASE}/macon_dsgaus4.tif
GAUSS5=${OUT_BASE}/macon_dsgaus5.tif
GAUSS6=${OUT_BASE}/macon_dsgaus6.tif
GAUSS7=${OUT_BASE}/macon_dsgaus7.tif
GAUSS8=${OUT_BASE}/macon_dsgaus8.tif
GAUSS9=${OUT_BASE}/macon_dsgaus9.tif
#GAUSS7=${OUT_BASE}/gauss7.tif
DOWNSAMPLE=./bin/test/GridDownSample

GAUSS=./bin/test/GridGauss

SIGMA=1.4

${GAUSS} ${INPUT} ${GAUSS1} ${SIGMA}
${DOWNSAMPLE} ${GAUSS1} ${DS1}
${GAUSS} ${DS1} ${GAUSS2} ${SIGMA}
${DOWNSAMPLE} ${GAUSS2} ${DS2}
${GAUSS} ${DS2} ${GAUSS3} ${SIGMA}
${DOWNSAMPLE} ${GAUSS3} ${DS3}
${GAUSS} ${DS3} ${GAUSS4} ${SIGMA}
${DOWNSAMPLE} ${GAUSS4} ${DS4}
${GAUSS} ${DS4} ${GAUSS5} ${SIGMA}
${DOWNSAMPLE} ${GAUSS5} ${DS5}
${GAUSS} ${DS5} ${GAUSS6} ${SIGMA}
${DOWNSAMPLE} ${GAUSS6} ${DS6}

${GAUSS} ${DS6} ${GAUSS7} ${SIGMA}
${DOWNSAMPLE} ${GAUSS7} ${DS7}

${GAUSS} ${DS7} ${GAUSS7} ${SIGMA}
${DOWNSAMPLE} ${GAUSS7} ${DS8}

${GAUSS} ${DS8} ${GAUSS8} ${SIGMA}
${DOWNSAMPLE} ${GAUSS8} ${DS9}

${GAUSS} ${DS9} ${GAUSS9} ${SIGMA}
${DOWNSAMPLE} ${GAUSS9} ${DS10}
#${DOWNSAMPLE} ${INPUT} ${DS1}
#${DOWNSAMPLE} ${DS1} ${DS2}
#${DOWNSAMPLE} ${DS2} ${DS3}
#${DOWNSAMPLE} ${DS3} ${DS4}
#${DOWNSAMPLE} ${DS4} ${DS5}
#${DOWNSAMPLE} ${DS5} ${DS6}
#${DOWNSAMPLE} ${DS6} ${DS7}

#!/bin/bash

module load gdal2-stack zlib hdf5-parallel MPICH

OUT_BASE=/gpfs_scratch/ncasler/data/tmp/stats

INPUT=/projects/isgs/output/pp2g/champ/11.tif

STAT=./bin/test/Stats

${STAT} ${INPUT}

#!/bin/bash

module load gdal2-stack zlib hdf5-parallel MPICH libgeotiff

OUT_BASE=/gpfs_scratch/ncasler/data/tmp/variance

#INPUT=/projects/isgs/output/pp2g/champ/10.tif
INPUT=/projects/isgs/output/pp2g/macon/10.tif

VAR=./bin/test/GridVar

OUTPUT="${OUT_BASE}/macon_var.tif"

${VAR} ${INPUT} ${OUTPUT}

#!/bin/bash

module purge

module load gdal2-stack  MPICH zlib fftw libgeotiff netcdf4-parallel

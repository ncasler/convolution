#!/bin/bash

module load gdal2-stack zlib hdf5-parallel libgeotiff MPICH parallel

BASE=/gpfs_scratch/ncasler/data/tmp
INPUT=${BASE}/gauss/macon_dog3.tif
OUTPUT=${BASE}/activate/macon_dog3_active.tif

ACT=/home/ncasler/app-dev/convolution/bin/test/Activate

${ACT} ${INPUT} ${OUTPUT}

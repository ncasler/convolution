#!/bin/bash

module load gdal2-stack zlib hdf5-parallel libgeotiff MPICH parallel

BASE=/gpfs_scratch/ncasler/data/tmp
INPUT=${BASE}/gauss/champ_gauss4.tif
OUTPUT=${BASE}/median/champ_gauss_iso.tif

ISO=/home/ncasler/app-dev/convolution/bin/test/Isophote

${ISO} ${INPUT} ${OUTPUT}

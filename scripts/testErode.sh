#!/bin/bash

module load gdal2-stack zlib hdf5-parallel MPICH

OUT_BASE=/gpfs_scratch/ncasler/data/tmp/morph

INPUT=/projects/isgs/output/pp2g/macon/10.tif

OUTPUT="${OUT_BASE}/macon_10_erode.tif"

ERODE=./bin/test/Erode

${ERODE} ${INPUT} ${OUTPUT}


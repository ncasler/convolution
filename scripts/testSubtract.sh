#!/bin/bash

module load gdal2-stack zlib hdf5-parallel MPICH

OUT_BASE=/gpfs_scratch/ncasler/data/tmp

IN_GRID=/projects/isgs/output/pp2g/champ/10.tif

MED_GRID=${OUT_BASE}/median/champ_med.tif

OUTPUT=${OUT_BASE}/median/residuals.tif

SUBTRACT=./bin/test/GridSubtract

${SUBTRACT} ${IN_GRID} ${MED_GRID} $OUTPUT

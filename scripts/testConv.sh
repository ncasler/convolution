#!/bin/bash
#PBS -N conv_Macon
#PBS -e /gpfs_scratch/ncasler/logs/conv/Macon_null_20p.err
#PBS -o /gpfs_scratch/ncasler/logs/conv/Macon_null_20p.out
#PBS -S /bin/bash
#PBS -l walltime=1:00:00
#PBS -l nodes=4:ppn=20


BASE=/home/ncasler/app-dev/convolution

module load gdal2-stack MPICH zlib hdf5-parallel fftw libgeotiff valgrind

CONV=${BASE}/bin/Conv


NITER=1
INPUT_FILE=/projects/isgs/output/pp2g/macon/macon.vrt
OUT_DIR=/gpfs_scratch/ncasler/data/conv2
PREFIX="conv"

START=`date +%s`

mpirun -np 72 valgrind --leak-check=full ${CONV} -a 8 -b 9  -i -v -f "${INPUT_FILE}" -o "${OUT_DIR}" -p "${PREFIX}" 

END=`date +%s`

echo "EXEC TIME: $(($END- $START))"


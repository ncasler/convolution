/**
 * @file
 * @author Nathan Casler
 * @date 17 October 2017
 */
#ifndef GRID_H
#define GRID_H
#include <stdlib.h>
#include <netcdf.h>
#include "Kernel.h"

/**
 * @brief Container struct for 2-dimensional array data.
 */
typedef struct {
	int cols;      /** Number of columns **/
	int rows;      /** Number of rows    **/
	double nodata; /** No data value     **/
	float *data;   /** Pointer to data array **/
} Grid;

/**
 * @brief Container struct for 2-dimensional multivariate array data.
 * @warning Currently not functional, will be updated to allow for NetCDF IO.
 */
typedef struct {
	int id;       /** Identifier **/
	int ndims;    /** Number of dimensions **/
	int *dims;    /** Pointer to dimension definitions **/
	int *dimids;  /** Pointer to dimension identifiers **/
	int nvars;    /** Number of variables **/
	int *varids;  /** Pointer to variable identifiers **/
} mGrid;


void Grid_init(Grid *grid);
int Grid_copy(Grid *g1, Grid *g2, int full);
int Grid_alloc(Grid *grid);
void Grid_free(Grid *grid);
float Grid_get(int col, int row, const Grid *grid);
int Grid_set(int col, int row, Grid *grid, float val);
int Grid_convolve(const Grid *in_grid, const Kernel *kern, Grid *out_grid);
int Grid_subtract(const Grid *in_grid_1, const Grid *in_grid_2, Grid *out_grid);
int Grid_square(const Grid *in_grid, Grid *out_grid);
int Grid_zero_cross(const Grid *in_grid, Grid *out_grid);
int Grid_IDW(const Grid *in_grid, int n_neighbors, float distance, int power, Grid *out_grid);
int Grid_read(const char *filePath, Grid *grid);
int Grid_downsample(const Grid *in_grid, Grid *out_grid);

// Stat functions
float Grid_mean(const Grid* grid);
float Grid_var(const Grid* grid, const float mean);
float Grid_stdev(const float var);
float Grid_median(const Grid* grid, int i, int j, int radius);
float Grid_Lee(const Grid* grid, int i, int j, int radius, float g_var);
float Grid_mean_win(const Grid* grid, int i, int j, int radius);
float Grid_var_win(const Grid* grid, int i, int j, int radius, float mean);
int Grid_laplace(const Grid* in_grid, Grid *out_grid);
float Grid_erode(const Grid *in_grid, int i, int j , Kernel *k);
float Grid_dilate(const Grid *in_grid, int i, int j, Kernel *k);
// Morphological functions
float calc_ridge(float x, float y, float xx, float xy, float yy);
float ridge(float xx, float xy, float yy);
float sdgd(float x, float y, float xx, float xy, float yy);
#endif

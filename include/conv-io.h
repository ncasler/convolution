/** conv-io.h
 * Author: Nathan P. Casler <ncasler@illinois.edu>
 * Date: 12/01/2017
 */

/* -*- Mode: C; c-basic-offset:4; -*- */
/* 
 * Basic IO Operations for the Convolution library
 */

#ifndef CONV_IO_H
#define CONV_IO_H

int CONVIO_Init(MPI_Comm comm);
int CONVIO_Can_restart(void);
int CONVIO_Info_set(MPI_Info info);
int CONVIO_Restart(CONVPatchDesc *patch, float **matrix, MPI_Info);
int CONVIO_Checkpoint(CONVPatchDesc *patch, float **matrix, 
		int iter, MPI_Info info);
int CONVIO_Finalize(void);

extern int CONV_myrows(int dimsz, int rank, int nprocs);
extern int CONV_myrowoffset(int dimsz, int rank, int nprocs);

#endif /** CONV_IO_H **/

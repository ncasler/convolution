/** global.h
 * Author: Nathan P. Casler <ncasler@illinois.edu>
 * Date: 12/01/2017
 */

#ifndef LAS_GLOBAL_H
#define LAS_GLOBAL_H

#define LAS_UCHAR	1
#define LAS_USHORT	2
#define LAS_ULONG	4
#define LAS_DBL		8

#endif

/** Util.h
 * Author: Nathan P. Casler <ncasler@illinois.edu>
 * Date: 12/01/2017
 */

#ifndef CONV_UTIL_H
#define CONV_UTIL_H
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <stdint.h>

int almostEqualFloat(float a, float b, int maxUlps);

int relativeEqualFloat(float a, float b, float maxRelDiff);

uint32_t byte_swap32(uint32_t a);

uint32_t nibb_swap32(uint32_t a);

int compareInt(const void *a, const void *b);

int compareFloat(const void *a, const void *b);

float gauss(int x, int y, float sigma);

double absolute(double x);

#endif

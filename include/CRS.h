/** CRS.h
 * Author: Nathan P. Casler <ncasler@illinois.edu>
 * Date: 12/01/2017
 */

/**
 * @file
 * Coordinate Reference System Header File
 */
#ifndef CRS_H
#define CRS_H
#include <geotiff.h>
#include <geo_simpletags.h>
#include <geo_normalize.h>
#include <geovalues.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "GeoKey.h"
#include "VLR.h"

/** 
 * @brief Container class for CRS data
 * Useful for translating between proj and GeoTIFF, or LAS formats.
 */
typedef struct CRS {
	ST_TIFF* m_tiff;    /** Tiff descriptor object **/
	GTIF*    m_gtiff;   /** GeoTiff descriptor object **/
	char*    proj;      /** Proj4 string **/
} CRS;


void CRS_init(CRS* crs);

int CRS_read(sGeoKeys* geo, CRS *crs);

int CRS_getProj4(CRS* crs, char * proj4);
int CRS_getGTIF(CRS *crs, VLR* vlr, unsigned long n_vlr);

void CRS_free(CRS* crs);

#endif // CONV_CRS_H

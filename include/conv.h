/** conv.h
 * Author: Nathan P. Casler <ncasler@illinois.edu>
 * Date: 12/01/2017
 */

#ifndef CONV_H
#define CONV_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <mpi.h>
#include "gdal.h"
#include "cpl_conv.h"
#define NODATA -9999

/**
 * @brief Descriptor for managing processor mesh, data mesh and raster information.
 */
typedef struct {
	MPI_Comm comm;  /** Communicator for processor mesh **/
	int patchI, patchJ; /** (I,J) location of patch in processor mesh **/
	int pNI, pNJ;       /** Size of processor mesh **/
	int left, right, up, down;  /** Neighbors to this patch: left = (I, J-1),
								   up=(I-1,J), etc. (standard matrix ordering) **/
	int ul, ur, ll, lr;    /** Upper left, upper right, lower left,
							  lower right (needed for on option of the 9
							  point stencil **/
	int gNI, gNJ;          /** Full size of the data mesh **/
	int gI, gJ;            /** Global I, J for the upper left of the 
							  local patch **/
	int lni, lnj;          /** Size of the local patch **/
	double gAffine[6];     /** Affine transformation parameters for dataset **/
	float nodata;          /** No Data value to be used for null cells **/
	char proj_str[1024];   /** Proj.4 Projection string **/
	int nBands;            /** Number of bands within raster **/
	GDALDatasetH hDS; /** GDAL dataset for IO (could just hold the input path) **/
} CONVPatchDesc;

/**
 * @brief Time tracker for profiling communication exchanges
 */
typedef struct {
	double packtime, unpacktime; /** Time to pack/unpack data if separate **/
	double exchtime;             /** Time for exchange **/
	double itertime;             /** Time for the iteration loop **/
} CONVTiming;

/** 
 * @brief Processing parameters for identifying mesh size and dataset
 */
typedef struct {
	int gNI, gNJ;				/** Size of global mesh **/
	int pNI, pNJ;				/** Size of processor mesh **/
	int nIter;					/** Number of iterations **/
	int restartIter;			/** Used to determine when to create restart files **/
	int verbose;				/** Whether verbose output used **/
	int doIO;					/** Whether I/O used **/
	char dataset[1024];         /** Source dataset path **/
	char outdir[1024];          /** Output directory path **/
	char prefix[64];			/** Name for output file prefix **/
} CONVOptions;

int CONV_ParseArgs(int argc, char **argv, CONVOptions *options);

int CONV_ParseDatasetHeader(CONVPatchDesc *patch, const char *dset);

int CONV_PatchCreateProcessMesh(CONVOptions *options, 
				CONVPatchDesc *patch);
int CONV_PatchCreateProcessMeshWithCart( CONVOptions *options,
				CONVPatchDesc *patch);
int CONV_PatchCreateDataMeshDesc(CONVOptions *options,
				CONVPatchDesc *patch);

int CONV_AllocateLocalMesh( CONVPatchDesc *patch, float ***m1, float ***m2);
int CONV_FreeLocalMesh( CONVPatchDesc *patch, float **m1, float **m2);
int CONV_InitLocalMesh( CONVPatchDesc *patch, float **m1, float **m2);
int CONV_WriteLocalMesh( CONVOptions *opts, CONVPatchDesc *patch, float **mat, 
		int iter);
int CONV_TimeIterations(CONVPatchDesc *patch, int nIter, int doCheck, 
					float **m1, float **m2,
					int (*exchangeInit)(CONVPatchDesc *, float**, float**, void*),
					int (*exchange)( CONVPatchDesc *, float **, CONVTiming *, void *),
					int (*exchangeEnd) ( void * ),
					CONVTiming *timedata);
int CONV_TimePreProcess(CONVPatchDesc *patch, int nIter, int doCheck, 
					float **m1, float **m2,
					int (*exchangeInit)(CONVPatchDesc *, float**, float**, void*),
					int (*exchange)( CONVPatchDesc *, float **, CONVTiming *, void *),
					int (*exchangeEnd) ( void * ),
					CONVTiming *timedata);
void CONV_FreePatch(CONVPatchDesc *patch);
void CONV_Abort( const char str[] );

/* RMA With Fence */
int CONV_ExchangeInitFence( CONVPatchDesc *patch,
		float **m1, float **m2, void *privateData );
int CONV_ExchangeEndFence( void *privateData );
int CONV_ExchangeFence( CONVPatchDesc *patch, float **matrix, 
		CONVTiming *timedata, void *privateData );

int CONV_TimeExchange(CONVPatchDesc *patch, float **m1, float **m2, 
		int (*exchangeInit) ( CONVPatchDesc *, float **, float **, void *),
		int (*exchange) (CONVPatchDesc *, float **, CONVTiming *, void *),
		int (*exchangeEnd) ( void * ),
		CONVTiming *timedata);
int CONV_TimeFillNull(CONVPatchDesc *patch, int nIter, int doCheckpoint, 
		float **m1, float **m2,
		int (*exchangeInit) (CONVPatchDesc *, float **, float **, void *),
		int (*exchange) (CONVPatchDesc *, float **, CONVTiming *, void *),
		int (*exchangeEnd) ( void * ),
		CONVTiming *timedata);
int CONV_TimeProcess(CONVPatchDesc *patch, int nIter, 
		float **m1, float **m2,
		int (*exchangeInit) (CONVPatchDesc *, float **, float **, void *),
		int (*exchange) (CONVPatchDesc *, float **, CONVTiming *, void *),
		int (*exchangeEnd) ( void * ),
		CONVTiming *timedata);
int CONV_TimeClassify(CONVPatchDesc *patch,
		float **m1, float **m2,
		int (*exchangeInit) (CONVPatchDesc *, float **, float **, void *),
		int (*exchange) (CONVPatchDesc *, float **, CONVTiming *, void *),
		int (*exchangeEnd) ( void * ),
		CONVTiming *timedata);



int CONV_cleanPatch(CONVPatchDesc *patch, float **matrix);
/* Null filling */
float CONV_IDW(CONVPatchDesc *patch, float **matrix, int row, int col, int radius, int power);
float CONV_NN(CONVPatchDesc *patch, float **matrix, int row, int col);

/* Stats */
float CONV_PatchMean(CONVPatchDesc *patch, float **mat);
float CONV_PatchVariance(CONVPatchDesc *patch, float **mat, const float mean);

/* Kernel function */
int CONV_AllocateKernel(float ***k, int radius);
int CONV_FreeKernel(float **kernel);


/* Filters */
float CONV_Lee(CONVPatchDesc *patch, float **matrix, int i, int j, int radius, float g_var);
float CONV_Convolve(CONVPatchDesc *patch, float **matrix, float **weights, int radius,
		int row, int col);

/* Differentiation */
float CONV_mixedDerivative(CONVPatchDesc *patch, float **matrix, int row, int col);
float CONV_Curvature(CONVPatchDesc *patch, float **matrix, int row, int col);


#endif

/** Kernel.h
 * Author: Nathan P. Casler <ncasler@illinois.edu>
 * Date: 12/01/2017
 */

#include <stdio.h>
#include <stdlib.h>
#ifndef KERNEL_H
#define KERNEL_H
typedef struct Kernel {
	int cols; // Columns within the kernel
	int rows; // Rows within the kernel
	float coef; // @deprecated: Coefficient to use when normalizing data
	float *data; // Array holding the values of the kernel
} Kernel;

void Kernel_init(Kernel *kern);
int Kernel_alloc(Kernel *kern);
void Kernel_free(Kernel *kern);
int PixelsNeededForSigma(float sigma);
int Kernel_gauss(float sigma, Kernel *kern);
int Kernel_sobel(int axis, Kernel *kern);
int Kernel_mean(int radius, Kernel *kern);
float Kernel_get(int col, int row, const Kernel *kern);
int Kernel_set(int col, int row, Kernel *kern, float val);




#endif // KERNEL_H

/** Activation.h
 * Author: Nathan P. Casler <ncasler@illinois.edu>
 * Date: 12/01/2017
 */

#ifndef CONV_ACTIVATE
#define CONV_ACTIVATE
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include "Util.h"

double CONVA_ident(double x);

int CONVA_step(double x);

double CONVA_log(double x);

double CONVA_tanh(double x);

double CONVA_atan(double x);

double CONVA_softsign(double x);

double CONVA_relu(double x);

#endif // CONV_ACTIVATE

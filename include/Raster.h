/**
 * Copyright (C) 2017 by Nathan Casler
 *
 * LICENSE: NCSA/University of Illinois
 */

/**
 * @file raster.h
 * @author Nathan Casler
 * @date 15 October 2017
 * @brief File containing functions relevant for raster manipulation and processing
 *
 * This Raster class is a minimalistic wrapper class which provides basic 
 * georeferencing and transformation support for 2D arrays
 */

#ifndef RASTER_H
#define RASTER_H
#include <stdlib.h>
#include "Grid.h"

/** Raster class
 * @brief This class contains basic metadata needed to georeference and project
 * 2D arrays for raster datasets.
 */
typedef struct Raster {
	Grid grid; /** Grid instance to hold pixel data */
	double affine[6]; /** Affine matrix to handle transformations */
	char proj4[1024]; /** proj.4 string of SRS */
} Raster;

/** 
 *  @brief Initialize a new Raster instance
 *
 *  @param raster Raster instance to initialize
 *  @return void
 */
void Raster_init(Raster *raster);

/** 
 *  @brief Read raster metadata into Raster instance, excludes pixel data
 *
 *  @param in_file File path to GDAL-compliant dataset
 *  @param raster  Destination raster object
 *  @return 1 on success, else -1
 */
int Raster_stat(char *in_file, Raster *raster);

/** 
 *  @brief Read GDAL-compliant raster data into a Raster instance
 *  @param in_file File path to GDAL-compliant raster
 *  @param raster  Destination Raster instance
 *  @return 1 if success, else -1
 */
int Raster_read(char *in_file, Raster *raster);

/** 
 *  @brief Copy contructor for Raster instances, may be full or shallow copy
 *  @param r1 Source Raster instance
 *  @param r2 Destination Raster instance
 *  @param full If true perform deep copy including pixel data, else leave pixels uninitialized
 *  @return 0
 */
int Raster_copy(Raster *r1, Raster *r2, int full);

/**  
 *  @brief Write a Raster instance out to GeoTiff
 *  @param out_file Destination file path
 *  @param raster Source Raster instance
 *  @return 0
 */
int Raster_write(char *out_file, Raster *raster);

/** 
 *  @brief De-initialize and free allocated memory from a Raster instance
 *  @param raster Raster instance to deallocate
 *  @return void
 */
void Raster_free(Raster *raster);

/** 
 *  @brief Downsample a Raster instance to 2x the pixel size in x and y dimensions.
 *  @detail Downsampling will use a naive average to downsample the pixels, which
 *  can cause border effects around edges of image.
 *  @param r1 Source Raster instance
 *  @param r2 Destination Raster instance
 *  @return 1 if success, else 0
 */
int Raster_downsample(const Raster *r1, Raster *r2);

#endif /* RASTER_H */
